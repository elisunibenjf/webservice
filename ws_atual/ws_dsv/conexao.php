<?php 
	
final class Conexao{
	private function __construct(){}

	public static function open($name){
		// Verifica se existe arquivo ode configuração para este banco de dados
		if(file_exists("config/{$name}.ini")){
			$db = parse_ini_file("config/{$name}.ini");
		}else{
			throw new Exception("Arquivo '$name' não encontrado.", 1);
		}
		
		// lê as informações contidas no arquivo
		// $path        = '192.168.1.4:d:\sistema_dsv\info\OPERADORAPS.FDB';
		// $ip          = '192.168.1.4';
		// $user        = 'SYSDBA';
		// $password    = 'masterkey';
		// $charset     = 'ISO8859_1';
		// $database    = '';
		// $sql         = '';
		// $dataSet     = array();
		// $recordCount = 0;
		// $erro        = '';
		// $sqlconn     = NULL;
		// $trans       = NULL;

		$user = isset($db['user']) ? $db['user'] : NULL;
		$pass = isset($db['pass']) ? $db['pass'] : NULL;
		$name = isset($db['name']) ? $db['name'] : NULL;
		$host = isset($db['host']) ? $db['host'] : NULL;
		$type = isset($db['type']) ? $db['type'] : NULL;
		$port = isset($db['port']) ? $db['port'] : NULL;

		$nameF = isset($host) ? "{$host}:{$name}" : $name; 

		//descobre qual o tipo (driver) de banco de dados a ser utilizado
		switch ($type) {
			case 'pgsql':
				$port = $port ? $port : '5432';
				$conn = new PDO("pgsql:dbname={$name}; user={$user}; password={$pass}; host={$host}; port={$port}");
				break;
			case 'mysql':
				$port = $port ? $port : '3306';
				// $conn = new PDO("mysql:host={$host};dbname={$name};user={$user};password={$pass}");
				$conn = new PDO("mysql:host=$host;dbname=$name", "$user", "$pass");
				break;
			case 'sqlite':
				$conn = new PDO("sqlite:{$name}");
				break;
			case 'ibase':
				$conn = new PDO("firebird:dbname={$nameF}", $user, $pass);
				break;
			case 'oci8':
				$conn = new PDO("oci:dbname={$name}", $user, $pass);
				break;
			case 'mssql':
				$conn = new PDO("mssql:host={$host},1433; dbname={$name}", $user, $pass);
				break;
		}
		//define para que o PDO lance exceções na ocorrência de erros
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
		return $conn;
	}
}

 ?>