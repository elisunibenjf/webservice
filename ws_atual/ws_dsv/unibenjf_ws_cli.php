<?php
/*******************************************************************************
 * unibenjf_ws_cli.php
    - processa as requisicoes de cliente
    
  http://unibenjf.com.br/corretor  
 */
 
$vHelp = '
/*******************************************************************************************************
  - operacoes (parametro op)
    - ???   - help   
    - alt   - cadastra/altera um cliente
    - vcpf  - verifica se existe cliente com o cpf informado
    - ler   - le os dados cadastrais
    - lstn  - lista cliente por nome
    - lcpf  - ler os dados cadastrais pelo CPF
    - ltel  - listar telefones
    - itel  - incluir telefone
    - atel  - alterar telefone
    - etel  - excluir telefone
    - ldep  - listar dependentes
    - idep  - incluir dependente
    - adep  - alterar dependente
    - edep  - excluir dependente
    - lpro  - listar produtos
    - ipro  - incluir produto
    - apro  - alterar produto
    - epro  - excluir produto
    - atpro - ativar produto
    - dlpro  - lista produtos dos dependentes 
    - dapro  - altera dados de produtos de dependentes
    - datpro - ativa produto de dependente
     
    op    Parametros                                        retorno
    ====  ==============================================================================================
    alt   cod   - codigo do cliente                         json de resposta: sucesso,erro,codigo do cliente incluido 
                - se for ZERO indica que é uma inclusao        
                - senao indica alteracao  do cliente com 
                  o codigo "cod"
          cpf   - cpf (11 caracteres sem formatacao )       
          nome  - nome(100 caracteres)                   
          log   - codigo do logradouro
          num   - numero do endereco
          compl - complemento endereco (20 caracteres)
          bai   - codigo do bairro
          cep   - cep (8 caracter)
          email - email (100 caracter)
          cxp   - caixa postal (10 caracter)
          sexo  - F ou M
          dtn   - data nascimento (aaaa-mm-dd)
          estc  - codigo do estado civil
          mae   - nome da mae
          rg    - carteira de identidade
          rgo   - orgao emissor rg
          rgu   - codigo uf rg
          rgp   - codigo pais rg
          csus  - cartao sus
          pis   - pis/pasep
          mpg   - codigo do modo de pagamento (1-boleto 2-Debito CC)
          bc    - codigo do banco
          ag    - agencia
          cc    - conta corrente
          diav  - dia vencimento
          dtadm - data de admissao (aaaa-mm-dd)
          cobcp - envio de cobranca para caixa postal (S/N)    
          i01   - instituicao 1
          u01   - unidade de instituicao 1
          i02   - instituicao 2
          u02   - unidade de instituicao 2
          emp   - codigo da empresa
    ----  ----------------------------------------------------------------------------------------------
    vcpf  cpf   - cpf (11 caracteres sem formatacao )       json de resposta: sucesso,erro,codigo cliente,email
    ----  ----------------------------------------------------------------------------------------------
    ler   cod   - codigo do cliente                         json de resposta
                                                            mesmo dados da operacao "alt" e os seguintes abaixo:
                                                            - lognome - logradouro
                                                            - tlognome- tipo do logradouro
                                                            - bainome - bairro
                                                            - cidnome - cidade
                                                            - ufsigla - estado
    ----  ----------------------------------------------------------------------------------------------                                                        
    lstn  nome  - parte no nome a ser consultado            json de resposta                  
          pos   - posicao [i]nicio [q]ualquer 
    ----  ----------------------------------------------------------------------------------------------
    lcpf  cpf   - cpf do cliente                            json de resposta
    ----  ----------------------------------------------------------------------------------------------
    ltel  cod   - codigo do cliente                         json de resposta
    ----  ----------------------------------------------------------------------------------------------
    itel  cod   - codigo do cliente                         json de resposta
    atel  ddd   - ddd (A2) 
          tel   - telefone (A8)
          descr - descricao(A30)(somente atel)
    ----  ----------------------------------------------------------------------------------------------
    etel  cod   - codigo do cliente                         json de resposta            
          ddd   - ddd (A2) 
          tel   - telefone (A8)
    ----  ----------------------------------------------------------------------------------------------
    ldep  cod   - codigo do cliente                         json de resposta
          depcod- codigo do dependente 
                  (opcional-se nao especificar lista todos)
    ----  ----------------------------------------------------------------------------------------------
    idep  cod   - codigo do cliente                         json de resposta
    adep  depcod- codigo do dependente 
          - <dados no formato do ldep>(somente adep) 
          - obs: - parametros depgparnome e depestcnome nao precisam ser usados
                 - depimens - isento de mensalidade (S/N) 
    ----  ----------------------------------------------------------------------------------------------      
    edep  cod   - codigo do cliente                         json de resposta            
          depcod- codigo do dependente 
    ----  ----------------------------------------------------------------------------------------------
    lpro  cod   - codigo do cliente                         json de resposta
          prodcod- codigo do produto 
          instcod- codigo da instituicao
          (opcionais-se nao especificar lista todos)   
    ----  ----------------------------------------------------------------------------------------------
    ipro  cod         - codigo do cliente                   json de resposta
          prodcod     - codigo do produto
          instcod     - codigo da instituicao
          desconto    - desconto em percentual (999.99)
          dtvigencia  - data de vigencia (aaaa-mm-dd)
          modocob     - modo de cobranca (C/I/E)
          txassoc     - cobrar taxa associativa(S/N) 
          numcarteira - numero da carteita (15 caracter)
          codconsultor- codigo do consultor  
    ----  ----------------------------------------------------------------------------------------------
    apro  <dados no formato do ipro>
    ----  ----------------------------------------------------------------------------------------------      
    epro  cod         - codigo do cliente                  json de resposta      
    atpro prodcod     - codigo do produto
          instcod     - codigo da instituicao
    ----  ----------------------------------------------------------------------------------------------      
    dlpro cod         - codigo do cliente                  json de resposta
                                                           - cod            - codigo do cliente
                                                           - depcod         - codigo do dependente
                                                           - prodcod        - codigo do produto
                                                           - ativo          - indica produto ativo (S/N)
                                                           - dtvigencia     - data de vigencia (aaaa-mm-dd)
                                                           - isento         - isencao de mensalidade (S/N)
                                                           - desconto       - desconto em percentual (999.99)
                                                           - numcarteira    - numero da carteita (15 caracter)
                                                           - codconsultor   - codigo do consultor
                                                           - depnome        - nome do dependente
                                                           - prodnome       - nome do produto
                                                           - consultorlogin - login do consultor
                                                           - consultornome  - nome do consultor
    ----   ----------------------------------------------------------------------------------------------      
    dapro  - cod          - codigo do cliente               json de resposta
           - depcod       - codigo do dependente
           - prodcod      - codigo do produto
           - dtvigencia   - data de vigencia (aaaa-mm-dd)
           - numcarteira  - numero da carteita (15 caracter) 
           - desconto     - desconto em percentual (999.99)                                            
           - codconsultor - codigo do consultor                             
    ----   ----------------------------------------------------------------------------------------------                                
    datpro - cod          - codigo do cliente               json de resposta
           - depcod       - codigo do dependente
           - prodcod      - codigo do produto

********************************************************************************************************/
';
//http://179.184.216.103/ws/unibenjf_ws_cli_dsv.php?op=alt&cod=0&cpf=12345678903&nome=nometeste&log=1&num=1&bai=1&cep=36000100&sexo=M&dtn=2000-01-01&estc=1&mae=maeteste&rg=meurg&rgo=ssp&rgu=1&rgp=1&mpg=1&mpg=1&bc=033&diav=1&dtadm=2000-01-01&i01=1&u01=1&i02=0&u02=0&emp=0


require_once("./uClassFB.php"); 
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php");

//xdebug_disable();
error_reporting(E_ALL ^ E_WARNING); 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';

if ($vOper==='???') {
	$vResult= $vHelp;
} else {  
  require_once("./uOperacoesCli.php");
  
  if (in_array($vOper,array(CTECLIALT,CTECLIITEL,CTECLIATEL,CTECLIETEL,CTECLIIDEP,CTECLIADEP,CTECLIEDEP,
                            CTECLIIPRO,CTECLIAPRO,CTECLIEPRO,CTECLIATPRO,CTECLIDAPRO,CTECLIDATPRO))) {  
    $vResult = alteracao($vOper);
  } elseif (in_array($vOper,array(CTECLIVCPF,CTECLILER,CTECLILCPF,CTECLILTEL,CTECLILDEP,CTECLILPRO,CTECLIDLPRO,CTECLILISTARNOME))) {  
    $vResult = lerdados($vOper);
  }  
} 

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log_cli.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 

/**
  *Leitura de dados
*/

function lerDados($pOper) {    
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  $vRetornoArr['erro']= 'erro na abertura do BD';
    goto FINALLYY;
  }
  
	$vSQL= '';

  if (in_array($pOper,array(CTECLILER,CTECLILTEL,CTECLILDEP,CTECLILPRO,CTECLIDLPRO))) {  
    if (!isset($_REQUEST['cod'])) {
      $vRetornoArr['erro']= 'codigo nao informado';
      goto FINALLYY;
    }
  } elseif (in_array($pOper,array(CTECLILCPF,CTECLIVCPF))) {  
    if (!isset($_REQUEST['cpf'])) {
      $vRetornoArr['erro']= 'cpf nao informado';
      goto FINALLYY;
    }
  } elseif (in_array($pOper,array(CTECLILISTARNOME))) { 
    if (!isset($_REQUEST[PAR_CLI_NOME])) {
      $vRetornoArr['erro']= PAR_CLI_NOME.' nao informado';
      goto FINALLYY;
    }
    if (!isset($_REQUEST[PAR_CLI_POS])) {
      $vRetornoArr['erro']= PAR_CLI_POS.' nao informado';
      goto FINALLYY;
    }
  }  


  if ($pOper === CTECLIVCPF) {
    $vSQL = "select coalesce(CLIENTE.CLICODIGO,0) as CODIGO, coalesce(CLIENTE.CLIEMAIL,'-') as EMAIL ".
            "from CLIENTE ".
            "where CLIENTE.CLICPF=".quotedSingleStr($_REQUEST['cpf']);
  } elseif (in_array($pOper,array(CTECLILER,CTECLILCPF))) {  
    $vSQL = 'select CLI.CLISENHAWEB AS SENHA, CLI.CLICPF as "cpf",CLI.CLICODIGO as "cod",CLI.CLINOME as nome,CLI.LOGCODIGO as log,CLI.CLINUMERO as num,CLI.CLICOMPLEMENTO as compl,'.
                   'CLI.BAICODIGO as "bai",CLI.CLICEP as "cep",CLI.CLIEMAIL as email,CLI.CLICXPOSTAL as cxp,CLI.CLISEXO as sexo,CLI.CLIDATANASCIMENTO as dtn,'.
                   'CLI.ESTCCODIGO as "estc",CLI.CLINOMEMAE as mae,CLI.CLIRG as rg,CLI.CLIRGORGAOEMISSOR as rgo,CLI.CLIRGUFCODIGO as rgu,CLI.CLIRGPAISCODIGO as rgp,'.
                   'CLI.CLICARTAOSUS as "csus",CLI.CLIPISPASEP as pis,CLI.MODOPCODIGO as mpg,BANCO.BANCOCODIGO as bc,CLI.CLIAGENCIA as ag,CLI.CLICONTACORRENTE as cc,'.
                   'CLI.CLIDIAVENCIMENTO as diav,CLI.CLIDATAADMISSAO as dtadm,CLI.CLICOBCXPOSTAL as cobcp,CLI.INSTCODIGO as i01,CLI.INSTUNICODIGO as u01,'.
                   'CLI.CLISTATUS as "status",'.
                   'CLI.INSTCODIGO_2 as i02,CLI.INSTUNICODIGO_2 as u02,CLI.EMPCODIGO as emp,'. 
                   "coalesce(L.LOGNOME,'ND') as lognome,coalesce(TL.TLOGNOME,'ND') as tlognome,coalesce(B.BAINOME,'ND') as bainome,".
                   "coalesce(CID.CIDNOME,'ND') as cidnome,coalesce(U.UFSIGLA,'ND') as ufsigla ".          
            'from CLIENTE CLI '.
            'left outer join LOGRADOURO L on (CLI.LOGCODIGO = L.LOGCODIGO) '.
            'left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) '.
            'left outer join BAIRRO B on (CLI.BAICODIGO =  B.BAICODIGO) '.
            'inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) '.
            'inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) '.
            "inner join BANCO on (CLI.BANCOID=BANCO.BANCOID) ".
            'where '.(($pOper===CTECLILER) ? 'CLI.CLICODIGO='.$_REQUEST['cod']:'CLI.CLICPF='.quotedSingleStr($_REQUEST['cpf']));
  } elseif ($pOper === CTECLILTEL) {
    $vSQL = 'select CT.CLICODIGO as cod,CT.CLITDDD as ddd,CT.CLITNUMERO as tel,CT.CLITDESCRICAO as descr '.
            'from CLITELEFONE CT '.
            'where CT.CLICODIGO='.$_REQUEST['cod'];

  } elseif ($pOper === CTECLILDEP) {
    $vSQL = 'select CD.CLICODIGO as cod,CD.CLIDEPCODIGO as depcod,CD.CLIDEPCPF as depcpf,CD.CLIDEPNOME as depnome,CD.CLIDEPDATANASCIMENTO as depdtn,'.
                   'CD.CLIDEPSEXO as depsexo,CD.CLIDEPOBS as depobs,CD.ESTCCODIGO as depestc,CD.GPARCODIGO as depgpar,CD.CLIDEPNOMEMAE as depmae,'.
                   'CD.CLIDEPRG as deprg,CD.CLIDEPRGORGAOEMISSOR as deprgo,CD.CLIDEPRGUFCODIGO as deprgu,CD.CLIDEPRGPAISCODIGO as deprgp,'.
                   'CD.CLIDEPCARTAOSUS as depcsus,CD.CLIDEPNASCIDOVIVO as depnascviv,CD.CLIDEPISENTOMENSALIDADE as depimens,'. 
                   'GPAR.GPARNOME as depgparnome,EC.ESTCNOME as depestcnome '.
            'from CLIDEP CD '. 
            'inner join GRAUPARENTESCO GPAR on (CD.GPARCODIGO=GPAR.GPARCODIGO) '. 
            'inner join ESTADOCIVIL EC on (CD.ESTCCODIGO=EC.ESTCCODIGO) '.
            'where CD.CLICODIGO='.$_REQUEST['cod'];
    $vDepCod = (isset($_REQUEST['depcod'])) ? $_REQUEST['depcod'] : 0;
    $vSQL.= ($vDepCod != 0) ? ' and CD.CLIDEPCODIGO='.$vDepCod : '';
    
  } elseif ($pOper === CTECLILPRO) {
    $vSQL = 'select CP.CLICODIGO as cod,CP.PRODCODIGO as prodcod,CP.INSTCODIGO as instcod,CP.CLIDESCONTO as desconto,'.
                   'CP.CLIPRODDATAVIGENCIA as dtvigencia,CP.CLIPRODNUMCARTEIRA as numcarteira,CP.CLIPRODMODOCOB as modocob,'.
                   'CP.CLIPRODTXASSOC as txassoc,CP.CLIPRODNUMEROPROPOSTA as numeroproposta,'.
                   'CP.CLIPRODCONSULTOR as codconsultor,'.
                   'C.CLINOME,P.PRODNOME,I.INSTRAZAOSOCIAL,'.
                   "coalesce(U4.UNOME,'ND') as CONSULTORNOME,".
                   'coalesce(INSTPROD.INSTPRODMESREAJUSTE,0) as "mesreajuste" '.
            'from CLIPROD CP '.
            'inner join CLIENTE C on (CP.CLICODIGO=C.CLICODIGO) '.
            'inner join PRODUTO P on (CP.PRODCODIGO=P.PRODCODIGO) '.
            'inner join INSTITUICAO I on (CP.INSTCODIGO=I.INSTCODIGO) '.
            'inner join INSTPROD on (INSTPROD.INSTCODIGO=CP.INSTCODIGO) and (INSTPROD.PRODCODIGO=CP.PRODCODIGO) '.
            'left join USUARIO U4 on (U4.UCODIGO=CP.CLIPRODCONSULTOR) '.

            'where CP.CLICODIGO='.$_REQUEST['cod'];
    $vProdCod = (isset($_REQUEST['prodcod'])) ? $_REQUEST['prodcod'] : 0;
    $vSQL.= ($vProdCod != 0) ? ' and CP.PRODCODIGO='.$vProdCod : '';        

    $vInstCod = (isset($_REQUEST['instcod'])) ? $_REQUEST['instcod'] : 0;
    $vSQL.= ($vInstCod != 0) ? ' and CP.INSTCODIGO='.$vInstCod : '';        
  } elseif ($pOper === CTECLIDLPRO) {
    $vSQL = 'select CDP.CLICODIGO as "cod",CDP.CLIDEPCODIGO as "depcod",CDP.PRODCODIGO as "prodcod",CDP.CLIDEPCLIPRODATIVO as "ativo",'.
                   'CDP.CLIDEPCLIPRODDATAVIGENCIA as "dtvigencia", CDP.CLIDEPCLIPRODISENTOMENSALIDADE as "isento",CDP.CLIDEPCLIPRODDESCONTO as "desconto",'.
                   'CDP.CLIDEPCLIPRODNUMCARTEIRA as "numcarteira", CDP.CLIDEPCLIPRODCONSULTOR as "codconsultor",'.
                   'CD.CLIDEPNOME as "depnome",P.PRODNOME as "prodnome",'.
                   'I.INSTRAZAOSOCIAL,I.INSTCODIGO,CP.CLIPRODMODOCOB as modocob,CP.CLIPRODTXASSOC as txassoc,'.
                   'coalesce(U4.ULOGIN,\'ND\') as "consultorlogin",coalesce(U4.UNOME,\'ND\') as "consultornome" '.
            'from CLIDEPCLIPROD CDP '.
            'inner join CLIDEP CD on (CD.CLICODIGO=CDP.CLICODIGO) and (CD.CLIDEPCODIGO=CDP.CLIDEPCODIGO) '.
            'inner join CLIPROD CP on (CDP.CLICODIGO=CP.CLICODIGO and CDP.PRODCODIGO=CP.PRODCODIGO) '.
            'inner join INSTPROD IPROD on (IPROD.INSTCODIGO=CP.INSTCODIGO and IPROD.PRODCODIGO=CP.PRODCODIGO) '.
            'inner join INSTITUICAO I on (I.INSTCODIGO=IPROD.INSTCODIGO) '.
            'inner join PRODUTO P on (P.PRODCODIGO=CDP.PRODCODIGO) '.
            'left join USUARIO U4 on (U4.UCODIGO=CDP.CLIDEPCLIPRODCONSULTOR) '.
            'where CDP.CLICODIGO='.$_REQUEST['cod'];
  } elseif ($pOper === CTECLILISTARNOME) {
    $vSQL= 'select first 10 C.CLINOME,C.CLICPF,C.CLICODIGO '.
           'from CLIENTE C '.
           'where C.CLINOME like '.quotedSingleStr( ($_REQUEST[PAR_CLI_POS] === 'q' ? '%': '').$_REQUEST[PAR_CLI_NOME].'%' ).
           'order by 1';
  }  

  if ($vSQL === '') {
    if ($vRetornoArr['erro']==='')  {
    $vRetornoArr['erro'] = 'operacao desconhecida';
    }
    goto FINALLYY;
  }
  
 // return $vSQL;

  try {
	  if ($vAcessoBD->open($vSQL)) {
      if ($vAcessoBD->recordCount===0) {
        $vRetornoArr['erro']= 'sem dados';
      } else {
        $vRetornoArr['sucesso']=true;
        $vRetornoArr['dados']= $vAcessoBD->dataSetJSON();
      }  
    } else {
	    $vRetornoArr['erro']= $vAcessoBD->erro;
		}
  } catch (Exception $vExcecao) {
    $vRetornoArr['erro'] = 'exception: '.$vExcecao->getMessage();
	}  
  
  FINALLYY:
	unset($vAcessoBD);
  return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}


function alteracao($pOper) {
  $vRetornoArr = array("sucesso"=>false,"erro"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  $vRetornoArr['erro']= 'erro na abertura do BD';
    goto FINALLYY_ALL;
  }

  if (!isset($_REQUEST['cod'])) {
    $vRetornoArr['erro']= 'codigo nao informado';
    goto FINALLYY_ALL;
  }

  $vNovoId=-1;
  
  if ($pOper === CTECLIALT) {
    $vNovoId= $_REQUEST['cod']; 
    
	  if (!isset($_REQUEST['cpf'])) {
	    $vRetornoArr['erro']= 'cpf nao informado';
      goto FINALLYY_ALL;
	  }
    if ( (!isset($_REQUEST['cobcp'])) 
          || 
         (!in_array($_REQUEST['cobcp'],array('S','N'))) 
      ) {
      $vRetornoArr['erro']= 'cobcp invalido ou nao informado--';
      goto FINALLYY_ALL;
    } 
	} elseif (in_array($pOper,array(CTECLIITEL,CTECLIATEL,CTECLIETEL))) {  
    if (!isset($_REQUEST['ddd'])) {
      $vRetornoArr['erro']= 'ddd nao informado';
      goto FINALLYY_ALL;
    }
    if (!isset($_REQUEST['tel'])) {
      $vRetornoArr['erro']= 'tel nao informado';
      goto FINALLYY_ALL;
    }
  } elseif (in_array($pOper,array(CTECLIIDEP,CTECLIADEP,CTECLIEDEP,CTECLIDAPRO,CTECLIDATPRO))) {  
    if (!isset($_REQUEST['depcod'])) {
      $vRetornoArr['erro']= 'codigo dependente nao informado';
      goto FINALLYY_ALL;
    }
  } elseif (in_array($pOper,array(CTECLIIPRO,CTECLIAPRO,CTECLIEPRO,CTECLIATPRO))) {  
    if (!isset($_REQUEST['prodcod'])) {
      $vRetornoArr['erro']= 'codigo produto nao informado';
      goto FINALLYY_ALL;
    }
    if (!isset($_REQUEST['instcod'])) {
      $vRetornoArr['erro']= 'codigo instituicao nao informado';
      goto FINALLYY_ALL;
    }
  }

	$vSQL= '';
  $vAgora = new DateTime();

  try {
    $vAcessoBD->createTrans();
    
    if ($pOper === CTECLIALT) {
      if ($_REQUEST['cod'] == 0) {
        if ($pOper === CTECLIALT) {
          $vSQL = 'select CODIGO from SP_NOVOCODIGO(12)';   
        }
      
        if ($vAcessoBD->open($vSQL, false)) {
          $vNovoId= $vAcessoBD->dataSet[0]->CODIGO;
        } else {
          $vRetornoArr['erro'] = $vAcessoBD->erro;
          goto FINALLYY;    
        }
             
        $vSQL = 'insert into CLIENTE (CLICPF,CLICODIGO,CLINOME,LOGCODIGO,CLINUMERO,CLICOMPLEMENTO,BAICODIGO,CLICEP,CLIEMAIL,CLICXPOSTAL,'.
                                     'CLISEXO,CLIDATANASCIMENTO,ESTCCODIGO,CLINOMEMAE,CLIRG,CLIRGORGAOEMISSOR,CLIRGUFCODIGO,CLIRGPAISCODIGO,'.
                                     'CLICARTAOSUS,CLIPISPASEP,MODOPCODIGO,BANCOID,CLIAGENCIA,CLICONTACORRENTE,CLIDIAVENCIMENTO,'.
                                     'CLIDATAADMISSAO,CLICOBCXPOSTAL,INSTCODIGO,INSTUNICODIGO,INSTCODIGO_2,INSTUNICODIGO_2,EMPCODIGO,'.
                                     'CLIDATAMUDADIAVENC,CLIULTCOMPETMOVMENS,CLISTATUS,CLIDIAVENC_AGENDADO,'.
                                     'CLIDATACADASTRO,CLIDATAALTERACAO,CLIDATAULTALTSTATUS,CLISENHAWEB'. 
                                    ') '.
                 'values ('.quotedSingleStr($_REQUEST['cpf']).','.
                           $vNovoId.','.quotedSingleStr($_REQUEST['nome']).','.$_REQUEST['log'].','.$_REQUEST['num'].','.
                           quotedSingleStr((isset($_REQUEST['compl'])) ? $_REQUEST['compl'] : '').','.
                           $_REQUEST['bai'].','.quotedSingleStr($_REQUEST['cep']).','.
                           quotedSingleStr((isset($_REQUEST['email'])) ? $_REQUEST['email'] : '').','.
                           quotedSingleStr((isset($_REQUEST['cxp'])) ? $_REQUEST['cxp'] : '').','.
                           quotedSingleStr($_REQUEST['sexo']).','.quotedSingleStr($_REQUEST['dtn']).','.
                           $_REQUEST['estc'].','.quotedSingleStr($_REQUEST['mae']).','.
                           quotedSingleStr($_REQUEST['rg']).','.quotedSingleStr($_REQUEST['rgo']).','.
                           $_REQUEST['rgu'].','.$_REQUEST['rgp'].','.
                           quotedSingleStr((isset($_REQUEST['csus'])) ? $_REQUEST['csus'] : '').','.
                           quotedSingleStr((isset($_REQUEST['pis'])) ? $_REQUEST['pis'] : '').','.
                           $_REQUEST['mpg'].','. quotedSingleStr($_REQUEST['bc']).','.
                           quotedSingleStr((isset($_REQUEST['ag'])) ? $_REQUEST['ag'] : '').','.
                           quotedSingleStr((isset($_REQUEST['cc'])) ? $_REQUEST['cc'] : '').','.$_REQUEST['diav'].','.
                           quotedSingleStr($_REQUEST['dtadm']).','.
                           quotedSingleStr((isset($_REQUEST['cobcp'])) ? $_REQUEST['cobcp'] : 'N').','.
                           $_REQUEST['i01'].','.$_REQUEST['u01'].','.$_REQUEST['i02'].','.$_REQUEST['u02'].','.
                           $_REQUEST['emp'].','.quotedSingleStr('2014-01-01').','.quotedSingleStr('201401').','.
                           quotedSingleStr(CTECLISTATUSSUSPENSO).','.$_REQUEST['diav'].','.
                           quotedSingleStr($vAgora->format('Y-m-d h:i:s')).','.
                           quotedSingleStr($vAgora->format('Y-m-d h:i:s')).','.
                           quotedSingleStr($vAgora->format('Y-m-d h:i:s')).','.
                           quotedSingleStr(strtoupper(md5(substr($_REQUEST['dtn'],8,2).substr($_REQUEST['dtn'],5,2).substr($_REQUEST['dtn'],0,4)))).
                        ')';
      } else {
        $vSQL = 'update CLIENTE '.
                'set CLICPF='.quotedSingleStr($_REQUEST['cpf']).',CLINOME='.quotedSingleStr($_REQUEST['nome']).','.
                    'LOGCODIGO='.$_REQUEST['log'].',CLINUMERO='.$_REQUEST['num'].','.
                    'CLICOMPLEMENTO='.quotedSingleStr((isset($_REQUEST['compl'])) ? $_REQUEST['compl'] : '').','.
                    'BAICODIGO='.$_REQUEST['bai'].',CLICEP='.quotedSingleStr($_REQUEST['cep']).','.
                    'CLIEMAIL='.quotedSingleStr((isset($_REQUEST['email'])) ? $_REQUEST['email'] : '').','.
                    'CLICXPOSTAL='.quotedSingleStr((isset($_REQUEST['cxp'])) ? $_REQUEST['cxp'] : '').','.
                    'CLISEXO='.quotedSingleStr($_REQUEST['sexo']).',CLIDATANASCIMENTO='.quotedSingleStr($_REQUEST['dtn']).','.
                    'ESTCCODIGO='.$_REQUEST['estc'].',CLINOMEMAE='.quotedSingleStr($_REQUEST['mae']).','.
                    'CLIRG='. quotedSingleStr($_REQUEST['rg']).',CLIRGORGAOEMISSOR='.quotedSingleStr($_REQUEST['rgo']).','.
                    'CLIRGUFCODIGO='.$_REQUEST['rgu'].',CLIRGPAISCODIGO='.$_REQUEST['rgp'].','.
                    'CLICARTAOSUS='.quotedSingleStr((isset($_REQUEST['csus'])) ? $_REQUEST['csus'] : '').','.
                    'CLIPISPASEP='.quotedSingleStr((isset($_REQUEST['pis'])) ? $_REQUEST['pis'] : '').','.
                    'MODOPCODIGO='.$_REQUEST['mpg'].',BANCOID='.$_REQUEST['bc'].','.
                    'CLIAGENCIA='.quotedSingleStr((isset($_REQUEST['ag'])) ? $_REQUEST['ag'] : '').','.
                    'CLICONTACORRENTE='.quotedSingleStr((isset($_REQUEST['cc'])) ? $_REQUEST['cc'] : '').',CLIDIAVENCIMENTO='.$_REQUEST['diav'].','.
                    'CLIDATAADMISSAO='.quotedSingleStr($_REQUEST['dtadm']).','.
                    'CLICOBCXPOSTAL='.quotedSingleStr((isset($_REQUEST['cobcp'])) ? $_REQUEST['cobcp'] : 'N').','.
                    'INSTCODIGO='.$_REQUEST['i01'].',INSTUNICODIGO='.$_REQUEST['u01'].',INSTCODIGO_2='.$_REQUEST['i02'].',INSTUNICODIGO_2='.$_REQUEST['u02'].','.
                    'EMPCODIGO='.$_REQUEST['emp'].','.
                    'CLIDIAVENC_AGENDADO='.$_REQUEST['diav'].','.
                    'CLIDATAALTERACAO='.quotedSingleStr($vAgora->format('Y-m-d h:i:s')).' '.
                'where CLIENTE.CLICODIGO='.$_REQUEST['cod'];
      }
    } elseif ($pOper === CTECLIITEL)  {
      $vSQL = 'insert into CLITELEFONE (CLICODIGO, CLITDDD, CLITNUMERO, CLITDESCRICAO) '.
              'values ('.$_REQUEST['cod'].','.quotedSingleStr($_REQUEST['ddd']).','.quotedSingleStr($_REQUEST['tel']).','.
                       quotedSingleStr((isset($_REQUEST['desc'])) ? $_REQUEST['descr'] : '').')';
    } elseif ($pOper === CTECLIATEL)  {
      $vSQL = 'update CLITELEFONE set CLITDESCRICAO='.quotedSingleStr((isset($_REQUEST['desc'])) ? $_REQUEST['descr'] : '').' '.
              'where (CLICODIGO='.$_REQUEST['cod'].') and '.
                    '(CLITDDD='.quotedSingleStr($_REQUEST['ddd']).') and '.
                    '(CLITNUMERO='.quotedSingleStr($_REQUEST['tel']).')';
    } elseif ($pOper === CTECLIETEL)  {
      $vSQL = 'delete from CLITELEFONE '.
              'where (CLICODIGO='.$_REQUEST['cod'].') and '.
                    '(CLITDDD='.quotedSingleStr($_REQUEST['ddd']).') and '.
                    '(CLITNUMERO='.quotedSingleStr($_REQUEST['tel']).')';
    } elseif ($pOper === CTECLIIDEP)  {
      $vSQL = 'select CODIGO from SP_NOVOCODIGO(15)';   
      if ($vAcessoBD->open($vSQL, false)) {
        $vNovoId= $vAcessoBD->dataSet[0]->CODIGO;
      } else {
        $vRetornoArr['erro'] = $vAcessoBD->erro;
        goto FINALLYY;    
      }

      $vSQL = 'insert into CLIDEP (CLICODIGO, CLIDEPCODIGO,CLIDEPCPF, CLIDEPNOME, CLIDEPDATANASCIMENTO, CLIDEPSEXO, CLIDEPDATACADASTRO, CLIDEPDATAALTERACAO, CLIDEPOBS,'.
                                  'ESTCCODIGO, GPARCODIGO, CLIDEPNOMEMAE, CLIDEPRG, CLIDEPRGORGAOEMISSOR, CLIDEPRGUFCODIGO, CLIDEPRGPAISCODIGO, CLIDEPCARTAOSUS,'.
                                  'CLIDEPNASCIDOVIVO, CLIDEPSTATUS, CLIDEPISENTOMENSALIDADE) '.
              'values ('.quotedSingleStr($_REQUEST['cod']).','.$vNovoId.','.
                         quotedSingleStr($_REQUEST['depcpf']).','.quotedSingleStr($_REQUEST['depnome']).','.quotedSingleStr($_REQUEST['depdtn']).','.
                         quotedSingleStr($_REQUEST['depsexo']).','.quotedSingleStr($vAgora->format('Y-m-d h:i:s')).','.
                         quotedSingleStr($vAgora->format('Y-m-d h:i:s')).','.quotedSingleStr($_REQUEST['depobs']).','.
                         $_REQUEST['depestc'].','.$_REQUEST['depgpar'].','.quotedSingleStr($_REQUEST['depmae']).','.
                         quotedSingleStr($_REQUEST['deprg']).','.quotedSingleStr($_REQUEST['deprgo']).','.
                         $_REQUEST['deprgu'].','.$_REQUEST['deprgp'].','.
                         quotedSingleStr((isset($_REQUEST['depcsus'])) ? $_REQUEST['depcsus'] : '').','.
                         quotedSingleStr((isset($_REQUEST['depnascviv'])) ? $_REQUEST['depnascviv'] : '0').','.
                         quotedSingleStr(CTECLISTATUSATIVO).','.quotedSingleStr('N').
                     ')';
    } elseif ($pOper === CTECLIADEP)  {
      $vSQL = 'update CLIDEP '.
              'set CLIDEPCPF = '.quotedSingleStr($_REQUEST['depcpf']).','.
                  'CLIDEPNOME = '.quotedSingleStr($_REQUEST['depnome']).','.
                  'CLIDEPDATANASCIMENTO = '.quotedSingleStr($_REQUEST['depdtn']).','.
                  'CLIDEPSEXO = '.quotedSingleStr($_REQUEST['depsexo']).','.
                  'CLIDEPDATAALTERACAO = '.quotedSingleStr($vAgora->format('Y-m-d h:i:s')).','.
                  'CLIDEPOBS = '.quotedSingleStr($_REQUEST['depobs']).','.
                  'ESTCCODIGO = '.$_REQUEST['depestc'].','.        
                  'GPARCODIGO = '.$_REQUEST['depgpar'].','.
                  'CLIDEPNOMEMAE = '.quotedSingleStr($_REQUEST['depmae']).','.
                  'CLIDEPRG = '.quotedSingleStr($_REQUEST['deprg']).','.
                  'CLIDEPRGORGAOEMISSOR = '.quotedSingleStr($_REQUEST['deprgo']).','.
                  'CLIDEPRGUFCODIGO = '.$_REQUEST['deprgu'].','.
                  'CLIDEPRGPAISCODIGO = '.$_REQUEST['deprgp'].','.
                  'CLIDEPCARTAOSUS = '.quotedSingleStr((isset($_REQUEST['depcsus'])) ? $_REQUEST['depcsus'] : '').','.
                  'CLIDEPNASCIDOVIVO = '.quotedSingleStr($_REQUEST['depnascviv']).','.
                  'CLIDEPISENTOMENSALIDADE = '.quotedSingleStr($_REQUEST['depimens']).' '.
              'where CLICODIGO='.$_REQUEST['cod'].' and CLIDEPCODIGO='.$_REQUEST['depcod'];
    } elseif ($pOper === CTECLIEDEP) {
      $vSQL = 'delete from CLIDEP '.
              'where CLICODIGO='.$_REQUEST['cod'].' and CLIDEPCODIGO='.$_REQUEST['depcod'];
    } elseif ($pOper === CTECLIIPRO) {
      $vSQL = 'insert into CLIPROD (CLICODIGO,PRODCODIGO,INSTCODIGO,CLIDESCONTO,CLIPRODDATAVIGENCIA,CLIPRODMODOCOB,CLIPRODTXASSOC,'.
                                   'CLIPRODNUMCARTEIRA,CLIPRODCONSULTOR) '.
              'values ('.$_REQUEST['cod'].','.$_REQUEST['prodcod'].','.$_REQUEST['instcod'].','.$_REQUEST['desconto'].','.
                         quotedSingleStr($_REQUEST['dtvigencia']).','.quotedSingleStr($_REQUEST['modocob']).','.
                         quotedSingleStr($_REQUEST['txassoc']).','.quotedSingleStr($_REQUEST['numcarteira']).','.
                         $_REQUEST['codconsultor'].
                     ')';
    } elseif ($pOper === CTECLIAPRO) {
      $vSQL = 'update CLIPROD '.
              'set CLIDESCONTO='.$_REQUEST['desconto'].','.
                  'CLIPRODDATAVIGENCIA='.quotedSingleStr($_REQUEST['dtvigencia']).','.
                  'CLIPRODMODOCOB='.quotedSingleStr($_REQUEST['modocob']).','.
                  'CLIPRODTXASSOC='.quotedSingleStr($_REQUEST['txassoc']).','.
                  'CLIPRODNUMCARTEIRA='.quotedSingleStr($_REQUEST['numcarteira']).','.
                  'CLIPRODCONSULTOR='.$_REQUEST['codconsultor'].' '.
              'where CLICODIGO='.$_REQUEST['cod'].' and '.
                    'PRODCODIGO='.$_REQUEST['prodcod'].' and '.
                    'INSTCODIGO='.$_REQUEST['instcod'];
    } elseif ($pOper === CTECLIEPRO) {
      $vSQL = 'delete from CLIPROD '.
              'where CLICODIGO='.$_REQUEST['cod'].' and '.
                    'PRODCODIGO='.$_REQUEST['prodcod'].' and '.
                    'INSTCODIGO='.$_REQUEST['instcod'];
    
    } elseif ($pOper === CTECLIATPRO) {
      $vSQL = 'update CLIPROD '.
              "set CLIPRODATIVO='S' ".
              'where CLICODIGO='.$_REQUEST['cod'].' and '.
                    'PRODCODIGO='.$_REQUEST['prodcod'].' and '.
                    'INSTCODIGO='.$_REQUEST['instcod'];
    
    } elseif ($pOper === CTECLIDAPRO) {
      $vSQL = 'update CLIDEPCLIPROD '.
              'set CLIDEPCLIPRODDATAVIGENCIA='.quotedSingleStr($_REQUEST['dtvigencia']).','.
                  'CLIDEPCLIPRODNUMCARTEIRA='.quotedSingleStr($_REQUEST['numcarteira']).','.
                  'CLIDEPCLIPRODDESCONTO='.$_REQUEST['desconto'].','.
                  'CLIDEPCLIPRODCONSULTOR='.$_REQUEST['codconsultor'].' '.
              'where CLICODIGO='.$_REQUEST['cod'].' and '.
                    'PRODCODIGO='.$_REQUEST['prodcod'].' and '.
                    'CLIDEPCODIGO='.$_REQUEST['depcod']; 
    } elseif ($pOper === CTECLIDATPRO) {
      $vSQL = 'update CLIDEPCLIPROD '.
              "set CLIDEPCLIPRODATIVO='S',".
                  "CLIDEPCLIPRODAPROVCAD='N',CLIDEPCLIPRODAPROVFIN='N',CLIDEPCLIPRODAPROVOP='N',".
                  "CLIDEPCLIPRODDATACANC=Null ".
              'where CLICODIGO='.$_REQUEST['cod'].' and '.
                    'PRODCODIGO='.$_REQUEST['prodcod'].' and '.
                    'CLIDEPCODIGO='.$_REQUEST['depcod'];

    } 


    //return $vSQL;
    
    if ($vSQL ==='') {
      $vRetornoArr['erro'] = 'operacao desconhecida';
  	  goto FINALLYY;
  	}
 
	  if ($vAcessoBD->exec($vSQL,false)) {
      $vRetornoArr['sucesso'] = true;

    } else {  
      $vRetornoArr['erro'] = $vAcessoBD->erro;
      goto FINALLYY;
    }  

  } catch (Exception $vExcecao) {
    $vRetornoArr['erro'] = 'exception: '.$vExcecao->getMessage();
	}  
  
  FINALLYY:
  if ($vRetornoArr['sucesso']) {
    //$vAcessoBD->commit();     
    $vRetornoArr['codigo']= $vNovoId;

    if (in_array($pOper,array(CTECLIIDEP,CTECLIADEP,CTECLIEDEP))) { 
      $vSQL= 'execute procedure SP_ATUALIZACLIDEPCLIPROD('.$vNovoId.')';
      if (!$vAcessoBD->exec($vSQL,false)) {
        $vRetornoArr['erro'] = $vAcessoBD->erro;
      }  
    } 
    $vAcessoBD->commit();  

  } else {
    $vAcessoBD->rollback();    
  }  

  FINALLYY_ALL:
  unset($vAcessoBD);
  return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}


?>