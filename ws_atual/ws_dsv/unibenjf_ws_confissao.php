<?php

    // require_once("./uClassFB.php"); 
    // require_once("./uDefinicoes.php"); 
    // require_once("./connectDB.php");
    // require_once 'conexao.php';
    require_once 'transaction.php';

    // Transaction::open('firebird');
    // $conn = Transaction::get();

    header("Content-Type: application/json; charset=ISO-8859-1",true);

    $vHelp = '
    /*******************************************************************************

    - operacoes (parametro op)
    - ??? - help   
    -  boletosemaberto:       retorna todos boletos em aberto 

    op                        parametros                     retorno
    --                        -----------------------        -----------
    boletos_abertos           

    *******************************************************************************/
    ';

    $vResult = '';    
    $tipo    = '';
    $vSQL    = '';
    $conn = '';

    $vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';

    if ($vOper==='???') {
      $vResult= $vHelp;
    }

    //função para retornar entrada de 'acordo', em aberto caso houver
    function getEntradaEmAberto($request, $conn){
      try{
        if ($request['tipo'] == 'C') {
          $vSQL = "SELECT CR.CRECENTIDADECODIGO AS CODIGO,
                    CR.CRECDATA AS DATA,
                    CR.CRECCOMPETENCIA AS COMPETENCIA,
                    CR.CRECVALORAPAGAR AS VALOR,
                    CR.CRECOBSERVACAO AS DESCRICAO,
                    CR.CRECTCODIGO AS TIPO_LANC,
                    CR.UCODIGO AS UCODIGO
                    FROM CONTASARECEBER AS CR 
                    WHERE CR.CRECENTIDADECODIGO = ".$request['codigo']." 
                    AND CR.CRECTIPOENTIDADE = 'C' 
                    AND CRECTCODIGO = 4 
                    AND CRECSTATUS = 'AB'
                    ORDER BY CR.CRECDATA";
        }else{
          $vSQL = "SELECT CR.CRECENTIDADECODIGO AS CODIGO,
                    CR.CRECDATA AS DATA,
                    CR.CRECCOMPETENCIA AS COMPETENCIA,
                    CR.CRECVALORAPAGAR AS VALOR,
                    CR.CRECOBSERVACAO AS DESCRICAO,
                    CR.CRECTCODIGO AS TIPO_LANC,
                    CR.UCODIGO AS UCODIGO
                    FROM CONTASARECEBER AS CR 
                    WHERE CR.CRECENTIDADECODIGO = ".$request['codigo']." 
                    AND CR.CRECTIPOENTIDADE = 'E' 
                    AND CRECTCODIGO = 4 
                    AND CRECSTATUS = 'AB'
                    ORDER BY CR.CRECDATA";
        }

        $result = $conn->query($vSQL);
        $data = $result->fetchAll(PDO::FETCH_OBJ);
        $encode = json_encode($data);
        print_r($encode);
        $conn = Transaction::close();
      }catch(Exception $e){
        print 'EXCEPTION MESSAGE entrada_em_aberto => '.$e->getMessage();
        $conn = Transaction::close();
      }
    }

    //funções
    function getBoletosAberto($request, $conn){
      try{
        $vSQL = "SELECT CR.CRECSEQUENCIA AS ID,".
        " CR.CRECCOMPETENCIA AS COMPETENCIA,".
        " CR.CRECDATAVENCIMENTO AS VENCIMENTO,".
        " CR.CRECDATAVENCIMENTOORI AS VENCIMENTOCONTRATO,".
        " CR.CRECSTATUS AS STATUS,".
        " CR.CRECOBSERVACAO AS OBSERVACAO,".
        " CR.CRECDESCONTO AS DESCONTO,".
        " CR.CRECMORAMULTA AS MORAMULTA,".
        " CR.CRECMORAMULTAAPAGAR AS MULTA,".
        " CR.CRECTIPOENTIDADE AS TIPOENTIDADE,".
        " CR.CRECENTIDADECODIGO AS IDENTIDADE,".
        " CR.CRECMENSALIDADE AS MENSALIDADE,".
        " CR.CRECCOPARTICIPACAO AS COPARTICIPACAO,".
        " CR.CRECAVULSO AS AVULSO,".
        " CR.CRECVALORAPAGAR AS VALORAPAGAR,".
        " (SELECT SUM(CR.CRECVALORAPAGAR) AS TOTAL FROM CONTASARECEBER AS CR WHERE CR.CRECTIPOENTIDADE = '".$request['tipo']."' AND CR.CRECENTIDADECODIGO = ".$request['codigo']." AND CR.CRECSTATUS = 'AB' AND CR.CRECAVULSO <> 0) AS TOTAL".
        " FROM CONTASARECEBER AS CR ".
        " WHERE CR.CRECTIPOENTIDADE= '".$request['tipo']."' 
        AND CR.CRECENTIDADECODIGO=".$request['codigo']." 
        AND CR.CRECSTATUS <> 'PG' 
        AND CR.CRECSTATUS <> 'CA'
        AND CR.CRECSTATUS <> 'AC'
        ORDER BY CR.CRECDATAVENCIMENTO";//AND CR.CRECAVULSO = 0

        $result = $conn->query($vSQL);
        $data = $result->fetchAll(PDO::FETCH_OBJ);
        $encode = json_encode($data);
        print_r($encode);
        $conn = Transaction::close();
      }catch(Exception $e){
        print 'EXCEPTION MESSAGE boletos_abertos => '.$e->getMessage();
        $conn = Transaction::close();
      }
    }// END

    function getAvulsoAbertos($request, $conn){
      try{
        if ($request['tipo'] == 'C') {
          $vSQL = "SELECT AV.CLICODIGO AS CODIGO,
                  AV.CLIAVDATA AS DATA,
                  AV.CLIAVCOMPETENCIA AS COMPETENCIA,
                  AV.CLIAVVALOR AS VALOR,
                  AV.CLIAVDESCRICAO AS DESCRICAO,
                  AV.CLIAVEXCLUIDO AS STATUS,
                  CLI.CLIDIAVENCIMENTO AS VENCIMENTO FROM CLIAVULSO AS AV JOIN CLIENTE AS CLI ON CLI.CLICODIGO = AV.CLICODIGO WHERE AV.CLICODIGO = ".$request['codigo']." ORDER BY AV.CLIAVCOMPETENCIA";
        }else{
          $vSQL = "SELECT AV.EMPCODIGO AS CODIGO,
                  AV.EMPAVDATA AS DATA,
                  AV.EMPAVCOMPETENCIA AS COMPETENCIA,
                  AV.EMPAVVALOR AS VALOR,
                  AV.EMPAVDESCRICAO AS DESCRICAO,
                  AV.EMPAVEXCLUIDO AS STATUS,
                  EMP.EMPDIAVENCIMENTO AS VENCIMENTO FROM EMPAVULSO AS AV JOIN EMPRESA AS EMP ON EMP.EMPCODIGO = AV.EMPCODIGO WHERE AV.EMPCODIGO = ".$request['codigo']." ORDER BY AV.EMPAVCOMPETENCIA";
        }

        $result = $conn->query($vSQL);
        $data = $result->fetchAll(PDO::FETCH_OBJ);
        $encode = json_encode($data);
        print_r($encode);
        $conn = Transaction::close();
      }catch(Exception $e){
        print 'EXCEPTION MESSAGE boletos_abertos => '.$e->getMessage();
        $conn = Transaction::close();
      }
    }

    //função retorna todos os dados para geração de boleto e inserção no movimento mensal e avulso
    function getAllDados($request, $conn){
      //-- CAST(EMP.EMPRAZAOSOCIAL AS VARCHAR(200)) AS NOME,
      try {
        if ($request['CRECTIPOENTIDADE'] == 'C') {
          $vSQL = "SELECT 
          CLI.CLICODRESPFIN, 
          CLI.CLIDIAVENCIMENTO, 
          CLI.CLICPF AS CPF, 
          CLI.CLICODIGO AS CODIGO, 
          CLI.CLINOME AS NOME, 
          CLI.CLINUMERO AS NUM, 
          CLI.CLICOMPLEMENTO AS COMPL,
          CLI.CLICEP AS CEP, 
          CLI.CLIEMAIL AS EMAIL, 
          CLI.CLICXPOSTAL as CXP, 
          CLI.CLIRG AS RG, 
          CLI.CLIAGENCIA AS AG, 
          CLI.CLICONTACORRENTE AS CC, 
          BANCO.BANCOCODIGO AS BC, 
          B.BAINOME AS BAIRRO, 
          B.BAICODIGO as baicod,  
          coalesce(B.BAINOME,'ND') AS BAINOME, 
          coalesce(L.LOGNOME,'ND') AS lognome,
          L.LOGCODIGO AS logcod, 
          coalesce(TL.TLOGNOME,'ND') AS TLOGNOME, 
          coalesce(U.UFSIGLA,'ND') AS UFSIGLA, 
          U.UFCODIGO as ufcod, 
          coalesce(CID.CIDNOME,'ND') AS CIDNOME,
          CID.CIDCODIGO as cidcod,  
          CR.CRECCOMPETENCIA, 
          CR.CRECSEQUENCIA, 
          CR.CRECDATA, 
          CR.CRECTIPOENTIDADE, 
          CR.CRECENTIDADECODIGO, 
          CR.MODOPCODIGO, 
          CR.CRECMENSALIDADE, 
          CR.CRECCOPARTICIPACAO, 
          CR.CRECAVULSO, 
          CR.CRECVALORAPAGAR, 
          CR.CRECDATAVENCIMENTO, 
          CR.CRECDATAPAGAMENTO, 
          CR.CRECDESCONTO, 
          CR.CRECMORAMULTA, 
          CR.CRECVALORPAGO, 
          CR.CRECOBSERVACAO, 
          CR.UCODIGO, 
          CR.BANCOID, 
          CR.CRECSTATUS, 
          CR.CRECSEUNUMERO, 
          CR.UCODIGOOPER, 
          CR.CRECTXASSOC, 
          CR.CRECVIA, 
          CR.CRECMORAMULTAAPAGAR, 
          CR.CRECDATAVENCIMENTOORI, 
          CR.CRECSEUNUMEROORI, 
          CR.CRECNOSSONUMERO, 
          CR.CRECINSTRUCAO, 
          CR.BANCOBOLETOCODCONVENIO, 
          CR.BANCOBOLETOEMISSAOLOCAL, 
          CR.CRECID, 
          CR.CRECTCODIGO, 
          CLIP.PRODCODIGO, 
          INST.INSTCODIGO, 
          OP.OPCODIGO, 
          INSTP.INSTPRODTERMO, 
          INSTP.INSTPRODMODALIDADE 
          FROM CLIENTE CLI 
          LEFT JOIN LOGRADOURO L on (CLI.LOGCODIGO = L.LOGCODIGO) 
          LEFT JOIN TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
          LEFT JOIN BAIRRO B on (CLI.BAICODIGO =  B.BAICODIGO) 
          LEFT JOIN CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
          LEFT JOIN UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) 
          LEFT JOIN BANCO on (CLI.BANCOID=BANCO.BANCOID) 
          LEFT JOIN CONTASARECEBER AS CR ON CR.CRECENTIDADECODIGO = CLI.CLICODIGO 
          LEFT JOIN CLIPROD AS CLIP ON CLIP.CLICODIGO = CLI.CLICODIGO
          LEFT JOIN PRODUTO AS PROD ON PROD.PRODCODIGO = CLIP.PRODCODIGO
          LEFT JOIN INSTITUICAO AS INST ON INST.INSTCODIGO = CLI.INSTCODIGO
          LEFT JOIN OPERADORA AS OP ON OP.OPCODIGO = PROD.OPCODIGO
          LEFT JOIN INSTPROD AS INSTP ON INSTP.INSTCODIGO = INST.INSTCODIGO AND INSTP.PRODCODIGO = PROD.PRODCODIGO
          WHERE CRECTIPOENTIDADE = '".$request['CRECTIPOENTIDADE']."' AND CRECENTIDADECODIGO = ".$request['CRECENTIDADECODIGO']."  AND CRECSEQUENCIA = (SELECT MAX(CRECSEQUENCIA) FROM CONTASARECEBER WHERE CRECENTIDADECODIGO = ".$request['CRECENTIDADECODIGO'].")";
        }else{
          $vSQL = "SELECT  EMP.EMPDIAVENCIMENTO, 
          EMP.EMPCNPJ AS CNPJ, 
          EMP.EMPCODIGO AS CODIGO, 
          EMP.EMPRAZAOSOCIAL AS NOME, 
          EMP.EMPNUMERO AS NUM, 
          EMP.EMPCOMPLEMENTO AS COMPL,
          EMP.EMPCEP AS CEP, 
          EMP.EMPEMAIL AS EMAIL, 
          CR.CRECCOMPETENCIA, 
          CR.CRECSEQUENCIA, 
          CR.CRECDATA, 
          CR.CRECTIPOENTIDADE, 
          CR.CRECENTIDADECODIGO, 
          CR.MODOPCODIGO, 
          CR.CRECMENSALIDADE, 
          CR.CRECCOPARTICIPACAO, 
          CR.CRECAVULSO, 
          CR.CRECVALORAPAGAR, 
          CR.CRECDATAVENCIMENTO, 
          CR.CRECDATAPAGAMENTO, 
          CR.CRECDESCONTO, 
          CR.CRECMORAMULTA, 
          CR.CRECVALORPAGO, 
          CR.CRECOBSERVACAO, 
          CR.UCODIGO, 
          CR.BANCOID, 
          CR.CRECSTATUS, 
          CR.CRECSEUNUMERO, 
          CR.UCODIGOOPER, 
          CR.CRECTXASSOC, 
          CR.CRECVIA, 
          CR.CRECMORAMULTAAPAGAR, 
          CR.CRECDATAVENCIMENTOORI, 
          CR.CRECSEUNUMEROORI, 
          CR.CRECNOSSONUMERO, 
          CR.CRECINSTRUCAO, 
          CR.BANCOBOLETOCODCONVENIO, 
          CR.BANCOBOLETOEMISSAOLOCAL, 
          CR.CRECID, 
          CR.CRECTCODIGO,
          TL.TLOGNOME AS TLOGNOME,
          L.LOGNOME AS LOGNOME,
          B.BAINOME AS BAIRRO,
          CID.CIDNOME AS CIDNOME,
          U.UFSIGLA AS UFSIGLA
          FROM EMPRESA EMP 
          LEFT JOIN CONTASARECEBER AS CR ON CR.CRECENTIDADECODIGO = EMP.EMPCODIGO 
          LEFT JOIN LOGRADOURO L on (EMP.LOGCODIGO = L.LOGCODIGO) 
          LEFT JOIN TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
          LEFT JOIN BAIRRO B on (EMP.BAICODIGO =  B.BAICODIGO) 
          LEFT JOIN CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
          LEFT JOIN UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO)  
          WHERE CRECTIPOENTIDADE = '".$request['CRECTIPOENTIDADE']."' AND CRECENTIDADECODIGO = ".$request['CRECENTIDADECODIGO']."  AND CRECSEQUENCIA = (SELECT MAX(CRECSEQUENCIA) FROM CONTASARECEBER WHERE CRECENTIDADECODIGO = ".$request['CRECENTIDADECODIGO'].")";
        }

        $stmt = $conn->prepare($vSQL);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // print_r($data);
        // die();
        $obj = array();

        // print_r(json_encode($obj));
        // die();

        if ($request['CRECTIPOENTIDADE'] == 'C') {
          $encode = json_encode($data);
          print_r($encode);
        }else{
          foreach ($data[0] as $key => $value) {
            // echo $key.$value;
            if ($key == 'NOME') {
              array_push($obj, utf8_encode($value));
            }else{
              array_push($obj, $value);
            }
          }
          print_r(json_encode($obj));
        }

      } catch (PDOException  $e) {
        print 'EXCEPTION MESSAGE getAll_dados => '.$e->getMessage();
        $conn = Transaction::close();
      }
    }// END

    //função gerar nossonumero
    function gerarNossoNumero($conn){
      try {
        $vSQL = "SELECT GEN_ID(G_BOL_NOSSONUMERO,1) FROM RDB\$DATABASE";

        $data = $conn->query($vSQL);
        $result = $data->fetchAll(PDO::FETCH_OBJ);
        echo $encode = json_encode($result);
        // $conn = Transaction::close();
      } catch (Exception $e) {
        print 'EXCEPTION MESSAGE gerar_nossonumero => '.$e->getMessage();
        $conn = Transaction::close();
      }
    }// END

    //função para inserir no contas a receber
    function insertContasAReceber($request, $conn){
      try{
          $vSQL = 
          "insert into CONTASARECEBER ( 
          CRECCOMPETENCIA,
          CRECSEQUENCIA,
          CRECDATA,
          CRECTIPOENTIDADE,
          CRECENTIDADECODIGO,
          MODOPCODIGO,
          CRECMENSALIDADE,
          CRECCOPARTICIPACAO,
          CRECAVULSO,
          CRECVALORAPAGAR,
          CRECDATAVENCIMENTO,
          CRECDATAPAGAMENTO,
          CRECDESCONTO,
          CRECMORAMULTA,
          CRECVALORPAGO,
          CRECOBSERVACAO,
          UCODIGO,
          BANCOID,
          CRECSTATUS,
          CRECSEUNUMERO,
          UCODIGOOPER,
          CRECTXASSOC,
          CRECVIA,
          CRECMORAMULTAAPAGAR,
          CRECDATAVENCIMENTOORI,
          CRECSEUNUMEROORI,
          CRECNOSSONUMERO,
          CRECINSTRUCAO,
          BANCOBOLETOCODCONVENIO,
          BANCOBOLETOEMISSAOLOCAL,
          CRECID,
          CRECTCODIGO) values ( ".
          " '".$request['CRECCOMPETENCIA']."', ".
          " (SELECT COALESCE(MAX( CONTASARECEBER.CRECSEQUENCIA ),1)+1 AS SEQ FROM CONTASARECEBER), ".
          " '".$data = date('Y-m-d h:m:s')."', ".
          " '".$request['CRECTIPOENTIDADE']."', ".
          " ".$request['CRECENTIDADECODIGO'].", ".
          " ".$request['MODOPCODIGO'].", ".
          " ".$request['CRECMENSALIDADE'].", ".
          " ".$request['CRECCOPARTICIPACAO'].", ".
          " ".$request['CRECAVULSO'].", ".
          " ".$request['CRECVALORAPAGAR'].", ".
          " CURRENT_DATE, ".
          " NULL, ".
          " ".$request['CRECDESCONTO'].", ".
          " ".$request['CRECMORAMULTA'].", ".
          " 0, ".
          " '".$request['CRECOBSERVACAO']."', ".
          " ".$request['UCODIGO'].", ".
          " ".$request['BANCOID'].", ".
          " '".$request['CRECSTATUS']."', ".
          " (SELECT GEN_ID(G_BOL_SEUNUMERO,1) FROM RDB\$DATABASE), ".
          " ".$request['UCODIGO'].", ".
          " ".$request['CRECTXASSOC'].", ".
          " ".$request['CRECVIA'].", ".
          " ".$request['CRECMORAMULTAAPAGAR'].", ".
          " (SELECT EXTRACT(YEAR FROM CURRENT_DATE) || '-' || SUBSTRING(100+EXTRACT(MONTH FROM CURRENT_DATE) FROM 2 FOR 2) || '-' || CLIDIAVENCIMENTO FROM CLIENTE WHERE CLICODIGO = ".$request['CRECENTIDADECODIGO']."), ".
          " (SELECT GEN_ID(G_BOL_SEUNUMERO,0) FROM RDB\$DATABASE), ".
          " '".$request['CRECNOSSNUMERO']."', ".
          " '".$request['CRECINSTRUCAO']."', ".
          " '".$request['BANCOBOLETOCODCONVENIO']."', ".
          " '".$request['BANCOBOLETOEMISSAOLOCAL']."', ".
          " (SELECT GEN_ID(G_CRECID,1) FROM RDB\$DATABASE), ".
          " ".$request['CRECTCODIGO'].")";

          $retorno = $conn->exec($vSQL);

          return $retorno;

        }catch(Exception $e){
          print 'EXCEPTION MESSAGE insert_contasareceber => Linhas: '.$e->getLine().' Error: '.$e->getTraceAsString();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }// END

      //função responsável por trocar o status do boleto AB para AC
      function updateStatusAcordo($request, $conn){
        $id_boletos = explode('/', $request['ids']);
        try{
          for($i = 0; $i < count($id_boletos); $i++){
            $vSQL = " update CONTASARECEBER".
              " set CRECSTATUS = 'AC', ".
              " CRECOBSERVACAO = '".$request['CRECINSTRUCAO']."' ".
              " WHERE CRECENTIDADECODIGO = ".$request['CRECENTIDADECODIGO']." and CRECSEQUENCIA = ".$id_boletos[$i];
              $retorno = $conn->exec($vSQL);
          }
          return $retorno;
        }catch(Exception $e){
            print 'EXCEPTION MESSAGE insert_contasareceber => Linhas: '.$e->getLine().' Error: '.$e->getTraceAsString();
            $conn = Transaction::rollback();
            $conn = Transaction::close();
        }
      }

      //função responsável por inserir no movimento mensal
      function insertMovimentoMensal($request, $conn){
        try {
          if ($request['CRECTIPOENTIDADE'] == 'C') {
            $vSQL = "INSERT INTO CLIMOVIMENTACAO
              (CLICODIGO, 
              CLIMCOMPETENCIA, 
              FLANCTCODIGO, 
              CLIMSEQUENCIA, 
              CLIMDATA, 
              CLIDEPCODIGO, 
              PRODCODIGO, 
              INSTCODIGO, 
              CLIMQTD, 
              CLIMVALORUNITARIOORIGINAL, 
              CLIMDESCONTO, 
              CLIMVALORUNITARIOCOBRADO, 
              CLIMDESCRICAO, 
              CLIMVALORUNITARIOOPERADORA, 
              CLIMMODOCOB, 
              CLIMDATAGERACAO, 
              CLIMDIAVENCIMENTO, 
              OPCODIGO, 
              MODOPCODIGO, 
              EMPCODIGO, 
              BANCOBOLETOCODCONVENIO, 
              BANCOBOLETOEMISSAOLOCAL, 
              CRECID, 
              INSTPRODTERMO, 
              INSTPRODMODALIDADE, 
              BANCOID)
              VALUES(
              ".$request['CRECENTIDADECODIGO'].",
              '".$request['CRECCOMPETENCIA']."',
              8,
              (SELECT GEN_ID(G_ENTIDADEMOVIMENTACAO, 1) FROM RDB\$DATABASE),
              CURRENT_DATE,
              0,
              ".$request['PRODCODIGO'].",
              ".$request['INSTCODIGO'].",
              1,
              ".$request['CRECVALORAPAGAR'].",
              ".$request['CRECDESCONTO'].",
              ".$request['CRECVALORAPAGAR'].",
              '".$request['CRECOBSERVACAO']."',
              0,
              '".$request['CRECTIPOENTIDADE']."',
              '".$data = date('Y-m-d h:m:s')."',
              ".$request['CLIMDIAVENCIMENTO'].",
              ".$request['OPCODIGO'].",
              ".$request['MODOPCODIGO'].",
              0,
              '".$request['BANCOBOLETOCODCONVENIO']."',
              '".$request['BANCOBOLETOEMISSAOLOCAL']."',
              (SELECT GEN_ID(G_CRECID, 0) FROM RDB\$DATABASE),
              '".$request['INSTPRODTERMO']."',
              '".$request['INSTPRODMODALIDADE']."',
              ".$request['BANCOID'].")";
          }else{
            $vSQL = "INSERT INTO ENTIDADEMOVIMENTACAO
              (ENTMCOMPETENCIA, ENTMSEQUENCIA, ENTMCODIGO, ENTMTIPO, FLANCTCODIGO, ENTMDATA, ENTMQTD, ENTMVALORUNITARIOORIGINAL, 
              ENTMDESCONTO, ENTMVALORUNITARIOCOBRADO, ENTMDESCRICAO, ENTMMODOCOB, ENTMDATAGERACAO, ENTMDIAVENCIMENTO, MODOPCODIGO, 
              BANCOBOLETOCODCONVENIO, BANCOBOLETOEMISSAOLOCAL, CRECID, BANCOID)
              VALUES(
              '".$request['CRECCOMPETENCIA']."', 
              (SELECT GEN_ID(G_ENTIDADEMOVIMENTACAO, 1) FROM RDB\$DATABASE), 
              ".$request['CRECENTIDADECODIGO'].", 
              '".$request['CRECTIPOENTIDADE']."', 
              9, 
              CURRENT_DATE, 
              1, 
              ".$request['CRECVALORAPAGAR'].", 
              ".$request['CRECDESCONTO'].", 
              ".$request['CRECVALORAPAGAR'].", 
              '".$request['CRECOBSERVACAO']."', 
              '".$request['ENTMMODOCOB']."', 
              '".$data = date('Y-m-d h:m:s')."',
              ".$request['ENTMDIAVENCIMENTO'].", 
              ".$request['MODOPCODIGO'].", 
              '".$request['BANCOBOLETOCODCONVENIO']."', 
              '".$request['BANCOBOLETOEMISSAOLOCAL']."', 
              (SELECT GEN_ID(G_CRECID, 0) FROM RDB\$DATABASE), 
              ".$request['BANCOID'].")";
          }

          $retorno = $conn->exec($vSQL);

          return $retorno;

        } catch (Exception $e) {
          print 'EXCEPTION MESSAGE insert_movimentomensal => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }// END

      //função responsável por inserir na tabela avulso
      function insertAvulso($request, $conn){
        $valor = explode('/', $request['CLIAVVALOR']);
        try {
          if ($request['CRECTIPOENTIDADE'] == 'C') {
            
            for ($i = 0; $i <= $request['num_parcela']; $i++) { 

              $data_competencias = date('Ym', strtotime("{$i} month", strtotime(substr($request['CRECCOMPETENCIA'], 0, 4).substr($request['CRECCOMPETENCIA'], 4, 5))));

              $aux = $i == 0 ? $valor[0] : $valor[1];

              $aux_text = $i == 0 ? $request['CRECOBSERVACAO'] : 
              utf8_decode("WEB ACORDO - UNIBEN ADMINISTRADORA DE BENEFICIOS LTDA - Registro ANS: 41.852-8 | Acordo Parcela $i/{$request['num_parcela']} | Esse boleto refere-se a negociacao entre V. Sa. e a Uniben, referente aos debitos anteriores das competencias ".$request['competencias']." |, sendo que o mesmo foi divido em ".$request['num_parcela']." parcelas, onde o pagamento do primeiro boleto ratifica a negociacao antes citada.");

              $tipo_lancamento = $i == 0 ? 13 : 8;

              $vSQL = "INSERT INTO CLIAVULSO
                (CLICODIGO, 
                CLIAVDATA, 
                CLIAVCOMPETENCIA, 
                CLIAVVALOR, 
                CLIAVDESCRICAO, 
                INSTCODIGO, 
                PRODCODIGO, 
                FLANCTCODIGO, 
                UCODIGO)
                VALUES(
                ".$request['CRECENTIDADECODIGO'].",
                CURRENT_TIMESTAMP,
                '".$data_competencias."',
                ".$aux.",
                '".$aux_text."',
                ".$request['INSTCODIGO'].",
                ".$request['PRODCODIGO'].",
                ".$tipo_lancamento.",
                ".$request['UCODIGO'].")";

                $retorno = $conn->exec($vSQL);
                sleep(1);
            }

          }else{

            for ($i = 0; $i <= $request['num_parcela']; $i++) { 

              $data_competencias = date('Ym', strtotime("{$i} month", strtotime(substr($request['CRECCOMPETENCIA'], 0, 4).substr($request['CRECCOMPETENCIA'], 4, 5))));

              $aux = $i == 0 ? $valor[0] : $valor[1];

              $aux_text = $i == 0 ? $request['CRECOBSERVACAO'] : 

              utf8_decode("WEB ACORDO - UNIBEN ADMINISTRADORA DE BENEFICIOS LTDA - Registro ANS: 41.852-8 | Acordo Parcela $i/{$request['num_parcela']} | Esse boleto refere-se a negociacao entre V. Sa. e a Uniben, referente aos debitos anteriores das competencias ".$request['competencias']." |, sendo que o mesmo foi divido em ".$request['num_parcela']." parcelas, onde o pagamento do primeiro boleto ratifica a negociacao antes citada.");

              $tipo_lancamento = $i == 0 ? 14 : 9;

              $vSQL = "INSERT INTO EMPAVULSO
                (EMPCODIGO, EMPAVDATA, EMPAVCOMPETENCIA, EMPAVVALOR, EMPAVDESCRICAO, FLANCTCODIGO, UCODIGO)
                VALUES(
                ".$request['CRECENTIDADECODIGO'].", 
                CURRENT_TIMESTAMP,
                '".$data_competencias."', 
                '".$aux."', 
                '".$aux_text."', 
                '".$tipo_lancamento."', 
                ".$request['UCODIGO'].")";

                $retorno = $conn->exec($vSQL);
                sleep(1);
            }
          }
            return $retorno;
        } catch (Exception $e) {
          print 'EXCEPTION MESSAGE insert_avulso => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }

      function gerarBoleto($request, $conn){
        try {

          $vSQL = 
          "select CLIENTE.CLINOME as sac_nome,CLIENTE.CLICPF as sac_CPF,
          coalesce(TL.TLOGNOME,'ND') || ' ' || coalesce(L.LOGNOME,'ND') || ', 
          ' || CLIENTE.CLINUMERO || ' ' || coalesce(CLIENTE.CLICOMPLEMENTO,'',
          CLIENTE.CLICOMPLEMENTO) || '-' || coalesce(B.BAINOME,'ND') as sac_endereco,
          CLIENTE.CLICEP as sac_cep, coalesce(CID.CIDNOME,'ND') as sac_cidade,
          coalesce(U.UFSIGLA,'ND') as sac_uf, 
          CONTASARECEBER.CRECDATA, 
          CONTASARECEBER.CRECDATAVENCIMENTO as VENCIMENTOORIGINAL, 
          CONTASARECEBER.CRECDATAVENCIMENTO as VENCIMENTO,
          CONTASARECEBER.CRECVALORAPAGAR as valorapagar,
          'UNIBEN ADMINISTRADORA DE BENEFICIOS LTDA' as ced_nome,
          'Rua Braz Bernardino, 192 - Centro' as ced_endereco,'36010320' as ced_cep,
          'Juiz de Fora' as ced_cidade,'MG' as ced_uf,
          '15.156.097/0001-01' as ced_CNPJ,
          CONTASARECEBER.CRECNOSSONUMERO as nossonumero,
          CONTASARECEBER.CRECINSTRUCAO as instrucao,
          CONTASARECEBER.CRECSEUNUMERO as seunumero,
          BANCO.BANCOCODIGO as banco, BANCO.BANCOAGENCIA as agencia, BANCO.BANCOCONTACORRENTE as contacorrente,
          CONTASARECEBER.BANCOBOLETOCODCONVENIO as convenio,BANCO.BANCODIASVALIDADEBOLETO,
          CONTASARECEBER.CRECENTIDADECODIGO as codigo, 0 as multa 
          from CONTASARECEBER 
          inner join CLIENTE on (CLIENTE.CLICODIGO=CONTASARECEBER.CRECENTIDADECODIGO) 
          left outer join LOGRADOURO L on (CLIENTE.LOGCODIGO = L.LOGCODIGO) 
          left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
          left outer join BAIRRO B on ( CLIENTE.BAICODIGO =  B.BAICODIGO) 
          inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
          inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) 
          inner join BANCO on (CONTASARECEBER.BANCOID=BANCO.BANCOID) 
          where CONTASARECEBER.CRECENTIDADECODIGO = ".$request['codigo']."
          and CONTASARECEBER.CRECTIPOENTIDADE= '".$request['tipo']."'
          and CONTASARECEBER.CRECNOSSONUMERO= '".$request['nossonumero']."'";

          $result = $conn->query($vSQL);
          $data = $result->fetchAll(PDO::FETCH_OBJ);
          $encode = json_encode($data);
          print_r($encode);
          $conn = Transaction::close();
          
        } catch (Exception $e) {
          print 'EXCEPTION MESSAGE gerarBoleto => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
        
      }

      function getEmail($request, $conn){
        try{
          if ($request['tipo'] == 'C') {
            $vSQL = "select CLIENTE.CLIEMAIL AS EMAIL from CLIENTE where CLIENTE.CLICODIGO= ".$request['codigo']."";
          } else {
            $vSQL = "select EMPRESA.EMPEMAIL AS EMAIL from EMPRESA where EMPRESA.EMPCODIGO= ".$request['codigo']."";
          }

          $result = $conn->query($vSQL);
          $data = $result->fetchAll(PDO::FETCH_OBJ);
          $encode = json_encode($data);
          print_r($encode);
          $conn = Transaction::close();
        }catch(Exception $e){
          print 'EXCEPTION MESSAGE get_email => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }

      function getDataExcluido($request, $conn){
        try{
          if ($request['tipo'] == 'C') {
            $vSQL = "SELECT CLISTATUS, CLIDATAULTALTSTATUS FROM CLIENTE WHERE CLICODIGO = ".$request['codigo']."";
          } else {
            $vSQL = "SELECT CLISTATUS, CLIDATAULTALTSTATUS FROM CLIENTE WHERE EMPCODIGO = ".$request['codigo']."";
          }

          $result = $conn->query($vSQL);
          $data = $result->fetchAll(PDO::FETCH_OBJ);
          $encode = json_encode($data);
          print_r($encode);
          $conn = Transaction::close();
        }catch(Exception $e){
          print 'EXCEPTION MESSAGE get_data_excluido => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }

      function insertObservacao($request, $conn){
        try{
          if ($request['CRECTIPOENTIDADE'] == 'C') {
            
            $vSQL = "INSERT INTO CLIOBS
                    (CLICODIGO, CLIOBSDATA, CLIOBSOBS, UCODIGO, CLIOBSTIPO)
                    VALUES(
                    ".$request['CRECENTIDADECODIGO'].", 
                    CURRENT_TIMESTAMP, 
                    '".$request['CRECOBSERVACAO']."', 
                    113, 
                    'O')";
          } else {
            
            $vSQL = "INSERT INTO EMPOBS
                    (EMPCODIGO, EMPOBSDATA, EMPOBSOBS, UCODIGO)
                    VALUES(
                    ".$request['CRECENTIDADECODIGO'].", 
                    CURRENT_TIMESTAMP, 
                    '".$request['CRECOBSERVACAO']."', 
                    113)";
          }

          $retorno = $conn->exec($vSQL);

          return $retorno;

        }catch(Exception $e){
          print 'EXCEPTION MESSAGE insert_observacao => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
        
      }


      function updateAvulsoStatus($request, $conn){
        try{
          
          $dataIds = explode('/', $request['compAvulso']);

          if ($request['CRECTIPOENTIDADE'] == 'C') {

            for ($i=0; $i < count($dataIds); $i++) { 
              $vSQL= " UPDATE CLIAVULSO SET CLIAVEXCLUIDO = 'S' WHERE CLICODIGO = ".$request['CRECENTIDADECODIGO']." AND CLIAVDATA='".$dataIds[$i]."'";
              $retorno = $conn->exec($vSQL);
            }

          } else {

            for ($i=0; $i < count($dataIds); $i++) { 
              $vSQL= "UPDATE EMPAVULSO SET EMPAVEXCLUIDO = 'S' WHERE EMPCODIGO = ".$request['CRECENTIDADECODIGO']." AND EMPAVDATA='".$dataIds[$i]."'";
              $retorno = $conn->exec($vSQL);
            }

          }

          return $retorno;

        } catch(Exception $e) {
          print 'Exception message update_avulso_status => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }

      function getCrecIds($request, $conn) {
        try{
          
          $vSQL = "SELECT CRECID FROM CONTASARECEBER WHERE CRECENTIDADECODIGO = ".$request['CRECENTIDADECODIGO']." AND CRECSTATUS = 'AB'";

          $stmt = $conn->prepare($vSQL);
          $stmt->execute();
          $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

          $retorno = json_encode($data);

          return $retorno;

        } catch(Exception $e) {
          print 'Exception message verificaCrecIdNoMM => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }

      // função que retornar se existe os crecid no movimento mensal
      function verificaCrecIdNoMM($request, $conn) {
        try{
          $retorno = getCrecIds($request, $conn);

          $array = json_decode($retorno);
          $aux = "";

          for ($i=0; $i < count($array); $i++) { 
            if ($i == 0) {
              $aux .= $array[$i]->CRECID;
            } else if ($i == count($array)) {
              $aux .= $array[$i]->CRECID;
            } else {
              $aux .= ', '.$array[$i]->CRECID;
            }
          }
          
          if ($request['CRECTIPOENTIDADE'] == 'C') {

            $vSQL = "SELECT CLIMCOMPETENCIA AS COMPETENCIA, 
                    CLIMDATA AS DATA, 
                    CLIMDATAGERACAO AS DATAGERACAO, 
                    CRECID AS ID, 
                    CLIMVALORUNITARIOCOBRADO  AS VALOR 
                    FROM CLIMOVIMENTACAO WHERE CLICODIGO = ".$_REQUEST['CRECENTIDADECODIGO']." AND CRECID IN (".$aux.")";

          } else {

            $vSQL = "SELECT ENTMCOMPETENCIA AS COMPETENCIA, 
            ENTMDATA AS DATA, 
            ENTMDATAGERACAO AS DATAGERACAO, 
            CRECID AS ID, 
            ENTMVALORUNITARIOCOBRADO AS VALOR
            FROM ENTIDADEMOVIMENTACAO WHERE ENTMCODIGO = ".$_REQUEST['CRECENTIDADECODIGO']." AND CRECID IN (".$aux.")";

          }

          $stmt = $conn->prepare($vSQL);
          $stmt->execute();
          $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

          $retorno = json_encode($data);

          print_r($retorno);

        } catch(Exception $e) {
          print 'Exception message verificaCrecIdNoMM => '.$e->getMessage();
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
      }

   if($vOper !=''){

    
    if ($_REQUEST['op'] === 'get_email') {
      Transaction::open('firebird');
      $conn = Transaction::get();
      getEmail($_REQUEST, $conn);  
      $conn = Transaction::commit();
      $conn = Transaction::close();  
    }// END

    if ($_REQUEST['op'] === 'get_data_excluido') {
      Transaction::open('firebird');
      $conn = Transaction::get();
      getDataExcluido($_REQUEST, $conn);  
      $conn = Transaction::commit();
      $conn = Transaction::close();  
    }// END

    if ($_REQUEST['op'] === 'entrada_em_aberto') {
      Transaction::open('firebird');
      $conn = Transaction::get();
      getEntradaEmAberto($_REQUEST, $conn);  
      $conn = Transaction::commit();
      $conn = Transaction::close();  
    }// END

    if($_REQUEST['op'] === 'boletos_abertos'){
      Transaction::open('firebird');
      $conn = Transaction::get();
      getBoletosAberto($_REQUEST, $conn);  
      $conn = Transaction::commit();
      $conn = Transaction::close();    
    }// END

    if($_REQUEST['op'] === 'avulsos_abertos'){
      Transaction::open('firebird');
      $conn = Transaction::get();
      getAvulsoAbertos($_REQUEST, $conn);
      $conn = Transaction::commit();
      $conn = Transaction::close(); 
    }

    if ($_REQUEST['op'] === 'gerar_nossonumero') {
      Transaction::open('firebird');
      $conn = Transaction::get();
      gerarNossoNumero($conn);
      $conn = Transaction::commit();
      $conn = Transaction::close();
    }// END

    if($_REQUEST['op'] === 'getAll_dados'){
      Transaction::open('firebird');
      $conn = Transaction::get();
      getAllDados($_REQUEST, $conn);
      $conn = Transaction::commit();
      $conn = Transaction::close();
    }// END

    if($_REQUEST['op'] === 'gerar_boleto'){
      Transaction::open('firebird');
      $conn = Transaction::get();
      gerarBoleto($_REQUEST, $conn);
      $conn = Transaction::commit();
      $conn = Transaction::close();

    }

    if($_REQUEST['op'] === 'update_avulso') {
        Transaction::open('firebird');
        $conn = Transaction::get();
        updateAvulsoStatus($_REQUEST, $conn);
        $conn = Transaction::commit();
        $conn = Transaction::close();
    }

    if($_REQUEST['op'] === 'verifica_crecid_mm') {
      Transaction::open('firebird');
      $conn = Transaction::get();
      verificaCrecIdNoMM($_REQUEST, $conn);
      $conn = Transaction::commit();
      $conn = Transaction::close();
    }

    if ($_REQUEST['op'] === 'insert_geral') {
      try {
        Transaction::open('firebird');
        $conn = Transaction::get();


        $cr = insertContasAReceber($_REQUEST, $conn);
        $up = updateStatusAcordo($_REQUEST, $conn);
        $av = insertAvulso($_REQUEST, $conn);
        $imp = insertMovimentoMensal($_REQUEST, $conn);
        $obs = insertObservacao($_REQUEST, $conn);

        if ($_REQUEST['compAvulso'] != '') {
            $avS = updateAvulsoStatus($_REQUEST, $conn);
        }

        if ($cr && $up && $imp && $av && $obs) {
          echo 1;
        }else{
          echo 0;
          $conn = Transaction::rollback();
          $conn = Transaction::close();
        }
        
        $conn = Transaction::commit();
        $conn = Transaction::close();
      } catch(Exception $e){
        print 'EXCEPTION MESSAGE insert_geral => Linhas: '.$e->getLine().' Error: '.$e->getTraceAsString();
        $conn = Transaction::rollback();
        $conn = Transaction::close();
      }
    }


    $fp = fopen("./log/log_confissao_dividas.txt", "a");
    $escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$vSQL."|".$vResult.PHP_EOL);
    fclose($fp); 
  }

?>