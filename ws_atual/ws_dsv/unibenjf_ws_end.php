<?php
/*******************************************************************************
 * unibenjf_ws_end.php
    - processa as requisicoes da endereco da unibenjf
    
  http://unibenjf.com.br/corretor  
 */
 
$vHelp = '
/*******************************************************************************
  - operacoes (parametro op)
    - ??? - help   
    -  1  - lista de UFs
    -  2  - lista de tipos de logradouros
    -  3  - lista de cidades
    -  4  - lista bairros
    -  5  - lista logradouros
    -  lpai  - lista paises
    -  ccid  - cadastrar cidade
    -  cbai  - cadastrar bairro
    -  clog  - cadastrar logradouro
     
  - parametros
 
    op    op2                      op3                    retorno
    --    -----------------------  ----------             -----------
    01    sem uso                 sem uso                lista de UFs
    02    sem uso                 sem uso                lista de tipos de logradouros
    03    codigo UF               sem uso                lista de cidades
    04    codigo cidade           filtro parte do nome   lista de bairros        
                                 (opcional)        
    05    codigo cidade           filtro parte do nome   lista de logradouros    
                                 (opcional)
          
          Parametros                                      retorno
          -------------------------------------------------------------------------------  
    lpai  <nenhum>                                        lista de paises
    ccid  nome - nome do cidade                           codigo da cidade criada 
          uf - codigo da uf
    cbai  nome - nome do bairro                           codigo do bairro
          cid - codigo da cidade
    clog  nome - nome do logradouro                       codigo da logradouro
          tlog - codigo do tipo de logradouro
          cid - codigo da cidade     
        
*******************************************************************************/
';
 
require_once("./uClassFB.php"); 
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php");

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';
$vOper2 = (isset($_REQUEST['op2'])) ? $_REQUEST['op2'] : '';
$vOper3 = (isset($_REQUEST['op3'])) ? $_REQUEST['op3'] : '';

if ($vOper==='???') {
	$vResult= $vHelp;
} else {  
  require_once("./uOperacoesEnd.php");
  
  if (in_array($vOper,array(CTELISTARUF,CTELISTARTLOG,CTELISTARCID,CTELISTARBAI,CTELISTARLOG,CTELISTARPAIS))) {
    $vResult = lerDados($vOper,$vOper2,$vOper3);
  } elseif (in_array($vOper,array(CTECADCID,CTECADBAI,CTECADLOG))) {
    $vResult = cadastro($vOper);
  }  
} 

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log_end.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 

function lerDados($pOper,$pOper2,$pOper3) {    
  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
  
  if (in_array($pOper,array(CTELISTARCID,CTELISTARBAI,CTELISTARLOG))) {
	  if ($pOper2 === '') {
	    $vRetorno= 'Requisicao invalida2';
	    goto FINALLYY;
	  }
	} 
  
  $vRetorno='';
	$vSQL= '';
  
  if ($pOper === CTELISTARUF) {
    $vSQL = "select UFCODIGO,UFNOME ".
            "from UNIDADEFEDERATIVA ".
            "order by UFNOME";
  } elseif ($pOper === CTELISTARTLOG) { 
    $vSQL = "select TLOGCODIGO,TLOGNOME ".
           "from TIPOLOGRADOURO ".
           "order by TLOGNOME";  
  } elseif ($pOper === CTELISTARCID) {
    $vSQL = "select CIDCODIGO,CIDNOME ".
            "from CIDADE ".
            "where UFCODIGO=".$pOper2.
            " order by CIDADE.CIDNOME"; 
  } elseif ($pOper === CTELISTARBAI) {
    $vSQL = "select BAICODIGO,BAINOME ".
            "from BAIRRO ".
            "where CIDCODIGO=".$pOper2.
            (($pOper3==='')?'':" and BAIRRO.BAINOME like '%".$pOper3."%'").
            " order by BAIRRO.BAINOME"; 
  } elseif ($pOper === CTELISTARPAIS) {
    $vSQL = "select PAISCODIGO,PAISNOME ".
            "from PAIS ".
            "order by PAISNOME";           
  } elseif ($pOper === CTELISTARLOG) {
    $vSQL = "select L.LOGCODIGO,L.LOGNOME,L.TLOGCODIGO,TL.TLOGNOME ".
            "from LOGRADOURO L ".
            "inner join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) ".
            "where L.CIDCODIGO=".$pOper2.
            (($pOper3==='')?'':" and L.LOGNOME like '%".$pOper3."%'").
            " order by L.LOGNOME";  
  }
  
  //return $vSQL;
  
  if ($vSQL ==='') {
	  goto FINALLYY;
	}
  
  try {
	  if ($vAcessoBD->open($vSQL)) {
      if ($vAcessoBD->recordCount===0) {
        $vRetorno = 'Sem dados';
        goto FINALLYY;
      }  
      $vRetorno= $vAcessoBD->dataSetJSON();
    } else {
	    return 'ERRO: '.$vAcessoBD->erro.'---'.$vSQL;		
		}
  } catch (Exception $pExcecao) {
	  $vRetorno= 'ERRO: Excecao: '.$pExcecao->getMessage();
	}  
  
  FINALLYY:
	unset($vAcessoBD);
  return $vRetorno;
}

function cadastro($pOper) {
  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
  
  /*
  if (in_array($pOper,array(CTELISTARCID,CTELISTARBAI,CTELISTARLOG))) {
	  if ($pOper2 === '') {
	    $vRetorno= 'Requisicao invalida2';
	    goto FINALLYY;
	  }
	} 
  */
  $vRetorno='';
	$vSQL= '';
  
  $vAcessoBD->createTrans();
  
  if ($pOper === CTECADCID) {
    $vSQL = 'select CODIGO from SP_NOVOCODIGOENDERECO(2)';   
  } elseif ($pOper === CTECADBAI) { 
   $vSQL = 'select CODIGO from SP_NOVOCODIGOENDERECO(3)';    
  } elseif ($pOper === CTECADLOG) { 
    $vSQL = 'select CODIGO from SP_NOVOCODIGOENDERECO(4)';    
  }
  
  if ($vAcessoBD->open($vSQL, false)) {
    $vNovoId= $vAcessoBD->dataSet[0]->CODIGO;
  } else {
    $vRetorno = 'ERRO gen: '.$vAcessoBD->erro;
    goto FINALLYY;    
  }
  
  if ($pOper === CTECADCID) {
    $vSQL = 'insert into CIDADE (CIDCODIGO, CIDNOME, UFCODIGO) values ('.$vNovoId.','.quotedSingleStr($_REQUEST['nome']).','.$_REQUEST['uf'].')';
  } elseif ($pOper === CTECADBAI) { 
    $vSQL = 'insert into BAIRRO (BAICODIGO, BAINOME, CIDCODIGO) values ('.$vNovoId.','.quotedSingleStr($_REQUEST['nome']).','.$_REQUEST['cid'].')';
  } elseif ($pOper === CTECADLOG) { 
    $vSQL = 'insert into LOGRADOURO (LOGCODIGO, LOGNOME, TLOGCODIGO, CIDCODIGO) '.
            'values ('.$vNovoId.','.quotedSingleStr($_REQUEST['nome']).','.$_REQUEST['tlog'].','.$_REQUEST['cid'].')';
  }
    
  //return $vSQL;
  
  if ($vSQL ==='') {
	  goto FINALLYY;
	}
  
  try {
	  if (!$vAcessoBD->exec($vSQL,false)) {
      $vRetorno = 'ERRO grv: '.$vAcessoBD->erro;
      goto FINALLYY;
    }  
  } catch (Exception $pExcecao) {
	  $vRetorno= 'ERRO: Excecao: '.$pExcecao->getMessage();
	}  
  
  FINALLYY:
  if ($vRetorno === '') {
    $vAcessoBD->commit();     
    $vRetorno= $vNovoId;
  } else {
    $vAcessoBD->rollback();    
  }  
  unset($vAcessoBD);
  return $vRetorno;
}



?>