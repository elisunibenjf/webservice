<?php
/*
	Operações
*/
define("CTEEMPAUT",'aut');
define("CTEEMPCNPJEMAIL",'cnpje');

define("CTEEMPALT",'alt');
define("CTEEMPVCNPJ",'vcnpj');
define("CTEEMPLER",'ler');

define("CTEEMPLCON",'ltel');
define("CTEEMPICON",'itel');
define("CTEEMPACON",'atel');
define("CTEEMPECON",'etel');
define("CTEEMPCOMPETCLIVIDA",'ccv');
define("CTEEMPMOVFIN",'mf');
define("CTEEMPBOLETO2AVIA",'b2v');
define("CTEEMPEXTRATOIR",'ir');

define("CTEMUDASENHACOD",'msc');

define("CTEEMPLISTARNOME",'lstn');
define("CTEEMPLISTARRAZSOCIAL",'lstr');

define("CTECONTAARECEBERPAGA",'PG');

define("BUSEMPCOD", "bempcod");

define("CNPJEMAIL", "cpnjfemail");


/*
	Parâmetros
*/
define("PAR_EMP_USUARIO",'u');
define("PAR_EMP_SENHA",'s');
define("PAR_EMP_COMPET",'compet');
define("PAR_EMP_ANO",'ano');

define("PAR_EMP_COD",'cod');
define("PAR_EMP_CNPJ",'cnpj');
define("PAR_EMP_NOME",'nome');
define("PAR_EMP_RAZAO",'razao');
define("PAR_EMP_LOG",'log');
define("PAR_EMP_NUM",'num');
define("PAR_EMP_COMPL",'compl');
define("PAR_EMP_BAI",'bai');
define("PAR_EMP_CID",'cid');
define("PAR_EMP_UF",'uf');
define("PAR_EMP_CEP",'cep');
define("PAR_EMP_EMAIL",'email');
define("PAR_EMP_CXP",'cxp');
define("PAR_EMP_DTF",'dtf');
define("PAR_EMP_OBS",'obs');
define("PAR_EMP_MODOPAGTO",'mpg');
define("PAR_EMP_AGE",'age');
define("PAR_EMP_BC",'bc');
define("PAR_EMP_DIAV",'diav');
define("PAR_EMP_CC",'cc');
define("PAR_EMP_NCC",'ncc');
define("PAR_EMP_COBCP",'cobcp');
define("PAR_EMP_INS",'ins');
define("PAR_EMP_DMV",'dmv');

define("PAR_EMP_CON_NOME",'nome');
define("PAR_EMP_CON_DDD",'ddd');
define("PAR_EMP_CON_TEL",'tel');
define("PAR_EMP_CON_CARGO",'cargo');
define("PAR_EMP_CON_EMAIL",'email');

define("PAR_EMP_NOSSONUM",'ns');
define("PAR_EMP_POS",'pos');


?>