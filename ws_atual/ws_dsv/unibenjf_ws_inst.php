<?php
/*******************************************************************************
 * unibenjf_ws_inst.php
    - processa as requisicoes de empresa

 */
 
$vHelp = '
/*******************************************************************************************************
  - operacoes (parametro op)
    - ??? - help   
    - ler  - le os dados cadastrais
     
    op    Parametros                                        retorno
    ====  ==============================================================================================
    ler   cod   - codigo da empresa                         json de resposta: sucesso,erro,codigo da empresa incluido 
                                                              cnpj  - cnpj (14 caractetes sem formatacao )       
                                                              nome  - nome(max 100 caracteres	)
                                                              razao - razao social nome(max 100 caracteres)		  
                                                              log   - codigo do logradouro
                                                              num   - numero do endereco
                                                              compl - complemento endereco (max 20 caracteres)
                                                              bai   - codigo do bairro
                                                              cep   - cep (8 caracter)
                                                              lognome - logradouro
                                                              tlognome- tipo do logradouro
                                                              bainome - bairro
                                                              cidnome - cidade
                                                              ufsigla - estado
********************************************************************************************************/
';
//http://179.184.216.103/ws/unibenjf_ws_emp_dsv.php?op=alt&cod=0&cnpj=12345678901234&nome=teste123&razao=teste&log=1&num=1&compl=1&bai=1&cep=36000100&cxp=213&dtf=2015-03-30&obs=observacao&age=123&bc=033&diav=1&cc=132&ncc=4125&cobcp=N&ins=teste&dmv=2015-03-01
//EMPCONTATO
//http://179.184.216.103/ws/unibenjf_ws_emp_dsv.php?op=icon&cod=3037&nome=teste&ddd=032&tel=32211111&cargo=abc&email=abc@gmail


require_once("./uClassFB.php"); 
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php");

//xdebug_disable();
error_reporting(E_ALL ^ E_WARNING); 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';

if ($vOper==='???') {
	$vResult= $vHelp;
} else {  
  require_once("./uOperacoesInst.php");
  
  if  (in_array($vOper,array(CTEINSTLER))) {  
    $vResult = lerdados($vOper);
  }  
} 

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log_inst.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 

/**
  Leitura de dados
*/

function lerDados($pOper) {    
	$vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

	if (!$vAcessoBD = criaAcessoBD()) {
		$vRetornoArr['erro']= 'erro na abertura do BD';
		goto FINALLYY;
	}
  
	$vSQL= '';
  
	if ($pOper === CTEINSTLER) {
		if (!isset($_REQUEST[PAR_INST_COD])) {
		  $vRetornoArr['erro']= PAR_INST_COD.' nao informado';
		  goto FINALLYY;
		}

		$vSQL = 'select '.
						'INST.INSTCNPJ AS '.PAR_INST_CNPJ.', '.
						'INST.INSTCODIGO AS '.PAR_INST_COD.', '.
						'INST.INSTRAZAOSOCIAL AS '.PAR_INST_RAZAO.', '.
						'INST.LOGCODIGO AS '.PAR_INST_LOG.', '.
						'INST.INSTNUMERO AS '.PAR_INST_NUM.', '.
						'INST.INSTCOMPLEMENTO AS '.PAR_INST_COMPL.', '.
						'INST.BAICODIGO AS '.PAR_INST_BAI.', '.
						'INST.INSTCEP AS '.PAR_INST_CEP.','.
					    "coalesce(L.LOGNOME,'ND') as lognome,coalesce(TL.TLOGNOME,'ND') as tlognome,coalesce(B.BAINOME,'ND') as bainome,".
					    "coalesce(CID.CIDNOME,'ND') as cidnome,coalesce(U.UFSIGLA,'ND') as ufsigla ".
				'from INSTITUICAO INST '.
				'left outer join LOGRADOURO L on (INST.LOGCODIGO = L.LOGCODIGO) '.
				'left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) '.
				'left outer join BAIRRO B on (INST.BAICODIGO =  B.BAICODIGO) '.
				'inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) '.
				'inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) '.
				'where INST.INSTCODIGO='.$_REQUEST[PAR_INST_COD];
	} 

	if ($vSQL ==='') {
		$vRetornoArr['erro'] = 'operacao desconhecida';
		goto FINALLYY;
	}
  
	//return $vSQL;
	
	try {
		if ($vAcessoBD->open($vSQL)) {
			if ($vAcessoBD->recordCount===0) {
				$vRetornoArr['erro']= 'sem dados';
			} else {
				$vRetornoArr['sucesso']=true;
				$vRetornoArr['dados']= $vAcessoBD->dataSetJSON();
			}  
		} else {
			$vRetornoArr['erro']= $vAcessoBD->erro;
		}
	} catch (Exception $vExcecao) {
		$vRetornoArr['erro'] = 'exception: '.$vExcecao->getMessage();
	}
	FINALLYY:
		unset($vAcessoBD);
	return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}


?>