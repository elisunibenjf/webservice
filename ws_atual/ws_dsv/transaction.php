<?php 

require_once 'conexao.php';

final class Transaction{
  private static $conn; // conexao ativa

  private function __construct(){}

  public static function open($database){
    if(empty(self::$conn)){
      self::$conn = Conexao::open($database);
      self::$conn->beginTransaction(); //inicia a transacao
    }
  }

  public static function get(){
    return self::$conn; //retorna a conexao ativa
  }

  public static function commit(){
    if(self::$conn){
      return self::$conn->commit(); //aplica as operações realizadas
    }
  }
  
  public static function rollback(){
    if(self::$conn){
      return self::$conn->rollback(); //desfaz as operações realizadas
    }
  }

  public static function close(){
    if(self::$conn){
      return self::$conn = NULL;
    }
  }

}

 ?>