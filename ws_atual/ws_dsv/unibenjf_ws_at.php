<?php

    require_once("./uClassFB.php"); 
    require_once("./uDefinicoes.php"); 
    require_once("./uUtil.php");

    $vHelp = '
    /*******************************************************************************

    - operacoes (parametro op)
    - ??? - help   
    -  bsct:                  valida token
    -  insert_token:          insere token
    -  insert_atedimento:     insere atendimento  
    -  boletosemaberto:       retorna todos boletos em aberto

    op                        parametros                     retorno
    --                        -----------------------        -----------
    bsct                      token                          retorna json
    
    insert_token              entidade_tipo                  retorna json
                              cod_entidade
                              token
    
    insert_atedimento         protocolo_tipo                 retorna json
                              cod_entidade

    update_token              protocolo                      retorna nada
                              token
    boletos_abertos           

	lstcomp					  id 							 retorna json

    *******************************************************************************/
    ';

    $vResult = '';    
    $tipo    = '';
    $vSQL    = '';

    $vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';

    if ($vOper==='???') {
        $vResult= $vHelp;
      }

   if($vOper !=''){

     if ($_REQUEST['op'] === 'bsct') {
      $vSQL = "SELECT
                FIRST 1
                IIF(ATPROTOCOLO IS NULL ,0,1) AS PROTOCOLO,
                ATTOK_ID,
                ATTOK_ENTIDADECODIGO,
                ATTOK_TOKEN,
                ATPROTOCOLO
                FROM ATTOKEN WHERE ATTOK_ENTIDADECODIGO = '".$_REQUEST['cod_entidade']."'
                AND ATTOK_VALIDADE = cast('Now' as date) 
                ORDER BY ATTOK_VALIDADE DESC";
      $tipo = 'ler';
    } 

    if($_REQUEST['op'] === 'insert_token') {
      $vSQL =  "INSERT INTO ATTOKEN (ATTOK_ENTIDADETIPO, ATTOK_ENTIDADECODIGO, ATTOK_VALIDADE, ATTOK_TOKEN)
      VALUES ('".$_REQUEST['entidade_tipo']."','".$_REQUEST['cod_entidade']."',cast('Now' as date),'".$_REQUEST['token']."') 
      returning  ATTOK_ID";
      $tipo = 'comRetorno';
    } 


    if($_REQUEST['op'] === 'update_token') {
      $vSQL =  "UPDATE ATTOKEN SET ATPROTOCOLO = '".$_REQUEST['protocolo']."' WHERE ATTOK_TOKEN = '".$_REQUEST['token']."'";
      $tipo = 'semRetorno';
    } 

    //id usario web 113
    if($_REQUEST['op'] === 'insert_atedimento') {
      $vSQL =  "INSERT INTO ATENDIMENTO (ATID, ATPROTOCOLO, ATCATASID, ATDATA, UCODIGO, ATPRIID, ATSTID, ATENTIDADETIPO, ATENTIDADECODIGO, ATDESCRICAO)
                VALUES((SELECT gen_id(G_ATID,1) FROM RDB\$DATABASE), (SELECT * FROM SP_ATGERAPROTOCOLOANS('418528')), 1, current_timestamp , 113, 1, 1, '".$_REQUEST['protocolo_tipo']."', 
                '".$_REQUEST['cod_entidade']."', '') returning ATPROTOCOLO";
      $tipo = 'comRetorno';
    } 

    if($_REQUEST['op'] === 'lstcomp') {
      $vSQL =  "";
      $tipo = 'comRetorno';
    } 

    if($_REQUEST['op'] === 'boletos_abertos'){
      $vSQL = "SELECT CR.CRECCOMPETENCIA AS COMPETENCIA,".
      " CR.CRECDATAVENCIMENTO AS VENCIMENTO,".
      " CR.CRECDATAVENCIMENTOORI AS VENCIMENTOCONTRATO,".
      " CR.CRECSTATUS AS STATUS,".
      " CR.CRECOBSERVACAO AS OBSERVACAO,".
      " CR.CRECDESCONTO AS DESCONTO,".
      " CR.CRECMORAMULTA AS MORAMULTA,".
      " CR.CRECMORAMULTAAPAGAR AS MULTA,".
      " CR.CRECTIPOENTIDADE AS TIPOENTIDADE,".
      " CR.CRECENTIDADECODIGO AS IDENTIDADE,".
      " CR.CRECMENSALIDADE AS MENSALIDADE,".
      " CR.CRECCOPARTICIPACAO AS COPARTICIPACAO,".
      " CR.CRECAVULSO AS AVULSO,".
      " CR.CRECVALORAPAGAR AS VALORAPAGAR FROM CONTASARECEBER AS CR".
      " WHERE CR.CRECTIPOENTIDADE='C' AND CR.CRECENTIDADECODIGO=".$_REQUEST['codigo']." AND CR.CRECSTATUS <> 'PG' AND CR.CRECSTATUS <> 'CA'";

      $tipo = 'comRetorno';
    }

    if ($_REQUEST['op'] === 'primeiroBoleto') {
      
    }

    executarComandos($vSQL, $tipo);

    $fp = fopen("./log/log_can.txt", "a");
    $escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
    fclose($fp); 
  }

?>