<?php
header('Access-Control-Allow-Origin: *');
/*******************************************************************************
 * uEscalaOperacao.php
    - processa as requisicoes da unibenjf
 */
$vHelp = '
/*******************************************************************************
  - operacoes (parametro op)
    - ??? - retorna este help   
    -  1  - instituicoes
    -  2  - produtos de uma instituicao
    -  3  - precos de um plano de uma instituicao
    -  4  - graus de parentesco
    -  5  - operadoras que atendem uma instituicao
    -  6  - acomodacoes
    -  7  - fator moderador
    -  8  - abrangencia
    -  9  - graus de parentesco permitidos para um produto
    - 10  - instituicoes que tem produto com uma operadora
    - 11  - valida um cliente
    - 12  - muda a senha de um cliente
    - 13  - lancamentos de um cliente na competencia
    - 14  - movimentacao financeira de um usuario no ano
    - 15  - retorna os dados para emissao de 2a via boleto 
    - 16  - extrato IR
    - luni  - lista de unidades de uma instituicao 
    - lemp  - lista empresas
    - lmop  - lista modos de pagamento
    - lban  - lista bancos 
    - lestc - lista estado civil
    - lcons - lista de consultores
    - cpfe  - retorna o email a partir de um CPF
    - msc   - muda senha do cliente apenas pelo codigo
      
  - parametros
 
    op  op2                      op3           op4         op5           op6           retorno
    --  -----------------------  ----------    ---------   -----------   -----------   -----------
    01  sem uso                  sem uso       sem uso     sem uso
    02  codigo da instituicao    codigo        codigo      codigo
                                 acomodacao    fator       abrangencia 
                                 (0-todos)     moderador   (0-todos)
                                               (0-todos)
    03  codigo da instituicao    codigo        sem uso     sem uso
                                 produto       sem uso     sem uso
    04  sem uso                  sem uso       sem uso     sem uso
    05  codigo da instituicao    sem uso       sem uso     sem uso
    06  sem uso                  sem uso       sem uso     sem uso
    07  sem uso                  sem uso       sem uso     sem uso 
    08  sem uso                  sem uso       sem uso     sem uso
    09  codigo do produto        sem uso       sem uso     sem uso
    10  codigo da operadora      sem uso       sem uso     sem uso
    11  CPF do usuario           senha                                                 codigo do usuario-nome do usuario
                                 (MD5)                                                 ZERO se nao validar                                
    12  codigo do usuario        senha antiga  nova senha                              se sucesso: codigo do usuario-nome
                                 (MD5)         (MD5)                                   se falhar:  -1 senha antiga invalida
                                                                                                   -2 tamanho invalido da senha nova                             
                                                                                       ZERO outros erros
    13  codigo do usuario        senha         competencia                             lancamentos do cliente
                                 (MD5)         (AAAAMM)
    14  codigo do usuario        senha         ano                                     lancamentos da movimentacao financeira do cliente
                                 (MD5)         (AAAA)
    15  codigo do usuario        senha         tipo        nosso         competencia   dados para emissao do boleto 
                                 (MD5)         entidade    numero        (AAAAMM)
                                               (C)	 
    16  codigo do usuario        senha         ano                                     extrato de IR
                                 (MD5)         (AAAA)															 
																 
  luni	codigo da instituicao                                                          lista de unidades de uma instituicao 
  lemp                                                                                 lista empresas
  lmop                                                                                 lista modos de pagamento
  lban                                                                                 lista bancos 
  lestc                                                                                lista estado civil
  lcons                                                                                lista consultores
  cpfe   cpf sem formatacao                                                            json de resposta
  ----  ----------------------------------------------------------------------------------------------
   msc   codigo do usuario       nova senha                                            se sucesso: codigo do usuario-nome
                                 (MD5)                                                 se falhar:  -1 senha invalida
                                                                                                   
                                 
*******************************************************************************/
';
/*******************************************************************************
Exemplos

http://127.0.0.1/unibenjf/ws/unibenjf_ws.php?op=12&op2=20&op3=8E62F252732EADF28315BD9F498D3356&op4=8E62F252732EADF28315BD9F498D3356

http://127.0.0.1/unibenjf/ws/unibenjf_ws.php?op=13&op2=20&op3=8E62F252732EADF28315BD9F498D3356&op4=201408

http://127.0.0.1/ws/unibenjf_ws.php?op=14&op2=402&op3=CBC8986A65372EDA6DAA78088C48D29D&op4=2014

http://187.32.177.136/ws/unibenjf_ws.php?op=15&op2=1544&op3=BB19E844A29320A5275977674802ABF4&op4=C&op5=27628980000001323&op6=201602

http://187.32.177.137/ws/unibenjf_ws.php?op=16&op2=402&op3=CBC8986A65372EDA6DAA78088C48D29D&op4=2016

http://127.0.0.1/ws/unibenjf_ws.php?op=14&op2=2003&op3=d320452797faa03475a65c8badecb758&op4=2014

site teste http://198.199.104.188/unibenjf/public/

*******************************************************************************/

require_once("./uClassFB.php");
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php"); 
 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';
$vOper2 = (isset($_REQUEST['op2'])) ? $_REQUEST['op2'] : '';
$vOper3 = (isset($_REQUEST['op3'])) ? $_REQUEST['op3'] : '';
$vOper4 = (isset($_REQUEST['op4'])) ? $_REQUEST['op4'] : '';
$vOper5 = (isset($_REQUEST['op5'])) ? $_REQUEST['op5'] : '';
$vOper6 = (isset($_REQUEST['op6'])) ? $_REQUEST['op6'] : '';

if ($vOper !== '') {
	if ($vOper==='???') {
		$vResult= $vHelp;
	} else {
    define("CTECONTAARECEBERPAGA",'PG');
	
  	define("CTELISTARINST",'1');
    define("CTELISTARINST2",'17');
  	define("CTELISTARINSTPROD",'2');
  	define("CTELISTARPRECO",'3');
  	define("CTELISTARGRAUPAR",'4');
  	define("CTEOPER_INST",'5');
  	define("CTELISTARACOMODACAO",'6');
  	define("CTELISTARFATORMODERADOR",'7');
  	define("CTELISTARABRANGENCIA",'8');
  	define("CTELISTAPRODGPAR",'9');
  	define("CTELISTAINSTOP",'10');
  	define("CTEUSUVALIDAR",'11');
  	define("CTEUSUTROCARSENHA",'12');
  	define("CTEUSUMOVMENSAL",'13');
   	define("CTEUSUMOVFIN",'14');
    define("CTEBOLETO2AVIA",'15');
    define("CTEEXTRATOIR",'16');

    define("CTELISTAUNI",'luni');
    define("CTELISTAEMP",'lemp');
    define("CTELISTAMOP",'lmop');
    define("CTELISTABAN",'lban');
    define("CTELISTAESTC",'lestc');
    define("CTELISTACONS",'lcons');

    define("CTECPFEMAIL",'cpfe');
    define("CTELISTCRECT",'ctemp');
    define("CTELISTPERMISSOES",'acs');
    define("CTEGBOLETOCAD",'gbol');
    define("CTEMUDASENHACOD",'msc');


    if (in_array($vOper,array(CTEUSUTROCARSENHA,CTEMUDASENHACOD))) {
      $vResult = trocarSenha($vOper,$vOper2,$vOper3,$vOper4);
    } else {
	  	$vResult = lerDados($vOper,$vOper2,$vOper3,$vOper4,$vOper5,$vOper6);
    }  
	}	
}

// PARA DESABILITAR A GERACAO DE 2A VIA DE BOLETO PELO SITE
//if (in_array($vOper,array(CTEBOLETO2AVIA))) {
//  $vResult = '';  
//}

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 


function lerDados($pOper,$pOper2,$pOper3,$pOper4,$pOper5,$pOper6) {    
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
	
	if (in_array($pOper,array(CTELISTARINSTPROD,CTELISTARPRECO,CTEOPER_INST,CTELISTAPRODGPAR,CTELISTAINSTOP,
	                          CTEUSUVALIDAR,CTEUSUTROCARSENHA,CTEUSUMOVMENSAL,CTEUSUMOVFIN,CTEEXTRATOIR,CTELISTAUNI,CTECPFEMAIL, CTEGBOLETOCAD))) {
	  if ($pOper2 === '') {
	    $vRetorno= 'Requisicao invalida2';
	    goto FINALLYY;
	  }
	}
	
	if (in_array($pOper,array(CTELISTARPRECO,CTEUSUVALIDAR,CTEUSUTROCARSENHA,CTEUSUMOVMENSAL,CTEUSUMOVFIN,CTEEXTRATOIR, CTEGBOLETOCAD))) {
	  if ($pOper3 === '') {
	    $vRetorno= 'Requisicao invalida3';
	    goto FINALLYY;
	  }
	}
  
  if (in_array($pOper,array(CTEUSUMOVFIN,CTEEXTRATOIR))) {
	  if ($pOper4 === '') {
	    $vRetorno= 'Requisicao invalida4';
	    goto FINALLYY;
	  }
	}
  
  if (in_array($pOper,array(CTEBOLETO2AVIA))) {
	  if ($pOper5 === '') {
	    $vRetorno= 'Requisicao invalida5';
	    goto FINALLYY;
	  }
	}
  
  if (in_array($pOper,array(CTEBOLETO2AVIA))) {
	  if ($pOper6 === '') {
	    $vRetorno= 'Requisicao invalida6';
	    goto FINALLYY;
	  }
	}
  
	$vRetorno='';
	$vSQL= '';
  
	if ($pOper === CTELISTARINST) {
    $vSQL = "select INSTCODIGO, INSTRAZAOSOCIAL from INSTITUICAO order by INSTRAZAOSOCIAL";
	} elseif ($pOper === CTELISTARINST2){
    $vSQL = //"SELECT INSTCODIGO, INSTRAZAOSOCIAL FROM INSTITUICAO WHERE INSTCODIGO IN(1,41,29,64,6,46,12,5,13,39,31,36,2,77,50,40,16,32,35,75,55,72,18,70) ORDER BY INSTRAZAOSOCIAL";
            "select distinct INSTPROD.INSTCODIGO, INSTITUICAO.INSTRAZAOSOCIAL AS INSTNOME ".
            "from INSTPROD  ".
            "inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO  ".
            "inner join OPERADORA on PRODUTO.OPCODIGO=OPERADORA.OPCODIGO  ".
            "inner join INSTITUICAO on INSTITUICAO.INSTCODIGO=INSTPROD.INSTCODIGO  ".
            "where PRODUTO.PRODSITCODIGO=1 AND OPERADORA.OPCODIGO = ".$pOper2;

  } elseif ($pOper === CTELISTARINSTPROD) {	
		$vSQL = "select IP.PRODCODIGO,P.PRODNOME,OP.OPRAZAOSOCIAL ".
            "from INSTPROD IP ".
            "inner join PRODUTO P on (P.PRODCODIGO=IP.PRODCODIGO) ".
						"inner join OPERADORA OP on (P.OPCODIGO=OP.OPCODIGO) ".			
						"where IP.INSTCODIGO=".$pOper2." and ".
					  "P.PRODINATIVO='N' and P.PRODCODIGO <> 9";
	  if (($pOper3 !== '') && ($pOper3 !== '0'))  {
	    $vSQL.= ' and P.ACMCODIGO='.$pOper3;
	  }
	  if (($pOper4 !== '') && ($pOper4 !== '0'))  {
	    $vSQL.= ' and P.FMCODIGO='.$pOper4;
	  }
	  if (($pOper5 !== '') && ($pOper5 !== '0'))  {
	    $vSQL.= ' and P.PRODAGCODIGO='.$pOper5;
	  }
	  
		$vSQL.= " order by OP.OPRAZAOSOCIAL,P.PRODNOME";
	} elseif ($pOper === CTELISTARPRECO) {
		$vSQL = 'select PRECO.PRECOFAIXAINICIO as "Inicio",PRECO.PRECOFAIXAFIM as "Fim",PRECOVALOR as "Valor" '.
		        'from PRECO '.
		        'where PRECO.INSTCODIGO='.$pOper2." and ".
		           		'PRECO.PRODCODIGO='.$pOper3."  ".
        		'order by PRECO.PRECOFAIXAINICIO';
	} elseif ($pOper === CTELISTARGRAUPAR) {
		$vSQL = "select GPARCODIGO,GPARNOME from GRAUPARENTESCO order by GPARNOME";
  } elseif ($pOper === CTEOPER_INST) {
    $vSQL = /*"select distinct PRODUTO.OPCODIGO as CODIGO,OPERADORA.OPNOMEFANTASIA as NOME ".
            "from INSTPROD ".
            "inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
            "inner join OPERADORA on PRODUTO.OPCODIGO=OPERADORA.OPCODIGO ".
            "where INSTPROD.INSTCODIGO=".$pOper2." and ".
                  "PRODUTO.PRODSITCODIGO=1"*/
            "select distinct PRODUTO.OPCODIGO as OPCODIGO, OPERADORA.OPNOMEFANTASIA as OPNOME ".
            "from INSTPROD ".
            "inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
            "inner join OPERADORA on PRODUTO.OPCODIGO=OPERADORA.OPCODIGO ".
            "inner join INSTITUICAO on INSTITUICAO.INSTCODIGO=INSTPROD.INSTCODIGO ".
            "where PRODUTO.PRODSITCODIGO=1 AND OPERADORA.OPCODIGO = ".$pOper2;
  } elseif ($pOper === CTELISTARACOMODACAO) {
    $vSQL = "select ACOMODACAO.ACMCODIGO,ACOMODACAO.ACMNOME from ACOMODACAO";
  } elseif ($pOper === CTELISTARFATORMODERADOR) {
    $vSQL = "select FATORMODERADOR.FMCODIGO,FATORMODERADOR.FMNOME from FATORMODERADOR";
  } elseif ($pOper === CTELISTARABRANGENCIA) {  
    $vSQL = "select PRODABRANGGEO.PRODAGCODIGO,PRODABRANGGEO.PRODAGNOME from PRODABRANGGEO";
  } elseif ($pOper === CTELISTAPRODGPAR) {
    $vSQL = "select GRAUPARENTESCO.GPARNOME ".
            "from PRODGPAR ".
            "inner join GRAUPARENTESCO on (GRAUPARENTESCO.GPARCODIGO=PRODGPAR.GPARCODIGO) ".
            "where PRODGPAR.PRODCODIGO=".$pOper2." ".
            "order by GRAUPARENTESCO.GPARNOME";
  } elseif ($pOper === CTELISTAINSTOP) {
    $vSQL = "select distinct INSTITUICAO.INSTCODIGO,INSTITUICAO.INSTRAZAOSOCIAL ".
            "from INSTITUICAO ".
            "inner join INSTPROD on INSTPROD.INSTCODIGO=INSTITUICAO.INSTCODIGO ".
            "inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
            "where PRODUTO.PRODINATIVO='N' and PRODUTO.OPCODIGO=".$pOper2." ".
            "order by INSTITUICAO.INSTRAZAOSOCIAL";
  } elseif ($pOper === CTEUSUVALIDAR) {
    $vSQL = "select CLIENTE.CLICODIGO,CLIENTE.CLINOME ".  
            "from CLIENTE ".	
			      "where CLIENTE.CLICPF='".$pOper2."' and ".
                  "CLIENTE.CLISENHAWEB='".$pOper3."'";
  } elseif ($pOper === CTEUSUMOVMENSAL) { 
    // $vSQL = "select cm.CRECID, crec.CRECSTATUS, CM.CLIMMODOCOB AS MODO, ". 
    //         "FLT.FLANCTACRONIMO as TIPO,".
    //                "case CM.CLIDEPCODIGO when 0 then C.CLINOME else coalesce(CD.CLIDEPNOME,'<<<DESCONHECIDO>>>') end as DEPEND,".
    //                "CM.CLIMDATA as DATA,CM.CLIMDESCRICAO as Descricao,CM.CLIMQTD as QTD,".
    //                "CM.CLIMVALORUNITARIOCOBRADO as ValorUnitario ".
    //         "from CLIMOVIMENTACAO CM ".  
    //         "inner join FLANCAMENTOTIPO FLT on (FLT.FLANCTCODIGO=CM.FLANCTCODIGO) ".
    //         "inner join CLIENTE C on (CM.CLICODIGO=C.CLICODIGO) ".
    //         "left join CLIDEP CD on (CM.CLICODIGO=C.CLICODIGO) and (CM.CLIDEPCODIGO=CD.CLIDEPCODIGO) ".
    //         "left join CONTASARECEBER crec on (crec.CRECID = cm.CRECID) ".
    //         "where CM.CLICODIGO='".$pOper2."' and ".
    //               "CM.CLIMCOMPETENCIA='".$pOper4."'";

   $vSQL = "select cm.CRECID, crec.CRECSTATUS, CM.CLIMMODOCOB AS MODO, ". 
            "FLT.FLANCTACRONIMO as TIPO,".
                   "case CM.CLIDEPCODIGO when 0 then C.CLINOME else coalesce(CD.CLIDEPNOME,'<<<DESCONHECIDO>>>') end as DEPEND,".
                   "CM.CLIMDATA as DATA,CM.CLIMDESCRICAO as Descricao,CM.CLIMQTD as QTD,".
                   "CM.CLIMVALORUNITARIOCOBRADO as ValorUnitario ".
            "from CLIMOVIMENTACAO CM ".  
            "inner join FLANCAMENTOTIPO FLT on (FLT.FLANCTCODIGO=CM.FLANCTCODIGO) ".
            "inner join CLIENTE C on (CM.CLICODIGO=C.CLICODIGO) ".
            "left join CLIDEP CD on (CM.CLICODIGO=C.CLICODIGO) and (CM.CLIDEPCODIGO=CD.CLIDEPCODIGO) ".
            "left join CONTASARECEBER crec on (crec.CRECID = cm.CRECID) ".
            "where CM.CLICODIGO='".$pOper2."' AND CREC.CRECSTATUS <> 'CA' AND CM.CLIMCOMPETENCIA='".$pOper4."'";

  } elseif (($pOper === CTEUSUMOVFIN) && ($pOper4 >= '2014')) {								
	// $vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
	// 	       'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
	// 	       "CONTASARECEBER.CRECSTATUS as Status,".
	// 	       "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
	// 	       'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
	// 	       "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
	// 	       'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
	// 	       "CONTASARECEBER.CRECNOSSONUMERO ".
									
	// 	       "from CONTASARECEBER ".  
	// 				 "inner join CLIENTE C on (CONTASARECEBER.CRECENTIDADECODIGO=C.CLICODIGO) ".
	// 				 'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.
	// 	       "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
 //      					 "(CONTASARECEBER.CRECTIPOENTIDADE='C') and ".
	// 							 "C.CLISENHAWEB='".$pOper3."' and ".
	// 				       "CONTASARECEBER.CRECENTIDADECODIGO='".$pOper2."' and ".
	// 				       "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)='".$pOper4."' ".
 //           "order by 1";

    /*$vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
		       'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
		       "CONTASARECEBER.CRECSTATUS as Status,".
		       "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
		       'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
		       "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
		       'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
		       "CONTASARECEBER.CRECNOSSONUMERO ".
									
		       "from CONTASARECEBER ".  
					 "inner join CLIENTE C on (CONTASARECEBER.CRECENTIDADECODIGO=C.CLICODIGO) ".
					 'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.
		       "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
		       			"(CONTASARECEBER.CRECSTATUS = 'AB') and".
      					 "(CONTASARECEBER.CRECTIPOENTIDADE='C') and ".
					       "CONTASARECEBER.CRECENTIDADECODIGO='".$pOper2."' ".
           "order by 1";
*/


           $vAnoAnt= $pOper4 - 2;

    $vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
           'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
           "CONTASARECEBER.CRECSTATUS as Status,".
           "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
           'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
           "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
           'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
           "CONTASARECEBER.CRECNOSSONUMERO ".
                  
           "from CONTASARECEBER ".  
           "inner join CLIENTE C on (CONTASARECEBER.CRECENTIDADECODIGO=C.CLICODIGO) ".
           'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.

           "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
                "CONTASARECEBER.CRECSTATUS IN ('AB','PG') and".
                 "(CONTASARECEBER.CRECTIPOENTIDADE='C') and ".
                 "CONTASARECEBER.CRECENTIDADECODIGO='".$pOper2."' and ".
                 "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)>'".$vAnoAnt."' ".

           "order by CONTASARECEBER.CRECDATAVENCIMENTO DESC";
           
  } elseif ($pOper === CTEBOLETO2AVIA)  {

    $vSQL= "select CLIENTE.CLINOME as sac_nome,CLIENTE.CLICPF as sac_CPF,".
                  "coalesce(TL.TLOGNOME,'ND') || ' ' || coalesce(L.LOGNOME,'ND') || ', ' || CLIENTE.CLINUMERO || ' ' || ".
                  "coalesce(CLIENTE.CLICOMPLEMENTO,'',CLIENTE.CLICOMPLEMENTO) || ".
                  "'-' || coalesce(B.BAINOME,'ND') as sac_endereco,".
                  "CLIENTE.CLICEP as sac_cep,".
                  "coalesce(CID.CIDNOME,'ND') as sac_cidade,coalesce(U.UFSIGLA,'ND') as sac_uf,".

                  "CONTASARECEBER.CRECDATA,".
                  "CONTASARECEBER.CRECDATAVENCIMENTO as VENCIMENTOORIGINAL,".
                  "CONTASARECEBER.CRECDATAVENCIMENTO as VENCIMENTO,CONTASARECEBER.CRECVALORAPAGAR as valorapagar,".

                  "'UNIBEN ADMINISTRADORA DE BENEFICIOS LTDA' as ced_nome,".
                  "'Rua Braz Bernardino, 192 - Centro' as ced_endereco,'36010320' as ced_cep,".
                  "'Juiz de Fora' as ced_cidade,'MG' as ced_uf,".
                  "'15.156.097/0001-01' as ced_CNPJ,".

                  "CONTASARECEBER.CRECNOSSONUMERO as nossonumero,".
                  "CONTASARECEBER.CRECINSTRUCAO as instrucao,".
				          "CONTASARECEBER.CRECSEUNUMERO as seunumero,".
                  
                  "BANCO.BANCOCODIGO as banco, BANCO.BANCOAGENCIA as agencia, BANCO.BANCOCONTACORRENTE as contacorrente,".
                  "CONTASARECEBER.BANCOBOLETOCODCONVENIO as convenio,BANCO.BANCODIASVALIDADEBOLETO,".

                  "CONTASARECEBER.CRECENTIDADECODIGO as codigo, 0 as multa ".

           "from CONTASARECEBER ".
           "inner join CLIENTE on (CLIENTE.CLICODIGO=CONTASARECEBER.CRECENTIDADECODIGO) ".
           "left outer join LOGRADOURO L on (CLIENTE.LOGCODIGO = L.LOGCODIGO) ".
           "left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) ".
           "left outer join BAIRRO B on ( CLIENTE.BAICODIGO =  B.BAICODIGO) ".
           "inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) ".
           "inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) ".
           "inner join BANCO on (CONTASARECEBER.BANCOID=BANCO.BANCOID) ".
           "where CONTASARECEBER.CRECENTIDADECODIGO=".$pOper2.
                 " and CONTASARECEBER.CRECTIPOENTIDADE='".$pOper4."'".
                 " and CONTASARECEBER.CRECNOSSONUMERO='".$pOper5."'".
                 " and CONTASARECEBER.CRECCOMPETENCIA='".$pOper6."'";

   } elseif ($pOper === CTEEXTRATOIR) {

    // $vSQL= 'select DADOS.COMPET AS COMPET'.
    //               ",case DADOS.CLIDEPCODIGO when 0 then 0 else 1 end as TIT".   
    //               ",case DADOS.CLIDEPCODIGO when 0 then DADOS.CLINOME else coalesce(CLIDEP.CLIDEPNOME,'<<<DESCONHECIDO>>>') end as BENEFICIARIO".
    //               ',FLANCAMENTOTIPO.FLANCTACRONIMO as TIPO'.
    //               ',DADOS.VALOR_BASE,DADOS.ENCARGOS,DADOS.DESCONTO'.
    //               ',(DADOS.VALOR_BASE+DADOS.ENCARGOS) AS TOTAL '.
    //               ',case DADOS.CLIDEPCODIGO when 0 then DADOS.CLICPF else CLIDEP.CLIDEPCPF end as CPF '.
    //        'from (select CMOV.*,'.
    //                     'case CMOV.FLANCTCODIGO '.
    //                        'when 1 then (select coalesce(sum(CONTASARECEBER.CRECMORAMULTA),0) '.
    //                                     'from CONTASARECEBER '.
    //                                     'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
    //                                           "(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and ".
    //                                           "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
    //                                           "(CONTASARECEBER.CRECTIPOENTIDADE='C')".
    //                                    ") ".
    //                        'else 0 '.
    //                     'end as ENCARGOS,'.

    //                     'case CMOV.FLANCTCODIGO '.
    //                      'when 1 then (select coalesce(sum(CONTASARECEBER.CRECDESCONTO),0) '.
    //                                   'from CONTASARECEBER '.
    //                                   'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
    //                                         '(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and '.
    //                                         "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
    //                                         "(CONTASARECEBER.CRECTIPOENTIDADE='C')".
    //                                   ') '.

    //                      'else 0 '.
    //                    'end as DESCONTO '.

    //               'from (select CM.CLIMCOMPETENCIA AS COMPET,CM.CLICODIGO,CM.CLIDEPCODIGO,CM.FLANCTCODIGO,C.CLINOME,C.CLICPF,'.
    //                            'cast( sum(CM.CLIMQTD*CM.CLIMVALORUNITARIOCOBRADO) as decimal(10,2))as VALOR_BASE '.
    //                     'from CLIMOVIMENTACAO CM '.
    //                     'inner join CLIENTE C on (CM.CLICODIGO=C.CLICODIGO) '.
    //                     'inner join CONTASARECEBER CR1 on (CR1.CRECID=CM.CRECID) '.
    //                     'where CM.CLICODIGO='.$pOper2.
    //                           " and C.CLISENHAWEB='".$pOper3."'".
    //                           " and substring(CR1.CRECDATAPAGAMENTO from 1 for 4)='".$pOper4."' ".
    //                           " and CR1.CRECSTATUS='".CTECONTAARECEBERPAGA."' ".
    //                           " and CM.CLIMMODOCOB='C' ".
    //                     'group by 1,2,3,4,5,6 '.
    //                    ') as CMOV '.
    //                    ' order by COMPET '.
    //             ') as DADOS '.
    //        'inner join FLANCAMENTOTIPO on (FLANCAMENTOTIPO.FLANCTCODIGO=DADOS.FLANCTCODIGO) '.
    //        'left join CLIDEP on (DADOS.CLICODIGO=DADOS.CLICODIGO) and (DADOS.CLIDEPCODIGO=CLIDEP.CLIDEPCODIGO) '.
    //        'order by 2,3,1';

    $vSQL= 'select DADOS.COMPET AS COMPET'.
                  ",case DADOS.CLIDEPCODIGO when 0 then 0 else 1 end as TIT".   
                  ",case DADOS.CLIDEPCODIGO when 0 then DADOS.CLINOME else coalesce(CLIDEP.CLIDEPNOME,'<<<DESCONHECIDO>>>') end as BENEFICIARIO".
                  ',FLANCAMENTOTIPO.FLANCTACRONIMO as TIPO'.
                  ',DADOS.VALOR_BASE,DADOS.ENCARGOS,DADOS.DESCONTO'.
                  ',(DADOS.VALOR_BASE+DADOS.ENCARGOS) AS TOTAL '.
                  ',case DADOS.CLIDEPCODIGO when 0 then DADOS.CLICPF else CLIDEP.CLIDEPCPF end as CPF '.
           'from (select CMOV.*,'.
                        'case CMOV.FLANCTCODIGO '.
                           'when 1 then (select coalesce(sum(CONTASARECEBER.CRECMORAMULTA),0) '.
                                        'from CONTASARECEBER '.
                                        'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
                                              "(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and ".
                                              "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
                                              "(CONTASARECEBER.CRECTIPOENTIDADE='C')".
                                       ") ".
                           'else 0 '.
                        'end as ENCARGOS,'.

                        'case CMOV.FLANCTCODIGO '.
                         'when 1 then (select coalesce(sum(CONTASARECEBER.CRECDESCONTO),0) '.
                                      'from CONTASARECEBER '.
                                      'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
                                            '(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and '.
                                            "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
                                            "(CONTASARECEBER.CRECTIPOENTIDADE='C')".
                                      ') '.

                         'else 0 '.
                       'end as DESCONTO '.

                  'from (select CM.CLIMCOMPETENCIA AS COMPET,CM.CLICODIGO,CM.CLIDEPCODIGO,CM.FLANCTCODIGO,C.CLINOME,C.CLICPF,'.
                               'cast( sum(CM.CLIMQTD*CM.CLIMVALORUNITARIOCOBRADO) as decimal(10,2))as VALOR_BASE '.
                        'from CLIMOVIMENTACAO CM '.
                        'inner join CLIENTE C on (CM.CLICODIGO=C.CLICODIGO) '.
                        'inner join CONTASARECEBER CR1 on (CR1.CRECID=CM.CRECID) '.
                        'where CM.CLICODIGO='.$pOper2.
                              " and substring(CR1.CRECDATAPAGAMENTO from 1 for 4)='".$pOper4."' ".
                              " and CR1.CRECSTATUS='".CTECONTAARECEBERPAGA."' ".
                              " and CM.CLIMMODOCOB='C' ".
                        'group by 1,2,3,4,5,6 '.
                       ') as CMOV '.
                       ' order by COMPET '.
                ') as DADOS '.
           'inner join FLANCAMENTOTIPO on (FLANCAMENTOTIPO.FLANCTCODIGO=DADOS.FLANCTCODIGO) '.
           'left join CLIDEP on (DADOS.CLICODIGO=DADOS.CLICODIGO) and (DADOS.CLIDEPCODIGO=CLIDEP.CLIDEPCODIGO) '.
           'order by 2,3,1';

           

  } elseif ($pOper === CTELISTAUNI)  {	                              
    $vSQL = "select INSTUNI.INSTUNICODIGO,INSTUNI.INSTUNINOME ".
            "from INSTUNI ".
            "where INSTUNI.INSTCODIGO=".$pOper2;
  } elseif ($pOper === CTELISTAEMP)  {	                              
    $vSQL = "select EMPRESA.EMPCODIGO,EMPRESA.EMPNOME,EMPRESA.EMPRAZAOSOCIAL ".
            "from EMPRESA ".
            "order by EMPRESA.EMPRAZAOSOCIAL";
  } elseif ($pOper === CTELISTAMOP)  {	                              
    $vSQL = "select MODOPAGAMENTO.MODOPCODIGO,MODOPAGAMENTO.MODOPNOME ".
            "from MODOPAGAMENTO ".
            "order by MODOPAGAMENTO.MODOPNOME";          
  } elseif ($pOper === CTELISTABAN)  {	                              
    $vSQL = "select BANCO.BANCOID,BANCO.BANCOCODIGO,BANCO.BANCONOME,BANCO.UNIEID ".
            "from BANCO ".
            "order by BANCO.BANCONOME"; 
  } elseif ($pOper === CTELISTAESTC)  {                                
    $vSQL = "select ESTADOCIVIL.ESTCCODIGO,ESTADOCIVIL.ESTCNOME ".
            "from ESTADOCIVIL ".
            "order by ESTADOCIVIL.ESTCNOME";           
  } elseif ($pOper === CTELISTACONS)  {                                
    $vSQL = 'select UCODIGO as "codigo",UNOME as "nome" '.
            "from USUARIO ".
            "where UINATIVO='N' ".
            "order by UNOME";                   
  } elseif ($pOper === CTECPFEMAIL)  {                                
    $vSQL = 'select CLICODIGO as "codigo", CLICPF AS "cpf", CLIEMAIL as "email" '.
            "from CLIENTE ".
            "where CLICPF='".$pOper2."'";
  } elseif ($pOper === CTELISTCRECT)  {                                
    $vSQL = "select * from CONTASARECEBER_TEMP where CRECTSTATUS = 'AB' and UCONSULTOR =".$pOper2;
  } elseif ($pOper === CTELISTPERMISSOES)  {                                
    $vSQL = "select uacesso FROM usuario WHERE ucodigo =".$pOper2;
  } elseif ($pOper === CTEGBOLETOCAD)  {                                
    $vSQL = "update CONTASARECEBER_TEMP set CRECTDATAVENCIMENTO = ".$pOper3." where C.CRECTID = ".$pOper2."; ".
      "select C.CRECTNOME, C.CRECTCPF, C.CRECTLOGRADOURO, C.CRECTCIDADE, C.UFCODIGO, C.CRECTCEP, C.CRECTVALOR, C.CRECTNOSSONUMERO, ".
      "B.BANCOAGENCIA, B.BANCOCONTACORRENTE, C.BANCOBOLETOCODCONVENIO, B.BANCOMSGLOCPAGTOBOL, C.CRECTSEUNUMERO ".
      "from CONTASARECEBER_TEMP C join BANCO B ON B.BANCOID = C.BANCOID where C.CRECTID = ".$pOper2;
  }  else {
    $vSQL = ''; 
  }
    
	if ($vSQL ==='') {
	  goto FINALLYY;
	}
	
	try {
	  if ($vAcessoBD->open($vSQL)) {
	    //return 'xxx1'.$vAcessoBD->recordCount;	
			if ($vAcessoBD->recordCount===0) {
        if ($pOper === CTEUSUVALIDAR) {
          $vRetorno= '0';
        } else {  
	        $vRetorno = 'Sem dados';
        }  
				goto FINALLYY;
			}
      
      if ($pOper === CTEBOLETO2AVIA) {	
        date_default_timezone_set('America/Sao_Paulo');
        $vAgora = new DateTime();
        $vAgora->setTime(0,0,0); 

        // verifica se boleto nao expirou
        if ($vAcessoBD->dataSet[0]->BANCODIASVALIDADEBOLETO != 0) {
          $vCRECDATA =  New DateTime($vAcessoBD->dataSet[0]->CRECDATA); 
          $vDiferenca = $vCRECDATA->diff($vAgora);
          $vDias= (int)$vDiferenca->format('%R%a');
          if ($vDias > $vAcessoBD->dataSet[0]->BANCODIASVALIDADEBOLETO) {
            $vRetornoArr['erro']= 'BOLETO EXPIRADO. Entre em contato com o setor financeiro';
            goto FINALLYY;
          }
        } 

        $vAcessoBD->dataSet[0]->VENCIMENTO= $vAgora->format('Y-m-d');   
        
        $vVenc= new DateTime($vAcessoBD->dataSet[0]->VENCIMENTOORIGINAL);  
        $vInterval = $vVenc->diff($vAgora); 
        $vDias= (int)$vInterval->format('%R%a');
        
        // ajusta se o vencimento cair em um sabado ou domingo
        if (($vVenc->format('w')==='0') && ($vDias===1)){
          $vDias= 0;
        } elseif (($vVenc->format('w')==='6') && ($vDias===2)) {
          $vDias= 0;
        } 
        
        $vAcessoBD->dataSet[0]->DIAS= $vDias;
        
        if ($vDias > 0) {
          $vMulta = 2 + ($vDias*0.033);
          $vAcessoBD->dataSet[0]->MULTA=$vMulta; 

          if ($vAcessoBD->dataSet[0]->BANCO==='001') {
            $vBBMora =  ($vAcessoBD->dataSet[0]->VALORAPAGAR*$vAcessoBD->dataSet[0]->DIAS= $vDias*0.0333);  
           $vBBJuros = trunca( ($vAcessoBD->dataSet[0]->VALORAPAGAR*0.02));
           $vAcessoBD->dataSet[0]->ENCARGOS= trunca(($vBBMora/100)+($vBBJuros));
            /*$vBBMora = trunca(trunca($vAcessoBD->dataSet[0]->VALORAPAGAR*0.01)/30);
            $vBBJuros = round($vAcessoBD->dataSet[0]->VALORAPAGAR*0.02,2);
            $vAcessoBD->dataSet[0]->ENCARGOS= ($vDias*$vBBMora)+$vBBJuros;*/
          } else {
            $vAcessoBD->dataSet[0]->ENCARGOS= trunca($vMulta/100*$vAcessoBD->dataSet[0]->VALORAPAGAR);
          }  
        } else {
          $vAcessoBD->dataSet[0]->ENCARGOS= 0;
        }
        
      } elseif ($pOper === CTEEXTRATOIR) {
        $object = new stdClass();
        $object->COMPET = '';
      }

	  } else {
	    return 'ERRO: '.$vAcessoBD->erro.'---'.$vSQL;		
		}
    if ($pOper === CTEUSUVALIDAR) {
      $vRetorno= $vAcessoBD->dataSet[0]->CLICODIGO.'-'.$vAcessoBD->dataSet[0]->CLINOME;
    } else {
	    $vRetorno= $vAcessoBD->dataSetJSON();
    }  
		
	} catch (Exception $pExcecao) {
	  $vRetorno= 'ERRO: Excecao: '.$pExcecao->getMessage();
	}
	FINALLYY:
	unset($vAcessoBD);
  
  if ($vRetornoArr['erro'] != '') {
    //return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
  } else {
    return $vRetorno;
  } 
}	

function trocarSenha($pOper,$pOper2,$pOper3,$pOper4) {    
  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
  
  $vRetorno='0';
  
  if ($pOper2 === '') {
     $vRetorno='faltam parametros'; 
	  goto FINALLYY;
  } elseif (strlen($pOper3)!==32)  {
    $vRetorno='-1'; 
    goto FINALLYY;
  } elseif ( ($pOper===CTEUSUTROCARSENHA) && (strlen($pOper4)!==32))  {
    $vRetorno='-2'; 
    goto FINALLYY;
  }    
  
  $vAcessoBD->startTrans();
  $vSQL= "select count(CLIENTE.CLICODIGO) as TOTAL ".
         "from CLIENTE ".  
         "where CLIENTE.CLICODIGO=".$pOper2;
  
  if ($pOper===CTEUSUTROCARSENHA) {
    $vSQL.= " and CLIENTE.CLISENHAWEB='".$pOper3."'";
  }  

  //return $vSQL;             
  if ($vAcessoBD->open($vSQL)) {
    if ($vAcessoBD->dataSet[0]->TOTAL >0) {
      $vSQL= "update CLIENTE ";
      if ($pOper===CTEUSUTROCARSENHA) {
        $vSQL.= "set CLISENHAWEB='".$pOper4."' ";
      } else {
        $vSQL.= "set CLISENHAWEB='".$pOper3."' ";
      } 
      $vSQL.= "where CLIENTE.CLICODIGO=".$pOper2;

      if ($vAcessoBD->exec($vSQL)) {
        $vRetorno= $pOper2;      
      }  
    } else {  
      $vRetorno='-1';  
    }
  }  
  $vAcessoBD->commit();
  
  FINALLYY:
	unset($vAcessoBD);
  return $vRetorno;
}
/*
function criaAcessoBD() {
  if ($vAcessoBD = new ClassFB()) {
    $vAcessoBD->ip= cteGBD_IP;
    $vAcessoBD->user= cteGBD_USER;
    $vAcessoBD->password= cteGBD_PASSWORD;
    $vAcessoBD->database= cteGBD_PATH.cteGBD_BD;
    return $vAcessoBD;
  } else {
    return false;
  }
}
*/
?>