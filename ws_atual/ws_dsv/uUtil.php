<?php

function criaAcessoBD() {
  if ($vAcessoBD = new ClassFB()) {
    $vAcessoBD->ip= cteGBD_IP;
    $vAcessoBD->user= cteGBD_USER;
    $vAcessoBD->password= cteGBD_PASSWORD;
    $vAcessoBD->database= cteGBD_PATH.cteGBD_BD;
    return $vAcessoBD;
  } else {
    return false;
  }
}

// retorna uma string com aspas simples
function quotedSingleStr($pStr) {
  return '\''.$pStr.'\'';
}

/*
* trunca um valor com o numero de casas informado
* @param $pValor  float valor a ser truncado
* @param $pCasas int numero casas que ficaram

*/
function trunca($pValor, $pCasas = 2){
  return floor($pValor*pow(10,$pCasas))/pow(10,$pCasas);
}

function executarComandos($vSQL, $tipo) {   

 $vResult = '';   

 if (!$vAcessoBD = criaAcessoBD()) {
   return 'ERRO: erro na abertura do BD';
 } 

 if($tipo == 'comRetorno'){

   if ($vSQL !='') {
     try {
       if ($vAcessoBD->open($vSQL)) {
        if ($vAcessoBD->recordCount===0) {
          $vRetorno = 'Sem dados';
        }  

        $vRetorno= $vAcessoBD->dataSetJSON();
      } else {
       return 'ERRO: '.$vAcessoBD->erro.'---'.$vSQL;    
     }
   } catch (Exception $pExcecao) {
     $vRetorno= 'ERRO: Excecao: '.$pExcecao->getMessage();
   }  
 }

}else{

 if ($vSQL !='') {
   try {   
     $vRetorno = $vAcessoBD->exec($vSQL,true);      
  } catch (Exception $pExcecao) {
   $vRetorno = 'ERRO: Excecao: '.$pExcecao->getMessage();
 }  

}

if ($vRetorno === '') {
  $vAcessoBD->commit();     
  $vRetorno= $vNovoId;
} else {
  $vAcessoBD->rollback();    
}  

}

unset($vAcessoBD);
echo $vRetorno;

}

?>