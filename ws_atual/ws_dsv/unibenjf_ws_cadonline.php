<?php
header('Access-Control-Allow-Origin: *');

$vHelp = '
/*******************************************************************************
  - operacoes (parametro op)
    - ??? - retorna este help   
    - apv - aprova cadastro online
      
  - parametros
 
    op  	op2                      op3           op4         op5           op6           retorno
    --  	-----------------------  ----------    ---------   -----------   -----------   -----------
    apv  	sem uso                  sem uso       sem uso     sem uso
                                                                                                   
                                 
*******************************************************************************/
';

require_once("./uClassFB.php");
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php"); 
 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';
$vOper2 = (isset($_REQUEST['op2'])) ? $_REQUEST['op2'] : '';
$vOper3 = (isset($_REQUEST['op3'])) ? $_REQUEST['op3'] : '';
$vOper4 = (isset($_REQUEST['op4'])) ? $_REQUEST['op4'] : '';
$vOper5 = (isset($_REQUEST['op5'])) ? $_REQUEST['op5'] : '';
$vOper6 = (isset($_REQUEST['op6'])) ? $_REQUEST['op6'] : '';
$vOper7 = (isset($_REQUEST['op7'])) ? $_REQUEST['op7'] : '';
$vOper8 = (isset($_REQUEST['op8'])) ? $_REQUEST['op8'] : '';
$vOper9 = (isset($_REQUEST['op9'])) ? $_REQUEST['op9'] : '';
$vOper10 = (isset($_REQUEST['op10'])) ? $_REQUEST['op10'] : '';
$vOper11 = (isset($_REQUEST['op11'])) ? $_REQUEST['op11'] : '';
$vOper12 = (isset($_REQUEST['op12'])) ? $_REQUEST['op12'] : '';
$vOper13 = (isset($_REQUEST['op13'])) ? $_REQUEST['op13'] : '';
$vOper14 = (isset($_REQUEST['op14'])) ? $_REQUEST['op14'] : '';
$vOper15 = (isset($_REQUEST['op15'])) ? $_REQUEST['op15'] : '';
$vOper16 = (isset($_REQUEST['op16'])) ? $_REQUEST['op16'] : '';
$vOper17 = (isset($_REQUEST['op17'])) ? $_REQUEST['op17'] : '';

if ($vOper !== '') {
	if ($vOper==='???') {
		$vResult= $vHelp;
	} else {
	    define("CTEAPROVACADASTRO",'apv');
	    $vResult = alteracao($vOper,$vOper2,$vOper3,$vOper4,$vOper5,$vOper6,$vOper7,$vOper8,$vOper9,$vOper10,$vOper11,$vOper12,$vOper13,$vOper14,$vOper15,$vOper16,$vOper17);
   	}
}

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  	echo 'Erro desconhecido - '.$vOper;
} else {
  	echo $vResult;
}

$fp = fopen("./log/log.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 


function alteracao($vOper,$vOper2,$vOper3,$vOper4,$vOper5,$vOper6,$vOper7,$vOper8,$vOper9,$vOper10,$vOper11,$vOper12,$vOper13,$vOper14,$vOper15,$vOper16,$vOper17) {    
  	$vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  	if (!$vAcessoBD = criaAcessoBD()) {
	  	return 'ERRO: erro na abertura do BD';
  	}
	
	if (in_array($vOper,array(CTEAPROVACADASTRO))) {
	  	if ($vOper2 === '') {
		    $vRetorno= 'Requisicao invalida2';
		    goto FINALLYY;
	  	}
	}
  
	$vRetorno='';
	$vSQL= '';
  	$vNovoId=-1;
  	$vNovoSeu=-1;
  	$vNovoNosso=-1;
  	$vConvenio=-1;
  	$vAgora = new DateTime();
  
	if ($vOper === CTEAPROVACADASTRO) {
		$vSQL = 'select GEN_ID( G_CONTASARECEBER_TEMP, 1 ) as CODIGO FROM RDB$DATABASE';
		if ($vAcessoBD->open($vSQL)) {
	      	$vNovoId= $vAcessoBD->dataSet[0]->CODIGO;
	    } else {
	      	$vRetornoArr['erro'] = $vAcessoBD->erro;
	      	goto FINALLYY;    
	    }

		$vSQL = 'select GEN_ID( G_BOL_SEUNUMERO, 1 ) as CODIGO FROM RDB$DATABASE';
		if ($vAcessoBD->open($vSQL)) {
	      	$vNovoSeu= $vAcessoBD->dataSet[0]->CODIGO;
	    } else {
	      	$vRetornoArr['erro'] = $vAcessoBD->erro;
	      	goto FINALLYY;    
	    }

		$vSQL = 'select UNIEFICHACADBOLCONVENIO FROM UNIDADEEMPRESARIAL WHERE UNIEID = 2';
		if ($vAcessoBD->open($vSQL)) {
	      	$vConvenio= $vAcessoBD->dataSet[0]->UNIEFICHACADBOLCONVENIO;
	    } else {
	      	$vRetornoArr['erro'] = $vAcessoBD->erro;
	      	goto FINALLYY;    
	    }

		$vSQL = 'select GEN_ID( G_BOL_NOSSONUMERO, 1 ) as CODIGO FROM RDB$DATABASE';
		if ($vAcessoBD->open($vSQL)) {
	      	$vNovoNosso= $vAcessoBD->dataSet[0]->CODIGO;
	    } else {
	      	$vRetornoArr['erro'] = $vAcessoBD->erro;
	      	goto FINALLYY;    
	    }

    	$vSQL = "insert INTO CONTASARECEBER_TEMP 
    			(CRECTID, CRECTSTATUS, CRECTCPF, CRECTNOME, CRECTNUMERO, CRECTCOMPLEMENTO, CRECTCEP, CRECTVALOR, 
		    	CRECTDATAVENCIMENTO, CRECTDATALANCAMENTO, CRECTSEUNUMERO, 
		    	BANCOBOLETOCODCONVENIO, BANCOBOLETOEMISSAOLOCAL, MODOPCODIGO, UCODIGO, CRECTNOSSONUMERO, 
		    	CRECTMODOCOB, CRECTTIPOENTIDADE, UCODIGOALTERACAO, CRECTDATAALTERACAO, CRECTINSTRUCAO, 
		    	INSTCODIGO, CRECTMENSALIDADE, CRECTCOPARTICIPACAO, CRECTAVULSO, CRECT_TXASSOC, CRECT_TXINSC, CRECTLOGRADOURO, CRECTBAIRRO, 
		    	CRECTCIDADE, UFCODIGO, BANCOID, CRECTDATATRANSFCREC, UCONSULTOR, CRECTCOMPETENCIA) 
				VALUES
				(".$vNovoId.", 'AB', ".$vOper2.", '".$vOper3."', ".$vOper4.", ".$vOper5.", ".$vOper6.", ".$vOper7.", 
				".quotedSingleStr($vAgora->format('Y-m-d h:i:s')).", ".quotedSingleStr($vAgora->format('Y-m-d h:i:s')).", ".$vNovoSeu.", 
				".$vConvenio.", 'S', 1, ".$vOper8.", ".$vConvenio.str_pad($vNovoNosso, 10, '0', STR_PAD_LEFT).",
				'C', 'C', ".$vOper8.", ".quotedSingleStr($vAgora->format('Y-m-d h:i:s')).", 'Serviço de atendimento ao cliente: 0800 940 2151 | UNIBEN ADMINISTRADORA DE BENEFÍCIOS LTDA - Registro ANS: 41.852-8 | Juros de mora 0,033% ao dia | Multa de 2% ao mes |', 
				".$vOper9.", ".$vOper10.", 0.00, 0.00, ".$vOper11.", ".$vOper12.", '".$vOper13."', '".$vOper14."',
				'".$vOper15."', ".$vOper16.", 14, ".quotedSingleStr($vAgora->format('Y-m-d h:i:s')).", ".$vOper17.", 000000);";
	} else {
    	$vSQL = ''; 
  	}
  
  	if ($vSQL ==='') {
      	$vRetornoArr['erro'] = 'operacao desconhecida';
  	  	goto FINALLYY;
  	}
 
	if ($vAcessoBD->exec($vSQL)) {
      	$vRetornoArr['sucesso'] = true;
    } else {  
      	$vRetornoArr['erro'] = $vAcessoBD->erro;
      	goto FINALLYY;
    }

	FINALLYY:
	unset($vAcessoBD);
  
  	if ($vRetornoArr['erro'] != '') {
    	return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
  	} else {
  		$vRetornoArr['codigo']= $vNovoId;
  	} 

  FINALLYY_ALL:
  unset($vAcessoBD);
  return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}
?>