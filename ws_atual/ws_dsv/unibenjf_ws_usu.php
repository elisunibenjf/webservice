<?php
/*******************************************************************************
 * unibenjf_ws_usu.php
    - processa as requisicoes de usuario

  http://unibenjf.com.br/corretor  
 */

$vHelp = '
/*******************************************************************************************************
  - operacoes (parametro op)
    - ???  - help   
    - val  - valida o acesso de um usuario
    - sen  - muda a senha de um usuario
    - ce   - retorna o email baseado no cpf
    - msc  - muda senha apenas pelo id

   op    Parametros                                        retorno
   ====  ==============================================================================================
   val   log   - login com ate 10 caracteres         json de resposta: sucesso,erro,codigo e nome do usuario 
         senha - md5 da senha                                                       (0 se nao for encontrado) 
   ----  ----------------------------------------------------------------------------------------------
   sen   cod - codigo do usuario                     json de resposta: sucesso,erro
         sa  - senha antiga (MD5)
         sn  - nova senha (MD5)                                     
   ----  ----------------------------------------------------------------------------------------------
   ce    cpf - cpf sem formatacao                   json de resposta: sucesso,erro
   ----  ----------------------------------------------------------------------------------------------
   msc   cod - codigo do usuario                     json de resposta: sucesso,erro
         sn  - nova senha (MD5)                                     
********************************************************************************************************/
';

require_once("./uClassFB.php"); 
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php");

//xdebug_disable();
//error_reporting(E_ALL ^ E_WARNING); 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';

if ($vOper==='???') {
	$vResult= $vHelp;
} else {  
  require_once("./uOperacoesUsu.php");
  
  if (in_array($vOper,array(CTEUSUVAL,CTEUSUCPFEMAIL))) {  
    $vResult = lerdados($vOper);
  } elseif (in_array($vOper,array(CTEUSUMUDASENHA,CTEUSUMUDASENHACOD))) {
  	$vResult = trocarSenha($vOper);
  }  
} 

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log_usu.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 


/************************************************************************************************************/
function lerDados($pOper) {    
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  $vRetornoArr['erro']= 'erro na abertura do BD';
    goto FINALLYY;
  }
  
  $vSQL= '';

  if ($pOper === CTEUSUVAL) {
    $vSQL = "select USUARIO.UCODIGO as cod, USUARIO.UNOME as nome ".
            "from USUARIO ".
            "where USUARIO.ULOGIN='".((isset($_REQUEST['log'])) ? $_REQUEST['log'] : '')."' and ".
                  "USUARIO.UINFO='".((isset($_REQUEST['senha'])) ? $_REQUEST['senha'] : '')."'";
  } elseif ($pOper === CTEUSUCPFEMAIL) {         
    $vSQL = "SELECT ULOGIN, UCODIGO FROM USUARIO WHERE ULOGIN = UPPER('".$_REQUEST['usuario']."')";

  }  

  //return $vSQL;
  

  if ($vSQL ==='') {
    if ($vRetornoArr['erro']==='')  {
    $vRetornoArr['erro'] = 'operacao desconhecida';
    }
    goto FINALLYY;
  }

  try {
	if ($vAcessoBD->open($vSQL)) {
      if ($vAcessoBD->recordCount===0) {
      	if ($pOper === CTEUSUVAL) {
      	  $vRetornoArr['erro']= 'autenticacao invalida';	
      	} else {  
          $vRetornoArr['erro']= 'sem dados';
        } 
      } else {
        $vRetornoArr['sucesso']=true;
        $vRetornoArr['dados']= $vAcessoBD->dataSetJSON();
      }  
    } else {
	  $vRetornoArr['erro']= $vAcessoBD->erro;
	}
  } catch (Exception $vExcecao) {
    $vRetornoArr['erro'] = 'exception: '.$vExcecao->getMessage();
  }  
  
  FINALLYY:
  unset($vAcessoBD);
  return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}  


function trocarSenha($pOper) { 
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  $vRetornoArr['erro']= 'erro na abertura do BD';
    goto FINALLYY;
  } 
  
  $vCod = (isset($_REQUEST['cod'])) ? $_REQUEST['cod'] : '0';
  $vSA  = (isset($_REQUEST['sa'])) ? $_REQUEST['sa'] : '';
  $vSN  = (isset($_REQUEST['sn'])) ? $_REQUEST['sn'] : '';
  
  if (($pOper===CTEUSUMUDASENHA) && (strlen($vSA)!==32))  {
    $vRetornoArr['erro']= 'senha inválida';
    goto FINALLYY;
  } elseif (strlen($vSN)!==32)  {
    $vRetornoArr['erro']= 'nova senha inválida';
    goto FINALLYY;
  }    
  
  $vAcessoBD->startTrans();
  $vSQL= "select count(USUARIO.UCODIGO) as TOTAL ".
         "from USUARIO ".  
         "where USUARIO.UCODIGO=".$vCod;

  if ($pOper===CTEUSUMUDASENHA) {
    $vSQL.=" and USUARIO.UINFO='".$vSA."'";
  }

  //return $vSQL;             
  if ($vAcessoBD->open($vSQL)) {
    if ($vAcessoBD->dataSet[0]->TOTAL >0) {
      $vSQL= "update USUARIO ".
             "set UINFO='".$vSN."' ".
             "where USUARIO.UCODIGO=".$vCod;
      if ($vAcessoBD->exec($vSQL)) {
        $vRetornoArr['sucesso']=true;      
      } else {
      	$vRetornoArr['erro']= $vAcessoBD->erro;
      } 
    } else {  
      $vRetornoArr['erro']= 'usuario nao encontrado';
    }
  } else {
  	$vRetornoArr['erro']= $vAcessoBD->erro;
  } 
  $vAcessoBD->commit();
  
  FINALLYY:
  unset($vAcessoBD);
  return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}

/*

e10adc3949ba59abbe56e057f20f883e 123456

96e79218965eb72c92a549dd5a330112  111111


*/


?>



