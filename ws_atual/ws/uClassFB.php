<?php
  /**
   * classe para acesso a BDs Firebird
   */
  class classFB{
    // ip do gbd
    public $ip;
    // usuario do bd
    public $user;
    // senha do bd
    public $password;
    // charset do bd
    public $charset;
    // nome do bd
    public $database;

    // conexao ao BD
    private $sqlconn;
    // handle de transacao
    public $trans;
    // indica que todos comandos devem ocorrer usando a transacao corrente
    private $fInTrans = FALSE;

    // guarda o comando sql
    public $sql;
    // array com os dados lidos onde cada linha da matriz � um objeto contendo um registro
    public $dataSet;
    // quantidade de registros
    public $recordCount = 0;
    // ultima string de erro reportada
    public $erro;

    // indica se deve decodificar utf8
    public $utf8Decode = true;

    function __construct() {
      $this->ip          = '192.168.1.4';
      $this->user        = 'SYSDBA';
      $this->password    = 'masterkey';
      $this->charset     = 'ISO8859_1';
      $this->database    = '';
      $this->sql         = '';
      $this->dataSet     = array();
      $this->recordCount = 0;
      $this->erro        = '';
      $this->sqlconn     = NULL;
      $this->trans       = NULL;
    }

    function __destruct() {
      unset($this->trans);
      unset($this->sqlconn);
      $this->dataSet = array();
    }
    
    public function InTrans() {
      return $this->fInTrans;
    }

    /**
     * cria uma conexao com o BD configurado
     * @return true se foi bem sucedido ou false caso contrario 
     * @comment em caso de erro a mensagem � armazenada em $this->erro
     */ 
    private function criaConexao() {
      if ($this->sqlconn == NULL) {  
        if (!($this->sqlconn= ibase_connect($this->ip.':'.$this->database, $this->user, $this->password, $this->charset, 0, 3))) {
          $this->erro= 'conexao-'.ibase_errmsg();
          return false;
        } else {
          return true;
        }
      } else {
        return TRUE;
      }  
    }
    
    /**
     * cria uma transacao e a deixa pronta para uso. Se necessario cria a conexao tambem
     * @return o handle da transacao se foi bem sucedido ou false caso contrario 
     * @comment em caso de erro a mensagem � armazenada em $this->erro
     */
    public function createTrans() {
      if ($this->trans == NULL) { 
        if ($this->criaConexao()) {
          if ($this->trans = ibase_trans(IBASE_WRITE|IBASE_COMMITTED|IBASE_REC_NO_VERSION|IBASE_WAIT,$this->sqlconn)) {
            return $this->trans;
          } else {
            $this->erro= 'conexao-'.ibase_errmsg();
          }
        } else {
          $this->erro= 'conexao-'.ibase_errmsg();
        }
        return false;
      } else {
        return $this->trans;
      }     
    }
    
    /**
     * inicia uma transacao - a partir deste ponto todos os comandos usarao a transacao corrente ate que commit() ou rollback() seja chamado
     */
    public function startTrans() {
      $this->createTrans();
      $this->fInTrans= TRUE;
    } 
    
    /**
     * conclui a transacao aberta
     */
    public function commit() {
      if ($this->trans != NULL) {
        ibase_commit($this->trans);
        ibase_close($this->sqlconn);
        $this->sqlconn= null;
        $this->trans= null;
        $this->fInTrans= FALSE;
      }   
    }
    
    /**
     * desfaz a transacao aberta
     */
    public function rollback() {
      if ($this->trans != NULL) {
        ibase_rollback($this->trans);
        ibase_close($this->sqlconn);
        $this->sqlconn= null;
        $this->trans= null;
        $this->fInTrans= FALSE;
      }   
    }
    
    // 
    /**
     * executa uma query sem retorno
     *
     * @param $pSQL sql a ser executado - se for vazio tenta usar this->$sql senao usa $pSQL e guarda o valor em $sql
     * @param $pTrans se true recria $this->trans e executa uma transacao local senao admite que ja foi criada - em ambos os casos usa $this->trans
     * @comment se nao for usar transacoes admite que $this->slqconn e $this->trans estao ja criados
     */
    public function exec($pSQL = '', $pTrans = true) {
      $vRetorno          = false;
      $this->erro        = '';
      $this->recordCount = 0;
      if ($pSQL !== '') {
        $this->sql = $pSQL;
      }
      if ($this->sql === ''){
        $this->erro= 'exec-sql não definido';
      }
      if ($this->database === ''){
        $this->erro= 'exec-database não definido';
      }
      
      if ($pTrans) {
        if ($this->createTrans()) {
          if (ibase_query($this->trans,$this->sql)){
            ibase_commit($this->trans);
            $vRetorno= true;
          } else {
            $this->erro= 'exec-'.ibase_errmsg();
           ibase_rollback($this->trans);
          }  
          ibase_close($this->sqlconn);
          $this->sqlconn= null;
          $this->trans= null;
        } else {
          $vRetorno= $this->erro;
        }
      } else {
        if (ibase_query($this->trans,$this->sql)){
          $vRetorno= true;
        } else {
          $this->erro= 'exec-'.ibase_errmsg();
          //ibase_rollback($this->trans);
        }
      }      
      return $vRetorno;
    }

    /**
     * executa uma query retornando o resultado em um array de objetos no campo interno - $this->dataset
     * 
     * @param $pSQL sql a ser executado - se for vazio tenta usar this->$sql senao usa $pSQL e guarda o valor em $sql
     * @param $pTrans se true recria $this->trans e executa uma transacao local senao admite que ja foi criada - em ambos os casos usa $this->trans
     * @comment se nao for usar transacoes admite que $this->slqconn e $this->trans estao ja criados
     */
    public function open($pSQL = '',$pTrans = true){
      $vRetorno          = FALSE;
      $this->dataSet     = array();
      $this->erro        = '';
      $this->recordCount = 0;

      if ($pSQL != '') {
        $this->sql = $pSQL;
      }
      if ($this->sql === ''){
        $this->erro= 'open-sql não definido';
      }
      if ($this->database === ''){
        $this->erro= 'open-database não definido';
      }

      if ($pTrans) {
        $this->createTrans();
      }  
      
      $vQry = @ibase_query($this->trans,$this->sql);  
      if (!$vQry) {
        $this->erro= 'open-'.ibase_errmsg();
        $vRetorno= FALSE;
      } else {
        $vQtdCol = ibase_num_fields($vQry);
        // preenche array com os nomes dos campos blob
        $vBlobFields = array();
        for ($vCont=0; $vCont < $vQtdCol; $vCont++) {
          $vColInfo = ibase_field_info($vQry, $vCont);
          if ($vColInfo["type"]=="BLOB") {
            $vBlobFields[$vCont] = $vColInfo["name"];
          }
        }
        while ($vReg = ibase_fetch_object($vQry)) {
          // extrai a informacao de cada campo blob
          foreach ($vBlobFields as $vFieldNum=>$vFieldName) {
            $vBlobInfo= ibase_blob_info($vReg->$vFieldName);
            // necessario no caso do blob se null
            if ($vBlobInfo[0] > 0) {
              $vBlobId= ibase_blob_open($vReg->$vFieldName);
              $vReg->$vFieldName = ibase_blob_get($vBlobId,$vBlobInfo[0]);
              ibase_blob_close($vBlobId);
            }
          }
          $this->dataSet[]= $vReg;
          $this->recordCount++;
        }
        ibase_free_result($vQry);
        $vRetorno= TRUE;
      }
      
      if ($pTrans) {
        if ($vRetorno) {
          ibase_commit($this->trans);
        } else {  
          ibase_rollback($this->trans);
        }  
        ibase_close($this->sqlconn);
        $this->sqlconn= null;
        $this->trans= null;
      }   
             
      return $vRetorno;
    }

    // retorna o dataset atual como JSON oustring nula caso o dataset esteja vazio
    // parametros:
    //   $pUTF8 - se true indica que deve usar UTF8_encode nos elementos
    public function datasetJSON() {
      ini_set('memory_limit','-1');
      if ($this->recordCount === 0) {
        return false;
      } else {
        $vDataSet= $this->dataSet;
        if ($this->utf8Decode) {
          $vDataSet= $this->dataSet;
          foreach ($vDataSet as &$vLinha) {
            foreach ($vLinha as &$vItem) {
              $vItem = utf8_encode($vItem);
            }
          }
          unset($vItem);
          unset($vLinha);
        }
        return json_encode($vDataSet,JSON_HEX_APOS|JSON_HEX_QUOT);
      }
    }
  }
?>