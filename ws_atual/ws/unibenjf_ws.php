<?php
header('Access-Control-Allow-Origin: *');
/*******************************************************************************
 * uEscalaOperacao.php
    - processa as requisicoes da unibenjf
 */
$vHelp = '
/*******************************************************************************
  - operacoes (parametro op)
    - ??? - retorna este help   
    -  1  - instituicoes
    -  2  - produtos de uma instituicao
    -  3  - precos de um plano de uma instituicao
    -  4  - graus de parentesco
    -  5  - operadoras que atendem uma instituicao
    -  6  - acomodacoes
    -  7  - fator moderador
    -  8  - abrangencia
    -  9  - graus de parentesco permitidos para um produto
    - 10  - instituicoes que tem produto com uma operadora
    - 11  - valida um cliente
    - 12  - muda a senha de um cliente
    - 13  - lancamentos de um cliente na competencia
    - 14  - movimentacao financeira de um usuario no ano
    - 15  - retorna os dados para emissao de 2a via boleto 
    - 16  - extrato IR
    - luni  - lista de unidades de uma instituicao 
    - lemp  - lista empresas
    - lmop  - lista modos de pagamento
    - lban  - lista bancos 
    - lestc - lista estado civil
    - lcons - lista de consultores
    - cpfe  - retorna o email a partir de um CPF
    - msc   - muda senha do cliente apenas pelo codigo
    - ouvidoria  - verifica número de protocolo 
    - boletos - movimentacao financeira de um usuario no ano
    - serasa - lista debitos de clientes no serasa
      
  - parametros
 
    op  		op2                      op3           op4         op5           op6           retorno
    --  		-----------------------  ----------    ---------   -----------   -----------   -----------
    01  		sem uso                  sem uso       sem uso     sem uso
    02  		codigo da instituicao    codigo        codigo      codigo
                                 		 acomodacao    fator       abrangencia 
                                 		 (0-todos)     moderador   (0-todos)
                                               		   (0-todos)
    03  		codigo da instituicao    codigo        sem uso     sem uso
                                 		 produto       sem uso     sem uso
    04  		sem uso                  sem uso       sem uso     sem uso
    05  		codigo da instituicao    sem uso       sem uso     sem uso
    06  		sem uso                  sem uso       sem uso     sem uso
    07  		sem uso                  sem uso       sem uso     sem uso 
    08  		sem uso                  sem uso       sem uso     sem uso
    09  		codigo do produto        sem uso       sem uso     sem uso
    10  		codigo da operadora      sem uso       sem uso     sem uso
    11  		CPF do usuario           senha                                                 codigo do usuario-nome do usuario
                                 		 (MD5)                                                 ZERO se nao validar                                
    12  		codigo do usuario        senha antiga  nova senha                              se sucesso: codigo do usuario-nome
                                 		 (MD5)         (MD5)                                   se falhar:  	-1 senha antiga invalida
                                                                                                   			-2 tamanho invalido da senha nova                             
                                                                                       						ZERO outros erros
    13  		codigo do usuario        senha         competencia                             lancamentos do cliente
                                 		 (MD5)         (AAAAMM)
    14  		codigo do usuario        senha         ano                                     lancamentos da movimentacao financeira do cliente
                                 		 (MD5)         (AAAA)
    15  		codigo do usuario        senha         tipo        nosso         competencia   dados para emissao do boleto 
                                 		 (MD5)         entidade    numero        (AAAAMM)
                                               (C)	 
    16  		codigo do usuario        senha         ano                                     extrato de IR
                                 		 (MD5)         (AAAA)															 
																 
  luni			codigo da instituicao                                                          lista de unidades de uma instituicao 
  lemp                                                                                 		   lista empresas
  lmop                                                                                 		   lista modos de pagamento
  lban                                                                                 		   lista bancos 
  lestc                                                                                		   lista estado civil
  lcons                                                                                		   lista consultores
  cpfe   		cpf sem formatacao                                                             json de resposta
  ouvidoria   	protocolo digiado                                                              0 se não existe, 1 se existe
  boletos   	email do usuario         ano                                     			   lancamentos da movimentacao financeira do cliente
  ----  ----------------------------------------------------------------------------------------------
   msc   		codigo do usuario       nova senha                                             se sucesso: codigo do usuario-nome
                                 		(MD5)                                                  se falhar:  -1 senha invalida
                                                                                                   
                                 
*******************************************************************************/
';
/*******************************************************************************
Exemplos

http://127.0.0.1/unibenjf/ws/unibenjf_ws.php?op=12&op2=20&op3=8E62F252732EADF28315BD9F498D3356&op4=8E62F252732EADF28315BD9F498D3356

http://127.0.0.1/unibenjf/ws/unibenjf_ws.php?op=13&op2=20&op3=8E62F252732EADF28315BD9F498D3356&op4=201408

http://127.0.0.1/ws/unibenjf_ws.php?op=14&op2=402&op3=CBC8986A65372EDA6DAA78088C48D29D&op4=2014

http://187.32.177.136/ws/unibenjf_ws.php?op=15&op2=1544&op3=BB19E844A29320A5275977674802ABF4&op4=C&op5=27628980000001323&op6=201602

http://187.32.177.137/ws/unibenjf_ws.php?op=16&op2=402&op3=CBC8986A65372EDA6DAA78088C48D29D&op4=2016

http://127.0.0.1/ws/unibenjf_ws.php?op=14&op2=2003&op3=d320452797faa03475a65c8badecb758&op4=2014

site teste http://198.199.104.188/unibenjf/public/

*******************************************************************************/

require_once("./uClassFB.php");
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php"); 
 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';
$vOper2 = (isset($_REQUEST['op2'])) ? $_REQUEST['op2'] : '';
$vOper3 = (isset($_REQUEST['op3'])) ? $_REQUEST['op3'] : '';
$vOper4 = (isset($_REQUEST['op4'])) ? $_REQUEST['op4'] : '';
$vOper5 = (isset($_REQUEST['op5'])) ? $_REQUEST['op5'] : '';
$vOper6 = (isset($_REQUEST['op6'])) ? $_REQUEST['op6'] : '';

if ($vOper !== '') {
	if ($vOper==='???') {
		$vResult= $vHelp;
	} else {
    define("CTECONTAARECEBERPAGA",'PG');
	
  	define("CTELISTARINST",'1');
    define("CTELISTARINST2",'17');
  	define("CTELISTARINSTPROD",'2');
  	define("CTELISTARPRECO",'3');
  	define("CTELISTARGRAUPAR",'4');
  	define("CTEOPER_INST",'5');
  	define("CTELISTARACOMODACAO",'6');
  	define("CTELISTARFATORMODERADOR",'7');
  	define("CTELISTARABRANGENCIA",'8');
  	define("CTELISTAPRODGPAR",'9');
  	define("CTELISTAINSTOP",'10');
  	define("CTEUSUVALIDAR",'11');
  	define("CTEUSUTROCARSENHA",'12');
  	define("CTEUSUMOVMENSAL",'13');
   	define("CTEUSUMOVFIN",'14');
    define("CTEBOLETO2AVIA",'15');
    define("CTEEXTRATOIR",'16');
   	define("CTE2VIACLI",'18');
    define("CTE2VIAEMP",'19');

    define("CTELISTAUNI",'luni');
    define("CTELISTAEMP",'lemp');
    define("CTELISTAMOP",'lmop');
    define("CTELISTABAN",'lban');
    define("CTELISTAESTC",'lestc');
    define("CTELISTACONS",'lcons');

    define("CTECPFEMAIL",'cpfe');
    define("CTEMUDASENHACOD",'msc');
    define("CTEOUVIDORIA",'ouvidoria');
   	define("CTEUSUBOLETOS",'boletos');
   	define("CTESERASA",'serasa');
   	define("CTESERASANAOABERTO",'naoserasa');
   	define("CTENAOSERASABERTO",'simserasa');
   	define("CTELISTAOPERADORA",'oplista');
   	define("CTENOMEPRODUTO",'20');
   	define("CTELISTAINST",'instlista');
   	define("CTELISTAPLANO",'planolista');
   	define("CTECNPJINST",'instcnpj');
    define("CTELISTPERMISSOES",'acs');
   	define("CTEPLANO",'plano');
   	define("CTELISTACOP",'cop');
   	define("CTELISTAPROD",'preco');
   	define("CTELISTACLI",'cliativo');
   	define("CTELISTACLIE",'cliexcluido');

    if (in_array($vOper,array(CTEUSUTROCARSENHA,CTEMUDASENHACOD))) {
      $vResult = trocarSenha($vOper,$vOper2,$vOper3,$vOper4);
    } else {
	  	$vResult = lerDados($vOper,$vOper2,$vOper3,$vOper4,$vOper5,$vOper6);
    }  
	}	
}

// PARA DESABILITAR A GERACAO DE 2A VIA DE BOLETO PELO SITE
//if (in_array($vOper,array(CTEBOLETO2AVIA))) {
//  $vResult = '';  
//}

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 


function lerDados($pOper,$pOper2,$pOper3,$pOper4,$pOper5,$pOper6) {    
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
	
	if (in_array($pOper,array(CTELISTARINSTPROD,CTELISTARPRECO,CTEOPER_INST,CTELISTAPRODGPAR,CTELISTAINSTOP, CTECNPJINST, CTELISTAPLANO,
	                          CTEUSUVALIDAR,CTEUSUTROCARSENHA,CTEUSUMOVMENSAL,CTEUSUMOVFIN,CTEEXTRATOIR, CTELISTAINST, CTEPLANO, CTELISTACOP,
	                          CTELISTAUNI,CTECPFEMAIL,CTEOUVIDORIA, CTEUSUBOLETOS, CTE2VIACLI, CTE2VIAEMP, CTENOMEPRODUTO))) {
	  if ($pOper2 === '') {
	    $vRetorno= 'Requisicao invalida2';
	    goto FINALLYY;
	  }
	}
	
	if (in_array($pOper,array(CTELISTARPRECO,CTEUSUVALIDAR,CTEUSUTROCARSENHA,CTEUSUMOVMENSAL,CTEUSUMOVFIN,CTEEXTRATOIR, CTEUSUBOLETOS, CTELISTAPLANO))) {
	  if ($pOper3 === '') {
	    $vRetorno= 'Requisicao invalida3';
	    goto FINALLYY;
	  }
	}
  
  if (in_array($pOper,array(CTEUSUMOVFIN,CTEEXTRATOIR))) {
	  if ($pOper4 === '') {
	    $vRetorno= 'Requisicao invalida4';
	    goto FINALLYY;
	  }
	}
  
  if (in_array($pOper,array(CTEBOLETO2AVIA))) {
	  if ($pOper5 === '') {
	    $vRetorno= 'Requisicao invalida5';
	    goto FINALLYY;
	  }
	}
  
  if (in_array($pOper,array(CTEBOLETO2AVIA))) {
	  if ($pOper6 === '') {
	    $vRetorno= 'Requisicao invalida6';
	    goto FINALLYY;
	  }
	}
  
	$vRetorno='';
	$vSQL= '';
  
	if ($pOper === CTELISTARINST) {
    $vSQL = "select INSTCODIGO, INSTRAZAOSOCIAL from INSTITUICAO order by INSTRAZAOSOCIAL";
	} elseif ($pOper === CTELISTARINST2){
    $vSQL = "select INSTCODIGO, INSTRAZAOSOCIAL FROM INSTITUICAO WHERE INSTCODIGO IN(1,7,41,29,64,6,46,12,5,13,39,31,36,2,77,50,40,16,32,35,75,55,72,18,70) ORDER BY INSTRAZAOSOCIAL";
 	} elseif ($pOper === CTELISTARINSTPROD) {	
		$vSQL = "select IP.PRODCODIGO,P.PRODNOME,OP.OPRAZAOSOCIAL ".
            "from INSTPROD IP ".
            "inner join PRODUTO P on (P.PRODCODIGO=IP.PRODCODIGO) ".
						"inner join OPERADORA OP on (P.OPCODIGO=OP.OPCODIGO) ".			
						"where IP.INSTCODIGO=".$pOper2." and ".
					  "P.PRODINATIVO='N' and P.PRODCODIGO <> 9";
	  if (($pOper3 !== '') && ($pOper3 !== '0'))  {
	    $vSQL.= ' and P.ACMCODIGO='.$pOper3;
	  }
	  if (($pOper4 !== '') && ($pOper4 !== '0'))  {
	    $vSQL.= ' and P.FMCODIGO='.$pOper4;
	  }
	  if (($pOper5 !== '') && ($pOper5 !== '0'))  {
	    $vSQL.= ' and P.PRODAGCODIGO='.$pOper5;
	  }
	  if (($pOper6 !== '') && ($pOper6 !== '0'))  {
	    $vSQL.= ' and P.OPCODIGO='.$pOper6;
	  }
	  
		$vSQL.= " order by OP.OPRAZAOSOCIAL,P.PRODNOME";
	} elseif ($pOper === CTELISTARPRECO) {
		$vSQL = 'select PRECO.PRECOFAIXAINICIO as "Inicio",PRECO.PRECOFAIXAFIM as "Fim",PRECOVALOR as "Valor" '.
		        'from PRECO '.
		        'where PRECO.INSTCODIGO='.$pOper2." and ".
		           		'PRECO.PRODCODIGO='.$pOper3."  ".
        		'order by PRECO.PRECOFAIXAINICIO';
	} elseif ($pOper === CTELISTARGRAUPAR) {
		$vSQL = "select GPARCODIGO,GPARNOME from GRAUPARENTESCO order by GPARNOME";
  } elseif ($pOper === CTEOPER_INST) {
    $vSQL = "select distinct PRODUTO.OPCODIGO as CODIGO,OPERADORA.OPNOMEFANTASIA as NOME ".
            "from INSTPROD ".
            "inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
            "inner join OPERADORA on PRODUTO.OPCODIGO=OPERADORA.OPCODIGO ".
            "where INSTPROD.INSTCODIGO=".$pOper2." and ".
                  "PRODUTO.PRODSITCODIGO=1";
  } elseif ($pOper === CTELISTARACOMODACAO) {
    $vSQL = "select ACOMODACAO.ACMCODIGO,ACOMODACAO.ACMNOME from ACOMODACAO";
  } elseif ($pOper === CTELISTARFATORMODERADOR) {
    $vSQL = "select FATORMODERADOR.FMCODIGO,FATORMODERADOR.FMNOME from FATORMODERADOR";
  } elseif ($pOper === CTELISTARABRANGENCIA) {  
    $vSQL = "select PRODABRANGGEO.PRODAGCODIGO,PRODABRANGGEO.PRODAGNOME from PRODABRANGGEO";
  } elseif ($pOper === CTELISTAPRODGPAR) {
    $vSQL = "select GRAUPARENTESCO.GPARNOME ".
            "from PRODGPAR ".
            "inner join GRAUPARENTESCO on (GRAUPARENTESCO.GPARCODIGO=PRODGPAR.GPARCODIGO) ".
            "where PRODGPAR.PRODCODIGO=".$pOper2." ".
            "order by GRAUPARENTESCO.GPARNOME";
  } elseif ($pOper === CTELISTAINSTOP) {
    $vSQL = "select distinct INSTITUICAO.INSTCODIGO,INSTITUICAO.INSTRAZAOSOCIAL ".
            "from INSTITUICAO ".
            "inner join INSTPROD on INSTPROD.INSTCODIGO=INSTITUICAO.INSTCODIGO ".
            "inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
            "where PRODUTO.PRODINATIVO='N' and PRODUTO.OPCODIGO=".$pOper2." ".
            "order by INSTITUICAO.INSTRAZAOSOCIAL";
  } elseif ($pOper === CTEUSUVALIDAR) {
    $vSQL = "select CLIENTE.CLICODIGO,CLIENTE.CLINOME ".  
            "from CLIENTE ".	
			      "where CLIENTE.CLICPF='".$pOper2."' and ".
                  "CLIENTE.CLISENHAWEB='".$pOper3."'";
  } elseif ($pOper === CTEUSUMOVMENSAL) { 
   $vSQL = "select cm.CRECID, crec.CRECSTATUS, CM.CLIMMODOCOB AS MODO, ". 
            "FLT.FLANCTACRONIMO as TIPO,".
                   "case CM.CLIDEPCODIGO when 0 then C.CLINOME else coalesce(CD.CLIDEPNOME,'<<<DESCONHECIDO>>>') end as DEPEND,".
                   "CM.CLIMDATA as DATA,CM.CLIMDESCRICAO as Descricao,CM.CLIMQTD as QTD,".
                   "CM.CLIMVALORUNITARIOCOBRADO as ValorUnitario ".
            "from CLIMOVIMENTACAO CM ".  
            "inner join FLANCAMENTOTIPO FLT on (FLT.FLANCTCODIGO=CM.FLANCTCODIGO) ".
            "inner join CLIENTE C on (CM.CLICODIGO=C.CLICODIGO) ".
            "left join CLIDEP CD on (CM.CLICODIGO=C.CLICODIGO) and (CM.CLIDEPCODIGO=CD.CLIDEPCODIGO) ".
            "left join CONTASARECEBER crec on (crec.CRECID = cm.CRECID) ".
            "where CM.CLICODIGO='".$pOper2."' AND CREC.CRECSTATUS <> 'CA' AND CM.CLIMCOMPETENCIA='".$pOper4."'";

  } elseif (($pOper === CTEUSUMOVFIN) && ($pOper4 >= '2014')) {								
	$vAnoAnt= $pOper4 - 2;

    $vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
           'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
           "CONTASARECEBER.CRECSTATUS as Status,".
           "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
           'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
           "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
           'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
           "CONTASARECEBER.CRECNOSSONUMERO ".
                  
           "from CONTASARECEBER ".  
           "inner join CLIENTE C on (CONTASARECEBER.CRECENTIDADECODIGO=C.CLICODIGO) ".
           'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.

           "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
                "CONTASARECEBER.CRECSTATUS IN ('AB','PG') and".
                 "(CONTASARECEBER.CRECTIPOENTIDADE='C') and ".
                 "CONTASARECEBER.CRECENTIDADECODIGO='".$pOper2."' and ".
                 "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)>'".$vAnoAnt."' ".

           "order by CONTASARECEBER.CRECDATAVENCIMENTO DESC";
           
  } elseif ($pOper === CTEBOLETO2AVIA)  {

    $vSQL= "select CLIENTE.CLINOME as sac_nome,CLIENTE.CLICPF as sac_CPF,".
                  "coalesce(TL.TLOGNOME,'ND') || ' ' || coalesce(L.LOGNOME,'ND') || ', ' || CLIENTE.CLINUMERO || ' ' || ".
                  "coalesce(CLIENTE.CLICOMPLEMENTO,'',CLIENTE.CLICOMPLEMENTO) || ".
                  "'-' || coalesce(B.BAINOME,'ND') as sac_endereco,".
                  "CLIENTE.CLICEP as sac_cep,".
                  "coalesce(CID.CIDNOME,'ND') as sac_cidade,coalesce(U.UFSIGLA,'ND') as sac_uf,".

                  "CONTASARECEBER.CRECDATA,".
                  "CONTASARECEBER.CRECDATAVENCIMENTO as VENCIMENTOORIGINAL,".
                  "CONTASARECEBER.CRECDATAVENCIMENTO as VENCIMENTO,CONTASARECEBER.CRECVALORAPAGAR as valorapagar,".

                  "'UNIBEN ADMINISTRADORA DE BENEFICIOS LTDA' as ced_nome,".
                  "'Rua Braz Bernardino, 192 - Centro' as ced_endereco,'36010320' as ced_cep,".
                  "'Juiz de Fora' as ced_cidade,'MG' as ced_uf,".
                  "'15.156.097/0001-01' as ced_CNPJ,".

                  "CONTASARECEBER.CRECNOSSONUMERO as nossonumero,".
                  "CONTASARECEBER.CRECINSTRUCAO as instrucao,".
				          "CONTASARECEBER.CRECSEUNUMERO as seunumero,".
                  
                  "BANCO.BANCOCODIGO as banco, BANCO.BANCOAGENCIA as agencia, BANCO.BANCOCONTACORRENTE as contacorrente,".
                  "CONTASARECEBER.BANCOBOLETOCODCONVENIO as convenio,BANCO.BANCODIASVALIDADEBOLETO,".

                  "CONTASARECEBER.CRECENTIDADECODIGO as codigo, 0 as multa ".

           "from CONTASARECEBER ".
           "inner join CLIENTE on (CLIENTE.CLICODIGO=CONTASARECEBER.CRECENTIDADECODIGO) ".
           "left outer join LOGRADOURO L on (CLIENTE.LOGCODIGO = L.LOGCODIGO) ".
           "left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) ".
           "left outer join BAIRRO B on ( CLIENTE.BAICODIGO =  B.BAICODIGO) ".
           "inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) ".
           "inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) ".
           "inner join BANCO on (CONTASARECEBER.BANCOID=BANCO.BANCOID) ".
           "where CONTASARECEBER.CRECENTIDADECODIGO=".$pOper2.
                 " and CONTASARECEBER.CRECTIPOENTIDADE='".$pOper4."'".
                 " and CONTASARECEBER.CRECNOSSONUMERO='".$pOper5."'".
                 " and CONTASARECEBER.CRECCOMPETENCIA='".$pOper6."'";

   } elseif ($pOper === CTE2VIACLI) {
	$vAnoAnt= $pOper3 - 2;

    $vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
           'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
           "CONTASARECEBER.CRECSTATUS as Status,".
           "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
           'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
           "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
           'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
           "CONTASARECEBER.CRECNOSSONUMERO ".
                  
           "from CONTASARECEBER ".  
           "inner join CLIENTE C on (CONTASARECEBER.CRECENTIDADECODIGO=C.CLICODIGO) ".
           'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.

           "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
                "CONTASARECEBER.CRECSTATUS IN ('AB','PG') and".
                 "(CONTASARECEBER.CRECTIPOENTIDADE='C') and ".
                 "CONTASARECEBER.CRECENTIDADECODIGO='".$pOper2."' and ".
                 "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)>'".$vAnoAnt."' ".

           "order by CONTASARECEBER.CRECDATAVENCIMENTO DESC";
           
  } elseif ($pOper === CTE2VIAEMP) {
	$vAnoAnt= $pOper3 - 2;

	$vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
		       'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
		       "CONTASARECEBER.CRECSTATUS as Status,".
		       "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
		       'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
		       "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
		       'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
		       "CONTASARECEBER.CRECNOSSONUMERO ".
									
		       "from CONTASARECEBER ".  
			   "inner join EMPRESA E on (CONTASARECEBER.CRECENTIDADECODIGO=E.EMPCODIGO) ".
			   'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.
		       "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
           "(CONTASARECEBER.CRECTIPOENTIDADE='E') and ".
					 "CONTASARECEBER.CRECENTIDADECODIGO='".$pOper2."' and ".
					 "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)<='".$vAnoAnt."' ".
           "order by CONTASARECEBER.CRECCOMPETENCIA DESC";           
  } elseif ($pOper === CTEEXTRATOIR) {

    $vSQL= 'select DADOS.COMPET AS COMPET'.
                  ",case DADOS.CLIDEPCODIGO when 0 then 0 else 1 end as TIT".   
                  ",case DADOS.CLIDEPCODIGO when 0 then DADOS.CLINOME else coalesce(CLIDEP.CLIDEPNOME,'<<<DESCONHECIDO>>>') end as BENEFICIARIO".
                  ',FLANCAMENTOTIPO.FLANCTACRONIMO as TIPO'.
                  ',DADOS.VALOR_BASE,DADOS.ENCARGOS,DADOS.DESCONTO'.
                  ',(DADOS.VALOR_BASE+DADOS.ENCARGOS) AS TOTAL '.
                  ',case DADOS.CLIDEPCODIGO when 0 then DADOS.CLICPF else CLIDEP.CLIDEPCPF end as CPF '.
           'from (select CMOV.*,'.
                        'case CMOV.FLANCTCODIGO '.
                           'when 1 then (select coalesce(sum(CONTASARECEBER.CRECMORAMULTA),0) '.
                                        'from CONTASARECEBER '.
                                        'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
                                              "(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and ".
                                              "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
                                              "(CONTASARECEBER.CRECTIPOENTIDADE='C')".
                                       ") ".
                           'else 0 '.
                        'end as ENCARGOS,'.

                        'case CMOV.FLANCTCODIGO '.
                         'when 1 then (select coalesce(sum(CONTASARECEBER.CRECDESCONTO),0) '.
                                      'from CONTASARECEBER '.
                                      'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
                                            '(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and '.
                                            "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
                                            "(CONTASARECEBER.CRECTIPOENTIDADE='C')".
                                      ') '.

                         'else 0 '.
                       'end as DESCONTO '.

                  'from (select CM.CLIMCOMPETENCIA AS COMPET,CM.CLICODIGO,CM.CLIDEPCODIGO,CM.FLANCTCODIGO,C.CLINOME,C.CLICPF,'.
                               'cast( sum(CM.CLIMQTD*CM.CLIMVALORUNITARIOCOBRADO) as decimal(10,2))as VALOR_BASE '.
                        'from CLIMOVIMENTACAO CM '.
                        'inner join CLIENTE C on (CM.CLICODIGO=C.CLICODIGO) '.
                        'inner join CONTASARECEBER CR1 on (CR1.CRECID=CM.CRECID) '.
                        'where CM.CLICODIGO='.$pOper2.
                              " and substring(CR1.CRECDATAPAGAMENTO from 1 for 4)='".$pOper4."' ".
                              " and CR1.CRECSTATUS='".CTECONTAARECEBERPAGA."' ".
                              " and CM.CLIMMODOCOB='C' ".
                        'group by 1,2,3,4,5,6 '.
                       ') as CMOV '.
                       ' order by COMPET '.
                ') as DADOS '.
           'inner join FLANCAMENTOTIPO on (FLANCAMENTOTIPO.FLANCTCODIGO=DADOS.FLANCTCODIGO) '.
           'left join CLIDEP on (DADOS.CLICODIGO=DADOS.CLICODIGO) and (DADOS.CLIDEPCODIGO=CLIDEP.CLIDEPCODIGO) '.
           'order by 2,3,1';          

  } elseif ($pOper === CTELISTAUNI)  {	                              
    $vSQL = "select INSTUNI.INSTUNICODIGO,INSTUNI.INSTUNINOME ".
            "from INSTUNI ".
            "where INSTUNI.INSTCODIGO=".$pOper2;
  } elseif ($pOper === CTELISTAEMP)  {	                              
    $vSQL = "select EMPRESA.EMPCODIGO,EMPRESA.EMPNOME,EMPRESA.EMPRAZAOSOCIAL ".
            "from EMPRESA ".
            "order by EMPRESA.EMPRAZAOSOCIAL";
  } elseif ($pOper === CTELISTAMOP)  {	                              
    $vSQL = "select MODOPAGAMENTO.MODOPCODIGO,MODOPAGAMENTO.MODOPNOME ".
            "from MODOPAGAMENTO ".
            "order by MODOPAGAMENTO.MODOPNOME";          
  } elseif ($pOper === CTELISTABAN)  {	                              
    $vSQL = "select BANCO.BANCOID,BANCO.BANCOCODIGO,BANCO.BANCONOME,BANCO.UNIEID ".
            "from BANCO ".
            "order by BANCO.BANCONOME"; 
  } elseif ($pOper === CTELISTAESTC)  {                                
    $vSQL = "select ESTADOCIVIL.ESTCCODIGO,ESTADOCIVIL.ESTCNOME ".
            "from ESTADOCIVIL ".
            "order by ESTADOCIVIL.ESTCNOME";           
  } elseif ($pOper === CTELISTACONS)  {                                
    $vSQL = 'select UCODIGO as "codigo",UNOME as "nome" '.
            "from USUARIO ".
            "where UINATIVO='N' ".
            "order by UNOME";                   
  } elseif ($pOper === CTECPFEMAIL)  {                                
    $vSQL = 'select CLICODIGO as "codigo", CLICPF AS "cpf", CLIEMAIL as "email" '.
            "from CLIENTE ".
            "where CLICPF='".$pOper2."'";
  } elseif ($pOper === CTEOUVIDORIA)  {                                
    $vSQL = 'select count(*) '.
            "from ATENDIMENTO ".
            "where ATPROTOCOLO='".$pOper2."'";
  } elseif (($pOper === CTEUSUBOLETOS) && ($pOper3 >= '2014')) {								
	$vAnoAnt= $pOper3 - 2;

    $vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
           'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
           "CONTASARECEBER.CRECSTATUS as Status,".
           "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
           'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
           "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
           'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
           "CONTASARECEBER.CRECNOSSONUMERO ".
                  
           "from CONTASARECEBER ".  
           "inner join CLIENTE C on (CONTASARECEBER.CRECENTIDADECODIGO=C.CLICODIGO) ".
           'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.

           "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
                "CONTASARECEBER.CRECSTATUS IN ('AB','PG') and".
                 "(CONTASARECEBER.CRECTIPOENTIDADE='C') and ".
                 "C.CLICPF = '".$pOper2."' and ".
                 "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)>'".$vAnoAnt."' ".

           "order by CONTASARECEBER.CRECDATAVENCIMENTO DESC";
           
  } elseif (($pOper === CTESERASA)) {

    $vSQL= "select CLIENTE.CLINOME, CLIENTE.CLICPF, CONTASARECEBER.CRECCOMPETENCIA, CONTASARECEBER.CRECMENSALIDADE, CONTASARECEBER.CRECSTATUS, ".
           "CONTASARECEBER.CRECCOPARTICIPACAO, CRECAVULSO, CRECVALORAPAGAR, CRECVALORPAGO, CRECAVULSO, CLIENTE.CLIREGISTROSERASA FROM CONTASARECEBER  ".
           "JOIN CLIENTE ON CLIENTE.CLICODIGO = CONTASARECEBER.CRECENTIDADECODIGO ".
           "WHERE CLIENTE.CLIREGISTROSERASA = 'S' AND CONTASARECEBER.CRECSTATUS = 'AB' ".
           "ORDER BY CLIENTE.CLICPF";
           
  } elseif (($pOper === CTESERASANAOABERTO)) {

    $vSQL= "select CLIENTE.CLINOME, CLIENTE.CLICPF, CONTASARECEBER.CRECCOMPETENCIA, CONTASARECEBER.CRECMENSALIDADE, CONTASARECEBER.CRECSTATUS, ".
           "CONTASARECEBER.CRECCOPARTICIPACAO, CRECAVULSO, CRECVALORAPAGAR, CRECVALORPAGO, CRECAVULSO, CLIENTE.CLIREGISTROSERASA FROM CONTASARECEBER  ".
           "JOIN CLIENTE ON CLIENTE.CLICODIGO = CONTASARECEBER.CRECENTIDADECODIGO ".
           "WHERE CLIENTE.CLIREGISTROSERASA = 'S' AND CONTASARECEBER.CRECSTATUS <> 'AB' ". 
		   "AND CLIENTE.CLICODIGO NOT IN (SELECT DISTINCT CONTASARECEBER.CRECENTIDADECODIGO  ".
		   		"FROM CONTASARECEBER WHERE CONTASARECEBER.CRECSTATUS = 'AB' and CONTASARECEBER.CRECTIPOENTIDADE = 'C') ".
           "ORDER BY CLIENTE.CLICPF";
           
  } elseif (($pOper === CTENAOSERASABERTO)) {

    $vSQL= "select CLIENTE.CLINOME, CLIENTE.CLICPF, CONTASARECEBER.CRECCOMPETENCIA, CONTASARECEBER.CRECMENSALIDADE, CONTASARECEBER.CRECSTATUS, ".
           "CONTASARECEBER.CRECCOPARTICIPACAO, CRECAVULSO, CRECVALORAPAGAR, CRECVALORPAGO, CRECAVULSO, CLIENTE.CLIREGISTROSERASA FROM CONTASARECEBER  ".
           "JOIN CLIENTE ON CLIENTE.CLICODIGO = CONTASARECEBER.CRECENTIDADECODIGO ".
           "WHERE CLIENTE.CLIREGISTROSERASA = 'N' AND CONTASARECEBER.CRECSTATUS = 'AB' ".
           "ORDER BY CLIENTE.CLICPF";
           
  } elseif (($pOper === CTENOMEPRODUTO)) {		
    $vSQL= "select PRODNOME ".                  
           "from PRODUTO ".  
           "WHERE PRODCODIGO = ".$pOper2;           
  } elseif (($pOper === CTELISTAOPERADORA)) {		
    $vSQL= "select OPCODIGO, OPRAZAOSOCIAL, OPNOMEFANTASIA, OPCNPJ, OPREGISTROANS, ".                  
           "LOGCODIGO, OPNUMERO, OPCOMPLEMENTO, BAICODIGO, OPCEP ".  
           "FROM OPERADORA WHERE OPINATIVO = 'N' ";  
           if($pOper2 !== ''){
           	$vSQL .= "AND OPCODIGO =".$pOper2;
           }
  } elseif (($pOper === CTELISTAPLANO)) {		
    $vSQL= "select DISTINCT PRODUTO.prodcodigo, prodnome ".
			"from INSTPROD ".
			"INNER JOIN INSTITUICAO ON instprod.instcodigo=instituicao.instcodigo ".
			"inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
			"inner join OPERADORA on PRODUTO.OPCODIGO=OPERADORA.OPCODIGO  ".
			"where PRODUTO.PRODINATIVO = 'N' AND PRODUTO.PRODSITCODIGO = 1 AND OPERADORA.OPCODIGO = ".$pOper2. "and INSTITUICAO.INSTCODIGO = ".$pOper3;
  } elseif (($pOper === CTELISTAINST)) {		
    $vSQL= "select DISTINCT INSTITUICAO.instcodigo, instrazaosocial ".
			"from INSTPROD ".
			"INNER JOIN INSTITUICAO ON instprod.instcodigo=instituicao.instcodigo ".
			"inner join PRODUTO on PRODUTO.PRODCODIGO=INSTPROD.PRODCODIGO ".
			"inner join OPERADORA on PRODUTO.OPCODIGO=OPERADORA.OPCODIGO  ".
			"where OPERADORA.OPCODIGO = ".$pOper2;
  } elseif (($pOper === CTELISTACOP)) {		
    $vSQL= "select CLINOME, CLICPF, CLIMOVIMENTACAO.CLIMCOMPETENCIA, OPNOMEFANTASIA, PRODNOME, FLANCTNOME, CLIMOVIMENTACAO.CLIMDATA, CLIMOVIMENTACAO.CLIMDESCRICAO ".
			"FROM CLIMOVIMENTACAO  ".
			"JOIN CLIENTE ON CLIENTE.CLICODIGO=CLIMOVIMENTACAO.CLICODIGO ".
			"JOIN FLANCAMENTOTIPO ON FLANCAMENTOTIPO.FLANCTCODIGO = CLIMOVIMENTACAO.FLANCTCODIGO ".
			"JOIN PRODUTO ON PRODUTO.PRODCODIGO=CLIMOVIMENTACAO.PRODCODIGO ".
			"JOIN OPERADORA ON OPERADORA.OPCODIGO=CLIMOVIMENTACAO.OPCODIGO ".
			"WHERE CLIMOVIMENTACAO.FLANCTCODIGO IN (3,4) AND CLIENTE.CLINOME = '".$pOper2."'";
  } elseif (($pOper === CTEPLANO)) {		
    $vSQL= "select DISTINCT PRODNOME, PRODNUMREGISTRO AS ANS, ACMNOME AS ACOMODACAO, FMNOME AS COPARTICIPACAO, PRODAGNOME AS ABRANGENCIA ".
			"from PRODUTO  ".
			"JOIN PRODABRANGGEO ON PRODABRANGGEO.PRODAGCODIGO = PRODUTO.PRODAGCODIGO ".
			"JOIN FATORMODERADOR ON FATORMODERADOR.FMCODIGO = PRODUTO.FMCODIGO ".
			"JOIN ACOMODACAO ON ACOMODACAO.ACMCODIGO = PRODUTO.ACMCODIGO ".
			"where PRODCODIGO= ".$pOper2;
  } elseif (($pOper === CTELISTAPROD)) {		
    $vSQL= 'select PROD.PRODNOME as "Produto", P.PRECOFAIXAINICIO as "FaixaInicio", P.PRECOFAIXAFIM as "FaixaFim",
    		P.PRECOVALOR as "Valor", I.INSTRAZAOSOCIAL as "Instituicao", OP.OPRAZAOSOCIAL as "Operadora" 
    		from PRECO P 
			inner join INSTITUICAO I on (P.INSTCODIGO=I.INSTCODIGO) 
			inner join PRODUTO PROD on (P.PRODCODIGO=PROD.PRODCODIGO) 
			inner join OPERADORA OP on (PROD.OPCODIGO=OP.OPCODIGO)';
  } elseif (($pOper === CTECNPJINST)) {		
    $vSQL= "select instcnpj from INSTITUICAO ".
			"where INSTCODIGO = ".$pOper2;
  } elseif ($pOper === CTELISTPERMISSOES)  {                                
    $vSQL = "select uacesso FROM usuario WHERE ucodigo =".$pOper2;
  } elseif (($pOper === CTELISTACLI)) {		
    $vSQL= "select Nome, Titular, Endereco, Numero, Complemento, Bairro, Cidade, UF, data_Nasc, EstadoCivil, CPF, 
			RG, CEP, Tel, Grau_Parentesco, cadastro, Email, Mensalidade, ValorOperadora, Empresa, EmpCNPJ, DiaVenc, CodExtTit 
			from (select C.CLINOME as Nome, C.CLINOME as Titular, TL.TLOGNOME || ' ' || L.LOGNOME as Endereco,
			C.CLINUMERO as Numero,case   when C.CLICOMPLEMENTO is null then ''   else ' ' || C.CLICOMPLEMENTO end as Complemento,
			B.BAINOME as Bairro,CID.CIDNOME as Cidade,U.UFSIGLA as UF, C.CLIDATANASCIMENTO as data_Nasc, EC.ESTCNOME as EstadoCivil,
			C.CLICPF as CPF, C.CLIRG AS RG, C.CLICEP as CEP, (select first 1 CLITELEFONE.CLITDDD || '-' || CLITELEFONE.CLITNUMERO 
			from CLITELEFONE where (CLITELEFONE.CLICODIGO=C.CLICODIGO) ) as Tel, 'Titular' as Grau_Parentesco,
			cast(C.CLIDATACADASTRO as date) as cadastro, C.CLIEMAIL as Email, 
			coalesce((select cast(sum(P.PRECOVALOR*(1-(CP.CLIDESCONTO/100))) as NUMERIC(10,2)) from CLIPROD CP 
			inner join PRECO P on (P.INSTCODIGO=CP.INSTCODIGO) and (P.PRODCODIGO=CP.PRODCODIGO) 
			where (CP.CLICODIGO=C.CLICODIGO) and (CP.CLIPRODATIVO='S') and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) >= P.PRECOFAIXAINICIO) and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) <= P.PRECOFAIXAFIM)),0) as Mensalidade,coalesce((select sum(P.PRECOVALOROPERADORA) 
			from CLIPROD CP inner join PRECO P on (P.INSTCODIGO=CP.INSTCODIGO) and (P.PRODCODIGO=CP.PRODCODIGO) 
			where (CP.CLICODIGO=C.CLICODIGO) and (CP.CLIPRODATIVO='S') and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) >= P.PRECOFAIXAINICIO) and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) <= P.PRECOFAIXAFIM)),0) as ValorOperadora,coalesce(E1.EMPRAZAOSOCIAL,'ND') as Empresa,coalesce(E1.EMPCNPJ,'ND') as EmpCNPJ,
			C.CLIDIAVENCIMENTO as DiaVenc,coalesce(C.CLICODIGOEXTERNO,'-') as CodExtTit from CLIENTE C 
			left outer join LOGRADOURO L on (C.LOGCODIGO = L.LOGCODIGO) 
			left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
			left outer join BAIRRO B on (C.BAICODIGO =  B.BAICODIGO) 
			inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
			inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) 
			left join UNIDADEFEDERATIVA URG on (C.CLIRGUFCODIGO = URG.UFCODIGO) 
			left join PAIS PRG on (C.CLIRGPAISCODIGO = PRG.PAISCODIGO) 
			inner join ESTADOCIVIL EC on (EC.ESTCCODIGO=C.ESTCCODIGO) 
			left outer join EMPRESA E1 on (C.EMPCODIGO=E1.EMPCODIGO) 
			WHERE C.CLISTATUS = 'A'
			union 
			select CD.CLIDEPNOME as Nome,C1.CLINOME as Titular,TL.TLOGNOME || ' ' || L.LOGNOME as Endereco, C1.CLINUMERO as Numero,
			case when C1.CLICOMPLEMENTO is null then '' else ' ' || C1.CLICOMPLEMENTO end as Complemento,B.BAINOME as Bairro,
			CID.CIDNOME as Cidade,U.UFSIGLA as UF, CD.CLIDEPDATANASCIMENTO as data_Nasc, EC.ESTCNOME as EstadoCivil, CD.CLIDEPCPF as CPF,
			CD.CLIDEPRG  asRG, '-' as CEP, (select first 1 CLITELEFONE.CLITDDD || '-' || CLITELEFONE.CLITNUMERO from CLITELEFONE 
			where (CLITELEFONE.CLICODIGO=CD.CLICODIGO) ) as Tel, GP.GPARNOME as Grau_Parentesco,
			cast(CD.CLIDEPDATACADASTRO as date) as cadastro, '-' as Email, 0 as Mensalidade, 0 as ValorOperadora,
			coalesce(E1.EMPRAZAOSOCIAL,'ND') as Empresa,coalesce(E1.EMPCNPJ,'ND') as EmpCNPJ, C1.CLIDIAVENCIMENTO as DiaVenc,
			coalesce(C1.CLICODIGOEXTERNO,'-') as CodExtTit from CLIDEP CD 
			inner join CLIENTE C1 on (CD.CLICODIGO=C1.CLICODIGO) 
			left outer join LOGRADOURO L on (C1.LOGCODIGO = L.LOGCODIGO) 
			left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
			left outer join BAIRRO B on (C1.BAICODIGO =  B.BAICODIGO) 
			inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
			inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) 
			left join UNIDADEFEDERATIVA URG on (CD.CLIDEPRGUFCODIGO = URG.UFCODIGO) 
			left join PAIS PRG on (CD.CLIDEPRGPAISCODIGO = PRG.PAISCODIGO) 
			inner join ESTADOCIVIL EC on (EC.ESTCCODIGO=CD.ESTCCODIGO) 
			inner join GRAUPARENTESCO GP on (GP.GPARCODIGO=CD.GPARCODIGO) 
			left outer join EMPRESA E1 on (C1.EMPCODIGO=E1.EMPCODIGO)
			WHERE CD.CLIDEPSTATUS = 'A') 
			order by Nome";
  } elseif (($pOper === CTELISTACLIE)) {		
    $vSQL= "select Nome, Titular, Endereco, Numero, Complemento, Bairro, Cidade, UF, data_Nasc, EstadoCivil, CPF, 
			RG, CEP, Tel, Grau_Parentesco, cadastro, Email, Mensalidade, ValorOperadora, Empresa, EmpCNPJ, DiaVenc, CodExtTit 
			from (select C.CLINOME as Nome, C.CLINOME as Titular, TL.TLOGNOME || ' ' || L.LOGNOME as Endereco,
			C.CLINUMERO as Numero,case   when C.CLICOMPLEMENTO is null then ''   else ' ' || C.CLICOMPLEMENTO end as Complemento,
			B.BAINOME as Bairro,CID.CIDNOME as Cidade,U.UFSIGLA as UF, C.CLIDATANASCIMENTO as data_Nasc, EC.ESTCNOME as EstadoCivil,
			C.CLICPF as CPF, C.CLIRG AS RG, C.CLICEP as CEP, (select first 1 CLITELEFONE.CLITDDD || '-' || CLITELEFONE.CLITNUMERO 
			from CLITELEFONE where (CLITELEFONE.CLICODIGO=C.CLICODIGO) ) as Tel, 'Titular' as Grau_Parentesco,
			cast(C.CLIDATACADASTRO as date) as cadastro, C.CLIEMAIL as Email, 
			coalesce((select cast(sum(P.PRECOVALOR*(1-(CP.CLIDESCONTO/100))) as NUMERIC(10,2)) from CLIPROD CP 
			inner join PRECO P on (P.INSTCODIGO=CP.INSTCODIGO) and (P.PRODCODIGO=CP.PRODCODIGO) 
			where (CP.CLICODIGO=C.CLICODIGO) and (CP.CLIPRODATIVO='S') and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) >= P.PRECOFAIXAINICIO) and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) <= P.PRECOFAIXAFIM)),0) as Mensalidade,coalesce((select sum(P.PRECOVALOROPERADORA) 
			from CLIPROD CP inner join PRECO P on (P.INSTCODIGO=CP.INSTCODIGO) and (P.PRODCODIGO=CP.PRODCODIGO) 
			where (CP.CLICODIGO=C.CLICODIGO) and (CP.CLIPRODATIVO='S') and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) >= P.PRECOFAIXAINICIO) and ((select * from SP_IDADEANOS(C.CLIDATANASCIMENTO,
			cast('2019-07-01' as date))) <= P.PRECOFAIXAFIM)),0) as ValorOperadora,coalesce(E1.EMPRAZAOSOCIAL,'ND') as Empresa,coalesce(E1.EMPCNPJ,'ND') as EmpCNPJ,
			C.CLIDIAVENCIMENTO as DiaVenc,coalesce(C.CLICODIGOEXTERNO,'-') as CodExtTit from CLIENTE C 
			left outer join LOGRADOURO L on (C.LOGCODIGO = L.LOGCODIGO) 
			left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
			left outer join BAIRRO B on (C.BAICODIGO =  B.BAICODIGO) 
			inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
			inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) 
			left join UNIDADEFEDERATIVA URG on (C.CLIRGUFCODIGO = URG.UFCODIGO) 
			left join PAIS PRG on (C.CLIRGPAISCODIGO = PRG.PAISCODIGO) 
			inner join ESTADOCIVIL EC on (EC.ESTCCODIGO=C.ESTCCODIGO) 
			left outer join EMPRESA E1 on (C.EMPCODIGO=E1.EMPCODIGO) 
			WHERE C.CLISTATUS = 'E'
			union 
			select CD.CLIDEPNOME as Nome,C1.CLINOME as Titular,TL.TLOGNOME || ' ' || L.LOGNOME as Endereco, C1.CLINUMERO as Numero,
			case when C1.CLICOMPLEMENTO is null then '' else ' ' || C1.CLICOMPLEMENTO end as Complemento,B.BAINOME as Bairro,
			CID.CIDNOME as Cidade,U.UFSIGLA as UF, CD.CLIDEPDATANASCIMENTO as data_Nasc, EC.ESTCNOME as EstadoCivil, CD.CLIDEPCPF as CPF,
			CD.CLIDEPRG  asRG, '-' as CEP, (select first 1 CLITELEFONE.CLITDDD || '-' || CLITELEFONE.CLITNUMERO from CLITELEFONE 
			where (CLITELEFONE.CLICODIGO=CD.CLICODIGO) ) as Tel, GP.GPARNOME as Grau_Parentesco,
			cast(CD.CLIDEPDATACADASTRO as date) as cadastro, '-' as Email, 0 as Mensalidade, 0 as ValorOperadora,
			coalesce(E1.EMPRAZAOSOCIAL,'ND') as Empresa,coalesce(E1.EMPCNPJ,'ND') as EmpCNPJ, C1.CLIDIAVENCIMENTO as DiaVenc,
			coalesce(C1.CLICODIGOEXTERNO,'-') as CodExtTit from CLIDEP CD 
			inner join CLIENTE C1 on (CD.CLICODIGO=C1.CLICODIGO) 
			left outer join LOGRADOURO L on (C1.LOGCODIGO = L.LOGCODIGO) 
			left outer join TIPOLOGRADOURO TL on (L.TLOGCODIGO = TL.TLOGCODIGO) 
			left outer join BAIRRO B on (C1.BAICODIGO =  B.BAICODIGO) 
			inner join CIDADE CID on (B.CIDCODIGO = CID.CIDCODIGO) 
			inner join UNIDADEFEDERATIVA U on (CID.UFCODIGO = U.UFCODIGO) 
			left join UNIDADEFEDERATIVA URG on (CD.CLIDEPRGUFCODIGO = URG.UFCODIGO) 
			left join PAIS PRG on (CD.CLIDEPRGPAISCODIGO = PRG.PAISCODIGO) 
			inner join ESTADOCIVIL EC on (EC.ESTCCODIGO=CD.ESTCCODIGO) 
			inner join GRAUPARENTESCO GP on (GP.GPARCODIGO=CD.GPARCODIGO) 
			left outer join EMPRESA E1 on (C1.EMPCODIGO=E1.EMPCODIGO)
			WHERE CD.CLIDEPSTATUS = 'E') 
			order by Nome";
  } else {
    $vSQL = ''; 
  }  
	if ($vSQL ==='') {
	  goto FINALLYY;
	}
	
	try {
	  if ($vAcessoBD->open($vSQL)) {	
			if ($vAcessoBD->recordCount===0) {
        if ($pOper === CTEUSUVALIDAR) {
          $vRetorno= '0';
        } else {  
	        $vRetorno = 'Sem dados';
        }  
				goto FINALLYY;
			}
 
       

   

      if ($pOper === CTEBOLETO2AVIA) {	
        date_default_timezone_set('America/Sao_Paulo');
        $vAgora = new DateTime();
        $vAgora->setTime(0,0,0); 

        

        // verifica se boleto nao expirou
        if ($vAcessoBD->dataSet[0]->BANCODIASVALIDADEBOLETO != 0) {
          $vCRECDATA =  New DateTime($vAcessoBD->dataSet[0]->CRECDATA); 
          $vDiferenca = $vCRECDATA->diff($vAgora);
          $vDias= (int)$vDiferenca->format('%R%a');
          if ($vDias > $vAcessoBD->dataSet[0]->BANCODIASVALIDADEBOLETO) {
            $vRetornoArr['erro']= 'BOLETO EXPIRADO. Entre em contato com o setor financeiro';
            goto FINALLYY;
          }
        } 

        $vAcessoBD->dataSet[0]->VENCIMENTO= $vAgora->format('Y-m-d');   
        
        $vVenc= new DateTime($vAcessoBD->dataSet[0]->VENCIMENTOORIGINAL);  
        $vInterval = $vVenc->diff($vAgora); 
        $vDias= (int)$vInterval->format('%R%a');
        
        // ajusta se o vencimento cair em um sabado ou domingo
        if (($vVenc->format('w')==='0') && ($vDias===1)){
          $vDias= 0;
        } elseif (($vVenc->format('w')==='6') && ($vDias===2)) {
          $vDias= 0;
        } 
          
        
        $vAcessoBD->dataSet[0]->DIAS= $vDias;

                
        if ($vDias > 0) {
          $vMulta = 2 + ($vDias*0.033);
          $vAcessoBD->dataSet[0]->MULTA=$vMulta; 

          if ($vAcessoBD->dataSet[0]->BANCO==='001') {
            $vBBMora =  ($vAcessoBD->dataSet[0]->VALORAPAGAR*$vAcessoBD->dataSet[0]->DIAS= $vDias*0.0333);  
           $vBBJuros = trunca( ($vAcessoBD->dataSet[0]->VALORAPAGAR*0.02));
           $vAcessoBD->dataSet[0]->ENCARGOS= trunca(($vBBMora/100)+($vBBJuros));

          } else {
            $vAcessoBD->dataSet[0]->ENCARGOS= trunca($vMulta/100*$vAcessoBD->dataSet[0]->VALORAPAGAR);
          }  
        } else {
          $vAcessoBD->dataSet[0]->ENCARGOS= 0;
        }
        
      } elseif ($pOper === CTEEXTRATOIR) {
        $object = new stdClass();
        $object->COMPET = '';
      }

	  } else {
	    return 'ERRO: '.$vAcessoBD->erro.'---'.$vSQL;		
		}
    if ($pOper === CTEUSUVALIDAR) {
      $vRetorno= $vAcessoBD->dataSet[0]->CLICODIGO.'-'.$vAcessoBD->dataSet[0]->CLINOME;
    } else {
	    $vRetorno= $vAcessoBD->dataSetJSON();
    }  
		
	} catch (Exception $pExcecao) {
	  $vRetorno= 'ERRO: Excecao: '.$pExcecao->getMessage();
	}
	FINALLYY:
	unset($vAcessoBD);
  
  if ($vRetornoArr['erro'] != '') {

  } else {
    return $vRetorno;
  } 
}	

function trocarSenha($pOper,$pOper2,$pOper3,$pOper4) {    
  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
  
  $vRetorno='0';
  
  if ($pOper2 === '') {
     $vRetorno='faltam parametros'; 
	  goto FINALLYY;
  } elseif (strlen($pOper3)!==32)  {
    $vRetorno='-1'; 
    goto FINALLYY;
  } elseif ( ($pOper===CTEUSUTROCARSENHA) && (strlen($pOper4)!==32))  {
    $vRetorno='-2'; 
    goto FINALLYY;
  }    
  
  $vAcessoBD->startTrans();
  $vSQL= "select count(CLIENTE.CLICODIGO) as TOTAL ".
         "from CLIENTE ".  
         "where CLIENTE.CLICODIGO=".$pOper2;
  
  if ($pOper===CTEUSUTROCARSENHA) {
    $vSQL.= " and CLIENTE.CLISENHAWEB='".$pOper3."'";
  }  

  //return $vSQL;             
  if ($vAcessoBD->open($vSQL)) {
    if ($vAcessoBD->dataSet[0]->TOTAL >0) {
      $vSQL= "update CLIENTE ";
      if ($pOper===CTEUSUTROCARSENHA) {
        $vSQL.= "set CLISENHAWEB='".$pOper4."' ";
      } else {
        $vSQL.= "set CLISENHAWEB='".$pOper3."' ";
      } 
      $vSQL.= "where CLIENTE.CLICODIGO=".$pOper2;

      if ($vAcessoBD->exec($vSQL)) {
        $vRetorno= $pOper2;      
      }  
    } else {  
      $vRetorno='-1';  
    }
  }  
  $vAcessoBD->commit();
  
  FINALLYY:
	unset($vAcessoBD);
  return $vRetorno;
}

?>