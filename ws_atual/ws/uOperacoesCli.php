<?php

define("CTECLIALT",'alt');
define("CTECLIVCPF",'vcpf');
define("CTECLILER",'ler');
define("CTECLILCPF",'lcpf');
define("CTECLILISTARNOME",'lstn');

define("CTECLILTEL",'ltel');
define("CTECLIITEL",'itel');
define("CTECLIATEL",'atel');
define("CTECLIETEL",'etel');

define("CTECLILDEP",'ldep');
define("CTECLIIDEP",'idep');
define("CTECLIADEP",'adep');
define("CTECLIEDEP",'edep');

define("CTECLILPRO",'lpro');
define("CTECLIIPRO",'ipro');
define("CTECLIAPRO",'apro');
define("CTECLIEPRO",'epro');
define("CTECLIATPRO",'atpro');

define("CTECLIDLPRO",'dlpro');
define("CTECLIDAPRO",'dapro');
define("CTECLIDATPRO",'datpro');

define("CTECLISTATUSATIVO",'A');
define("CTECLISTATUSSUSPENSO",'S');

define("PAR_CLI_NOME",'nome');
define("PAR_CLI_POS",'pos');
?>