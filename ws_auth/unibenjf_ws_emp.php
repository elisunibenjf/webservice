<?php
/*******************************************************************************
 * unibenjf_ws_emp.php
    - processa as requisicoes de empresa

 */
 
$vHelp = '
/*******************************************************************************************************
  - operacoes (parametro op)
    - ???   - help   
    - aut   - autentica empresa
    - cnpje - retorna o email a partir de um CNPJ
    - alt   - cadastra/altera uma empresa
    - vcnpj - verifica se existe empresa com o cnpj
    - ler   - le os dados cadastrais
    - ltel  - listar telefones
    - itel  - incluir telefone
    - atel  - alterar telefone
    - etel  - excluir telefone
    - ccv   - valores resumidos por tipo e vida de uma oompetencia
    - mf    - movimento financeiro no ano
    - msc   - muda senha da empresa apenas pelo codigo
    - ir    - extrato de imposto renda
    - lstn  - lista empresas por nome
    - lstr  - lista empresas por razao social 
    - bempcod - lista todos os dados da empresa por codigo
    - cpnjfemail - retorna cnpj, email da empresa
     
    op    Parametros                                        retorno
    =====  ==============================================================================================
    aut    u - usuario (cnpj da empresa)                     json de resposta: se sucesso => codigo da empresa 
           s - md5 da senha 
    -----  ----------------------------------------------------------------------------------------------
    cnpje  cnpj  - cnpj (14 caracteres sem formatacao )      json de resposta: sucesso,erro,email
    -----  ---------------------------------------------------------------------------------------------- 
    alt    cod   - codigo da empresa                         json de resposta: sucesso,erro,codigo da empresa incluido 
                 - se for ZERO indica que é uma inclusao        
                 - senao indica alteracao da empresa com 
                   o codigo "cod"
           cnpj  - cnpj (14 caractetes sem formatacao )       
           nome  - nome(max 100 caracteres	)
           razao - razao social nome(max 100 caracteres)		  
           log   - logradouro
           num   - numero do endereco
           compl - complemento endereco (max 20 caracteres)
           bai   - bairro
           cid   - cidade
           uf    - unidade federativa
           cep   - cep (8 caracter)
           cnpj  - cnpj(14 caracter sem formatacao) 
           cxp   - caixa postal (max 10 caracteres)
           dtf   - data fundacao (aaaa-mm-dd)
           obs   - observacao (max 5000 caracteres)
           mpg   - modo de pagamento (1-boleto 2- debito cc)
           age   - agencia (max 7 caracteres)
           bc    - id do banco
           diav  - dia vencimento
           cc    - conta corrente
           ncc   - numero do cartao de credito (max 16 caracteres)
           cobcp - envio de cobranca para caixa postal (S/N)   
           iest   - inscricao estadual (max 20 caracteres)
           dmv   - data a partir da qual podera ser feita mudanca na data de vencimento (aaaa-mm-dd)
    -----  ----------------------------------------------------------------------------------------------
    vcnpj   cnpj  - cnpj (14 caracteres sem formatacao )       json de resposta: sucesso,erro,codigo empresa
    -----   ----------------------------------------------------------------------------------------------
    ler     cod   - codigo da empresa                         json de resposta
                                                             mesmo dados da operacao "alt" e os seguintes abaixo:
                                                             - lognome - logradouro
                                                             -  tlognome- tipo do logradouro
                                                             -  bainome - bairro
                                                             -  cidnome - cidade
                                                             -  ufsigla - estado
    -----  ----------------------------------------------------------------------------------------------
    ltel   cod   - codigo da empresa                         json de resposta
    -----  ----------------------------------------------------------------------------------------------
    itel   cod   - codigo da empresa                         json de resposta
    atel   nome  - nome (max 100 caracteres)
           ddd   - ddd (A2) 
           tel   - telefone (A8)
           cargo - cargo (max 100 caracteres)
           email - email (max 100 caracteres, pode ser null)
    -----  ----------------------------------------------------------------------------------------------
    etel   cod   - codigo do empresa                         json de resposta            
           nome  - nome (max 100 caracteres)
           ddd   - ddd (A2) 
           tel   - telefone (A8)
    -----  ----------------------------------------------------------------------------------------------
    ccv    cod   - codigo do empresa                         json de resposta                  
           s     - senha (MD5)    
           compet- competencia(AAAAMM)
    -----  ----------------------------------------------------------------------------------------------
    mf     cod   - codigo do empresa                         json de resposta                  
           s     - senha (MD5)    
           ano   - ano(AAAA)
    -----  ----------------------------------------------------------------------------------------------       
    b2v    cod   - codigo do empresa                         json de resposta (dados para emissao do boleto) 
           s     - senha (MD5)    
           ns    - nosso numero
    -----  ----------------------------------------------------------------------------------------------
    msc    cod - codigo do usuario                           json de resposta: sucesso,erro
           sn  - nova senha (MD5)                                     
    -----  ----------------------------------------------------------------------------------------------
    ir     cod   - codigo do empresa                         json de resposta                  
           s     - senha (MD5)    
           ano   - ano(AAAA)
    -----  ----------------------------------------------------------------------------------------------
    lstn   nome  - parte no nome a ser consultado            json de resposta                  
           pos   - posicao [i]nicio [q]ualquer 
    -----  ----------------------------------------------------------------------------------------------
    lstr   razao - parte da razao social a ser consultado    json de resposta                  
           pos   - posicao [i]nicio [q]ualquer        
    -----  ----------------------------------------------------------------------------------------------       
    bempcod cod  - codigo da empresa  json de resposta

********************************************************************************************************/
';
//http://187.32.177.137/ws/unibenjf_ws_emp.php?op=a
//EMPCONTATO
//http://179.184.216.103/ws/unibenjf_ws_emp_dsv.php?op=icon&cod=3037&nome=teste&ddd=032&tel=32211111&cargo=abc&email=abc@gmail

require_once("./uClassFB.php"); 
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php");

//xdebug_disable();
error_reporting(E_ALL ^ E_WARNING); 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';

if ($vOper==='???') {
	$vResult= $vHelp;
} else {  
  require_once("./uOperacoesEmp.php");
  
  if (in_array($vOper,array(CTEEMPALT, CTEEMPICON, CTEEMPACON, CTEEMPECON))) {  
    $vResult = alteracao($vOper);
  } elseif (in_array($vOper,array(CTEEMPAUT,CTEEMPVCNPJ, CTEEMPLER, CTEEMPLCON,CTEEMPCNPJEMAIL,CTEEMPCOMPETCLIVIDA,CTEEMPMOVFIN,CTEEMPBOLETO2AVIA,CTEEMPEXTRATOIR,
                                  CTEEMPLISTARNOME,CTEEMPLISTARRAZSOCIAL,BUSEMPCOD, CNPJEMAIL))) {  
    $vResult = lerdados($vOper);
  } elseif (in_array($vOper,array(CTEMUDASENHACOD))) {  
  	$vResult = trocarSenha($vOper); 
  }	
} 

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log_emp.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 

/**
  *Leitura de dados
*/

function lerDados($pOper) {    
	$vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

	if (!$vAcessoBD = criaAcessoBD()) {
		$vRetornoArr['erro']= 'erro na abertura do BD';
		goto FINALLYY;
	}
  
	$vSQL= '';

	if (in_array($pOper,array(CTEEMPLER,CTEEMPLCON,CTEEMPCOMPETCLIVIDA,CTEEMPBOLETO2AVIA,CTEEMPEXTRATOIR))) {  
    if (!isset($_REQUEST[PAR_EMP_COD])) {
		  $vRetornoArr['erro']= PAR_EMP_COD.' nao informadox';
		  goto FINALLYY;
	  }
	}	

	if (in_array($pOper,array(CTEEMPCOMPETCLIVIDA,CTEEMPMOVFIN,CTEEMPBOLETO2AVIA,CTEEMPEXTRATOIR))) {  
    if (!isset($_REQUEST[PAR_EMP_SENHA])) {
	    $vRetornoArr['erro']= 'acesso nao permitido';
	    goto FINALLYY;
	  }
	}  


	if (in_array($pOper,array(CTEEMPCOMPETCLIVIDA))) {  
	  if (!isset($_REQUEST[PAR_EMP_COMPET])) {
	    $vRetornoArr['erro']= PAR_EMP_COMPET.' nao informado';
	    goto FINALLYY;
    }
	}

	if (in_array($pOper,array(CTEEMPMOVFIN,CTEEMPEXTRATOIR))) { 
    if (!isset($_REQUEST[PAR_EMP_ANO])) {
	    $vRetornoArr['erro']= 'ano nao informado';
	    goto FINALLYY;
	  }
	}

	if (in_array($pOper,array(CTEEMPBOLETO2AVIA))) { 
    if (!isset($_REQUEST[PAR_EMP_NOSSONUM])) {
	    $vRetornoArr['erro']= PAR_EMP_NOSSONUM.' nao informado';
	    goto FINALLYY;
	  }
	}

  if (in_array($pOper,array(CTEEMPLISTARNOME))) { 
    if (!isset($_REQUEST[PAR_EMP_NOME])) {
      $vRetornoArr['erro']= PAR_EMP_NOME.' nao informado';
      goto FINALLYY;
    }
  }

  if (in_array($pOper,array(CTEEMPLISTARRAZSOCIAL))) { 
    if (!isset($_REQUEST[PAR_EMP_RAZAO])) {
      $vRetornoArr['erro']= PAR_EMP_RAZAO.' nao informado';
      goto FINALLYY;
    }
  }

  if (in_array($pOper,array(CTEEMPLISTARNOME,CTEEMPLISTARRAZSOCIAL))) { 
    if (!isset($_REQUEST[PAR_EMP_POS])) {
      $vRetornoArr['erro']= PAR_EMP_POS.' nao informado';
      goto FINALLYY;
    }
  }

  if ($pOper === CTEEMPAUT) {
    if  (!( isset($_REQUEST[PAR_EMP_USUARIO]) && isset($_REQUEST[PAR_EMP_SENHA]) )) {
 	    $vRetornoArr['erro']= 'faltam dados de autenticacao';
		goto FINALLYY;
	  }	
	  $vSQL = 'select coalesce(EMPCODIGO,0) as CODIGO, coalesce(EMPNOME,0) as NOME, coalesce(EMPRAZAOSOCIAL,0) AS RAZAO  '.
              'from EMPRESA '.
              'where EMPCNPJ='.quotedSingleStr($_REQUEST[PAR_EMP_USUARIO]).
			        ' and EMPSENHAWEB='.quotedSingleStr($_REQUEST[PAR_EMP_SENHA]);	
	}elseif ($pOper === CTEEMPVCNPJ) {
		if (!isset($_REQUEST[PAR_EMP_CNPJ])) {
		  $vRetornoArr['erro']= PAR_EMP_CNPJ.' nao informado';
		  goto FINALLYY;
		}
		$vSQL = 'select coalesce(EMPCODIGO,0) as CODIGO, EMPCNPJ'.
				' from EMPRESA '.
				'where EMPCNPJ='.$_REQUEST[PAR_EMP_CNPJ];
	} elseif ($pOper === CTEEMPLER) {
		if (!isset($_REQUEST[PAR_EMP_COD])) {
		  $vRetornoArr['erro']= PAR_EMP_COD.' nao informado';
		  goto FINALLYY;
		}

		$vSQL = 'select '.
						'EMP.EMPCNPJ AS '.PAR_EMP_CNPJ.', '.
						'EMP.EMPCODIGO AS '.PAR_EMP_COD.', '.
						'EMP.EMPNOME AS '.PAR_EMP_NOME.', '.
						'EMP.EMPRAZAOSOCIAL AS '.PAR_EMP_RAZAO.', '.
            'EMP.EMPLOGRADOURO AS '.PAR_EMP_LOG.', '.     
						'EMP.EMPNUMERO AS '.PAR_EMP_NUM.', '.
						'EMP.EMPCOMPLEMENTO AS '.PAR_EMP_COMPL.', '.
						'EMP.EMPBAIRRO AS '.PAR_EMP_BAI.', '.
            'EMP.EMPCIDADE AS '.PAR_EMP_CID.', '.
            'EMP.EMPUF AS '.PAR_EMP_UF.', '.
						'EMP.EMPCEP AS '.PAR_EMP_CEP.', '.
						'EMP.EMPCAIXAPOSTAL AS '.PAR_EMP_CXP.', '.
						'EMP.EMPDATAFUNDACAO AS '.PAR_EMP_DTF.', '.
						'EMP.EMPOBS AS '.PAR_EMP_OBS.', '.
						'EMP.MODOPCODIGO AS '.PAR_EMP_MODOPAGTO.', '.
						'EMP.EMPAGENCIA AS '.PAR_EMP_AGE.', '.
						'EMP.BANCOID AS '.PAR_EMP_BC.', '.
						'EMP.EMPDIAVENCIMENTO AS '.PAR_EMP_DIAV.', '.
						'EMP.EMPCONTACORRENTE AS '.PAR_EMP_CC.', '.
						'EMP.EMPCARTAOCREDITONUM AS '.PAR_EMP_NCC.', '.
						'EMP.EMPCOBCXPOSTAL AS '.PAR_EMP_COBCP.', '.
						'EMP.EMPINSCEST AS '.PAR_EMP_INS.', '.
						'EMP.EMPDATAMUDADIAVENC '.PAR_EMP_DMV.' '.
				'from EMPRESA EMP '.	
				'where EMP.EMPCODIGO='.$_REQUEST[PAR_EMP_COD];

	} elseif ($pOper === CTEEMPLCON) {
		if (!isset($_REQUEST[PAR_EMP_COD])) {
		  $vRetornoArr['erro']= PAR_EMP_COD.' nao informado';
		  goto FINALLYY;
		}
		$vSQL = 'select '.
					'EMPCODIGO AS '.PAR_EMP_COD.', '.
					'EMPCNOME AS '.PAR_EMP_CON_NOME.', '.
					'EMPCDDD AS '.PAR_EMP_CON_DDD.', '.
					'EMPCNUMERO AS '.PAR_EMP_CON_TEL.', '.
					'EMPCCARGO AS '.PAR_EMP_CON_CARGO.', '.
					'EMPCEMAIL AS '.PAR_EMP_CON_EMAIL.' '.				
				'from EMPCONTATO '.
				'where EMPCODIGO='.$_REQUEST[PAR_EMP_COD];
	} elseif ($pOper === CTEEMPCNPJEMAIL) {
      if (!isset($_REQUEST[PAR_EMP_CNPJ])) {
		$vRetornoArr['erro']= PAR_EMP_CNPJ.' nao informado';
		goto FINALLYY;
	  }	
	  $vSQL = 'select '.
					'EMPEMAIL AS '.PAR_EMP_EMAIL.' '.				
			  'from EMPRESA '.
			  'where EMPCNPJ='.quotedSingleStr($_REQUEST[PAR_EMP_CNPJ]);	
	} elseif ($pOper === CTEEMPCOMPETCLIVIDA) {

        $vSQL = 'select 
        DADOS.CRECID,
        DADOS.COMPET AS COMPET'.
        ',DADOS.CLIMMODOCOB AS MODO'.
        ',C.CLINOME as CLIENTE'.
        ',case DADOS.CLIDEPCODIGO '.
        'when 0 then '.quotedSingleStr('').' '.
        'else coalesce(CLIDEP.CLIDEPNOME,'.quotedSingleStr('<<<DESCONHECIDO>>>').') end as DEPEND'.
        ',FLANCAMENTOTIPO.FLANCTACRONIMO as TIPO'.
        ',DADOS.VALOR_BASE,DADOS.ENCARGOS'.
        ',(DADOS.VALOR_BASE+DADOS.ENCARGOS) AS TOTAL'.
        ',DADOS.CLICODIGO '.
        'from '.
        '('.
        'select CMOV.*,'.
        'case CMOV.FLANCTCODIGO '.
        'when 1 then (select coalesce(sum(CONTASARECEBER.CRECMORAMULTA),0) '.
        'from CONTASARECEBER '.
        'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
        '(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.CLICODIGO) and (CONTASARECEBER.CRECSTATUS='.quotedSingleStr('PG').')'.
        ' and (CONTASARECEBER.CRECTIPOENTIDADE='.quotedSingleStr('C').') '.
        ') '.

        'else 0 '.
        'end as ENCARGOS '.
        'from '.
        '('.
        'select CM.CRECID, CM.CLIMCOMPETENCIA AS COMPET,CM.CLIMMODOCOB,CM.CLICODIGO,CM.CLIDEPCODIGO,CM.FLANCTCODIGO,'.
        'sum(CM.CLIMQTD*CM.CLIMVALORUNITARIOCOBRADO) as VALOR_BASE '.
        'from CLIMOVIMENTACAO CM '.
        'inner join EMPRESA E on (E.EMPCODIGO=CM.EMPCODIGO) '.
        'where CM.CLIMCOMPETENCIA='.quotedSingleStr($_REQUEST[PAR_EMP_COMPET]).' '.
        'and (CM.EMPCODIGO='.$_REQUEST[PAR_EMP_COD].') '.
        'group by 1,2,3,4,5,6 '.
        ') as CMOV '.
        ') as DADOS '.
        'inner join FLANCAMENTOTIPO on (FLANCAMENTOTIPO.FLANCTCODIGO=DADOS.FLANCTCODIGO) '.
        'inner join CLIENTE C on (DADOS.CLICODIGO=C.CLICODIGO) '.
        'left join CLIDEP on (DADOS.CLICODIGO=C.CLICODIGO) and (DADOS.CLIDEPCODIGO=CLIDEP.CLIDEPCODIGO) '.
        'order by 2,1,3';


  } elseif ($pOper === CTEEMPMOVFIN) {
      $vAnoAux = (int) $_REQUEST[PAR_EMP_ANO]-1;
      $VCompetAux = (string)$vAnoAux.'01';

      $vSQL= "select substring(CONTASARECEBER.CRECCOMPETENCIA from 5 for 2) as Mes,".
		       'MODOPAGAMENTO.MODOPNOME as "ModoPgto",'.
		       "CONTASARECEBER.CRECSTATUS as STATUS,".
		       "CONTASARECEBER.CRECDATAVENCIMENTO as Venc,".
		       'CONTASARECEBER.CRECVALORAPAGAR as "Valor",'.
		       "CONTASARECEBER.CRECDATAPAGAMENTO as Pgto,".
		       'CONTASARECEBER.CRECVALORPAGO as "Valor_Pago",'.
		       "CONTASARECEBER.CRECNOSSONUMERO ".
									
		       "from CONTASARECEBER ".  
			   "inner join EMPRESA E on (CONTASARECEBER.CRECENTIDADECODIGO=E.EMPCODIGO) ".
			   'inner join MODOPAGAMENTO on (CONTASARECEBER.MODOPCODIGO=MODOPAGAMENTO.MODOPCODIGO) '.
		       "where (CONTASARECEBER.CRECSTATUS<>'CA') and ".
           "(CONTASARECEBER.CRECTIPOENTIDADE='E') and ".
					 "CONTASARECEBER.CRECENTIDADECODIGO='".$_REQUEST[PAR_EMP_COD]."' and ".
           "CONTASARECEBER.CRECCOMPETENCIA >= '".$VCompetAux."' and ".
					 "substring(CONTASARECEBER.CRECCOMPETENCIA from 1 for 4)<='".$_REQUEST[PAR_EMP_ANO]."' ".
           "order by CONTASARECEBER.CRECCOMPETENCIA DESC"; 
	
  } elseif ($pOper === CTEEMPBOLETO2AVIA)  {
      $vSQL= "select E.EMPRAZAOSOCIAL as sac_nome,E.EMPCNPJ as sac_CNPJ,".
              "coalesce(E.EMPLOGRADOURO,'ND') || ', ' || E.EMPNUMERO || ' ' || ".
              "coalesce(E.EMPCOMPLEMENTO,'',E.EMPCOMPLEMENTO) || ".
              "'-' || coalesce(E.EMPBAIRRO,'ND') as sac_endereco,".
              "E.EMPCEP as sac_cep,".
              "coalesce(E.EMPCIDADE,'ND') as sac_cidade,coalesce(E.EMPUF,'ND') as sac_uf, ".

              "CR.CRECDATA,".
              "CR.CRECDATAVENCIMENTO as VENCIMENTOORIGINAL,".
              "CR.CRECDATAVENCIMENTO as VENCIMENTO,CR.CRECVALORAPAGAR as valorapagar,".

              "'UNIBEN ADMINISTRADORA DE BENEFICIOS LTDA' as ced_nome,".
              "'Rua Braz Bernardino, 192 - Centro' as ced_endereco,'36010320' as ced_cep,".
              "'Juiz de Fora' as ced_cidade,'MG' as ced_uf,".
              "'15.156.097/0001-01' as ced_CNPJ,".

              "CR.CRECNOSSONUMERO as nossonumero,".
              "CR.CRECINSTRUCAO as instrucao,".
	            "CR.CRECSEUNUMERO as seunumero,".
            
              "BANCO.BANCOCODIGO as banco, BANCO.BANCOAGENCIA as agencia, BANCO.BANCOCONTACORRENTE as contacorrente,".
              "CR.BANCOBOLETOCODCONVENIO as convenio,BANCO.BANCODIASVALIDADEBOLETO,".

              "CR.CRECENTIDADECODIGO as codigo, 0 as multa ".

             "from CONTASARECEBER CR ".
             "inner join EMPRESA E on (E.EMPCODIGO=CR.CRECENTIDADECODIGO) ".
             "inner join BANCO on (CR.BANCOID=BANCO.BANCOID) ".
             "where CR.CRECENTIDADECODIGO=".$_REQUEST[PAR_EMP_COD].
                    " and CR.CRECNOSSONUMERO=".quotedSingleStr($_REQUEST[PAR_EMP_NOSSONUM]);
  }	elseif ($pOper === CTEEMPEXTRATOIR)  {
    $vSQL= 'select DADOS.COMPET AS COMPET'.
                  ",DADOS.NOME".
                  ',FLANCAMENTOTIPO.FLANCTACRONIMO as TIPO'.
                  ',DADOS.VALOR_BASE,DADOS.ENCARGOS,DADOS.DESCONTO'.
                  ',(DADOS.VALOR_BASE+DADOS.ENCARGOS) AS TOTAL '.
                  ', DADOS.CNPJ '.
           'from (select CMOV.*,'.
                        'case CMOV.FLANCTCODIGO '.
                           'when 1 then (select coalesce(sum(CONTASARECEBER.CRECMORAMULTA),0) '.
                                        'from CONTASARECEBER '.
                                        'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
                                              "(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.EMPCODIGO) and ".
                                              "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
                                              "(CONTASARECEBER.CRECTIPOENTIDADE='E')".
                                       ") ".
                           'else 0 '.
                        'end as ENCARGOS,'.

                        'case CMOV.FLANCTCODIGO '.
                         'when 1 then (select coalesce(sum(CONTASARECEBER.CRECDESCONTO),0) '.
                                      'from CONTASARECEBER '.
                                      'where (CONTASARECEBER.CRECCOMPETENCIA=CMOV.COMPET) and '.
                                            '(CONTASARECEBER.CRECENTIDADECODIGO=CMOV.EMPCODIGO) and '.
                                            "(CONTASARECEBER.CRECSTATUS='".CTECONTAARECEBERPAGA."') and ".
                                            "(CONTASARECEBER.CRECTIPOENTIDADE='E')".
                                      ') '.

                         'else 0 '.
                       'end as DESCONTO '.

                  'from (select CM.CLIMCOMPETENCIA AS COMPET,CM.EMPCODIGO,CM.CLIDEPCODIGO,CM.FLANCTCODIGO,E.EMPRAZAOSOCIAL as NOME, E.EMPCNPJ as CNPJ,'.
                               'sum(CM.CLIMQTD*CM.CLIMVALORUNITARIOCOBRADO) as VALOR_BASE '.
                        'from CLIMOVIMENTACAO CM '.
                        'inner join EMPRESA E on (CM.EMPCODIGO = E.EMPCODIGO) '.
                        'inner join CONTASARECEBER CR1 on (CR1.CRECID=CM.CRECID) '.
                        'where E.EMPCODIGO='.$_REQUEST[PAR_EMP_COD].
                              " and E.EMPSENHAWEB=".quotedSingleStr($_REQUEST[PAR_EMP_SENHA]).
                              " and substring(CM.CLIMCOMPETENCIA from 1 for 4)=".quotedSingleStr($_REQUEST[PAR_EMP_ANO]).
                              " and CR1.CRECSTATUS='".CTECONTAARECEBERPAGA."' ".
                              " and CM.CLIMMODOCOB='E' ".
                        'group by 1,2,3,4,5,6 '.
                       ') as CMOV '.
                       ' order by COMPET '.
                ') as DADOS '.
           'inner join FLANCAMENTOTIPO on (FLANCAMENTOTIPO.FLANCTCODIGO=DADOS.FLANCTCODIGO) '.
           'order by 2,3,1';
    

  } elseif (in_array($pOper,array(CTEEMPLISTARNOME,CTEEMPLISTARRAZSOCIAL))) {  
    $vSQL= 'select FIRST 5 E.EMPNOME,E.EMPRAZAOSOCIAL,E.EMPCODIGO '.
           'from EMPRESA E '.
           'where ';
    if ($pOper === CTEEMPLISTARNOME) {         
      $vSQL.= 'E.EMPNOME like '.quotedSingleStr( ($_REQUEST[PAR_EMP_POS] == 'q' ? '%': '').$_REQUEST[PAR_EMP_NOME].'%' );
    } else {
      $vSQL.= 'E.EMPRAZAOSOCIALNOME like '.quotedSingleStr('%'.$_REQUEST[PAR_EMP_RAZAONOME].($_REQUEST[PAR_EMP_POS] == 'q' ? '%': '')  );
    }  

  } elseif($pOper == BUSEMPCOD){

        $vSQL = 'select '.
              'EMP.EMPCNPJ, '.
              'EMP.EMPCODIGO, '.
              'EMP.EMPNOME, '.
              'EMP.EMPRAZAOSOCIAL, '.
              'EMP.EMPLOGRADOURO, '.     
              'EMP.EMPNUMERO, '.
              'EMP.EMPCOMPLEMENTO, '.
              'EMP.EMPBAIRRO, '.
              'EMP.EMPCIDADE, '.
              'EMP.EMPUF, '.
              'EMP.EMPCEP, '.
              'EMP.EMPCAIXAPOSTAL, '.
              'EMP.EMPDATAFUNDACAO, '.
              'EMP.EMPOBS, '.
              'EMP.MODOPCODIGO, '.
              'EMP.EMPAGENCIA, '.
              'BANCO.BANCOCODIGO, '.
              'EMP.EMPDIAVENCIMENTO, '.
              'EMP.EMPCONTACORRENTE, '.
              'EMP.EMPCARTAOCREDITONUM, '.
              'EMP.EMPCOBCXPOSTAL, '.
              'EMP.EMPINSCEST, '.
              'EMP.EMPDATAMUDADIAVENC, '.
              'CONT.EMPCNOME AS RESPONSAVEL, '.
              'CONT.EMPCDDD, '.
              'CONT.EMPCNUMERO AS TELEFONE, '.
              'CONT.EMPCCARGO, '.
              'CONT.EMPCEMAIL '.
              'FROM EMPRESA EMP '.  
              'JOIN EMPCONTATO CONT ON EMP.EMPCODIGO = CONT.EMPCODIGO '.
              'JOIN BANCO ON EMP.BANCOID = BANCO.BANCOID '.
              'WHERE EMP.EMPCODIGO ='.$_REQUEST[PAR_EMP_COD]; echo  $vSQL;
} elseif ($pOper == CNPJEMAIL) {
  $vSQL = "SELECT EMPCNPJ, EMPEMAIL, EMPCODIGO FROM EMPRESA WHERE EMPCNPJ = '".$_REQUEST[PAR_EMP_CNPJ]."'";
} 

	if ($vSQL ==='') {
	  $vRetornoArr['erro'] = 'operacao desconhecida';
	  goto FINALLYY;
	}
	
	try {
	  if ($vAcessoBD->open($vSQL)) {
	    if ($vAcessoBD->recordCount===0) {
		    $vRetornoArr['erro']= 'sem dados';
		  } else {

        if ($pOper === CTEEMPBOLETO2AVIA) {  
          date_default_timezone_set('America/Sao_Paulo');
          $vAgora = new DateTime();
          $vAgora->setTime(0,0,0); 

          // verifica se boleto nao expirou
          if ($vAcessoBD->dataSet[0]->BANCODIASVALIDADEBOLETO != 0) {
            $vCRECDATA =  New DateTime($vAcessoBD->dataSet[0]->CRECDATA); 
            $vDiferenca = $vCRECDATA->diff($vAgora);
            $vDias= (int)$vDiferenca->format('%R%a');
            if ($vDias > $vAcessoBD->dataSet[0]->BANCODIASVALIDADEBOLETO) {
              $vRetornoArr['erro']= 'BOLETO EXPIRADO. Entre em contato com o setor financeiro';
              goto FINALLYY;
            }
          } 

          $vAcessoBD->dataSet[0]->VENCIMENTO= $vAgora->format('Y-m-d');   
        
          $vVenc= new DateTime($vAcessoBD->dataSet[0]->VENCIMENTOORIGINAL);  
          $vInterval = $vVenc->diff($vAgora); 
          $vDias= (int)$vInterval->format('%R%a');
          
          if (($vVenc->format('w')==='0') && ($vDias===1)){
            $vDias= 0;
          } elseif (($vVenc->format('w')==='6') && ($vDias===2)) {
            $vDias= 0;
          } 
          
          $vAcessoBD->dataSet[0]->DIAS= $vDias;

          if ($vDias > 0) {
            $vMulta = 2 + ($vDias*0.033);
            $vAcessoBD->dataSet[0]->MULTA=$vMulta; 

            if ($vAcessoBD->dataSet[0]->BANCO==='001') {

            $vBBMora =  ($vAcessoBD->dataSet[0]->VALORAPAGAR*$vAcessoBD->dataSet[0]->DIAS= $vDias*0.0333);  
           $vBBJuros = trunca( ($vAcessoBD->dataSet[0]->VALORAPAGAR*0.02));
           $vAcessoBD->dataSet[0]->ENCARGOS= trunca(($vBBMora/100)+($vBBJuros));
            } else {
              $vAcessoBD->dataSet[0]->ENCARGOS= trunca($vMulta/100*$vAcessoBD->dataSet[0]->VALORAPAGAR);
            }  
          } else {
            $vAcessoBD->dataSet[0]->ENCARGOS= 0;
          }

        } 

		    $vRetornoArr['sucesso']=true;
		    $vRetornoArr['dados']= $vAcessoBD->dataSetJSON();
		  }  
	  } else {
		  $vRetornoArr['erro']= $vAcessoBD->erro;
	  }
	} catch (Exception $vExcecao) {
	  $vRetornoArr['erro'] = 'exception: '.$vExcecao->getMessage();
	}

	FINALLYY:
	  unset($vAcessoBD);

	return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}

function alteracao($pOper) {
	$vRetornoArr = array("sucesso"=>false,"erro"=>"");

	if (!$vAcessoBD = criaAcessoBD()) {
		$vRetornoArr['erro']= 'erro na abertura do BD';
		goto FINALLYY_ALL;
	}

	if ($pOper === CTEEMPALT) {
		if (!isset($_REQUEST[PAR_EMP_COD])) {
			$vRetornoArr['erro']= PAR_EMP_COD.' nao informado';
			goto FINALLYY_ALL;
		} else {
			$vNovoId= $_REQUEST[PAR_EMP_COD]; 
		}
		
		if (!isset($_REQUEST[PAR_EMP_CNPJ])) {
			$vRetornoArr['erro']= PAR_EMP_CNPJ.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_NOME])) {
			$vRetornoArr['erro']= PAR_EMP_NOME.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_RAZAO])) {
			$vRetornoArr['erro']= PAR_EMP_RAZAO.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_CEP])) {
			$vRetornoArr['erro']= PAR_EMP_CEP.' nao informado';
			goto FINALLYY_ALL;
		}
	} elseif (in_array($pOper,array(CTEEMPICON, CTEEMPACON, CTEEMPECON))) {  
		if (!isset($_REQUEST[PAR_EMP_COD])) {
			$vRetornoArr['erro']= PAR_EMP_COD.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_CON_NOME])) {
			$vRetornoArr['erro']= PAR_EMP_CON_NOME.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_CON_DDD])) {
			$vRetornoArr['erro']= PAR_EMP_CON_DDD.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_CON_TEL])) {
			$vRetornoArr['erro']= PAR_EMP_CON_TEL.' nao informado';
			goto FINALLYY_ALL;
		}
		if (!isset($_REQUEST[PAR_EMP_CON_CARGO])) {
			$vRetornoArr['erro']= PAR_EMP_CON_CARGO.' nao informado';
			goto FINALLYY_ALL;
		}
	}

	$vSQL= '';
	$vAgora = new DateTime();

	try {
		$vAcessoBD->createTrans();
		
		if ($pOper === CTEEMPALT) {
			$vCnpj = quotedSingleStr($_REQUEST[PAR_EMP_CNPJ]);
			$vNome = quotedSingleStr($_REQUEST[PAR_EMP_NOME]);
			$vRazao = quotedSingleStr($_REQUEST[PAR_EMP_RAZAO]);

      //$vLogCodigo = '1';

			$vLog = quotedSingleStr($_REQUEST[PAR_EMP_LOG]);
			$vNum = $_REQUEST[PAR_EMP_NUM];
			$vCompl = quotedSingleStr((isset($_REQUEST[PAR_EMP_COMPL])) ? $_REQUEST[PAR_EMP_COMPL] : '');
			$vBai = quotedSingleStr($_REQUEST[PAR_EMP_BAI]);
      $vCid = quotedSingleStr($_REQUEST[PAR_EMP_CID]);
      $vUF = quotedSingleStr($_REQUEST[PAR_EMP_UF]);
			$vCep = quotedSingleStr($_REQUEST[PAR_EMP_CEP]);
			$vCxp = quotedSingleStr((isset($_REQUEST[PAR_EMP_CXP])) ? $_REQUEST[PAR_EMP_CXP] : '');
			$vDtf = quotedSingleStr((isset($_REQUEST[PAR_EMP_DTF])) ? $_REQUEST[PAR_EMP_DTF] : '');
			$vObs = quotedSingleStr((isset($_REQUEST[PAR_EMP_OBS])) ? $_REQUEST[PAR_EMP_OBS] : '');
			$vMPg = quotedSingleStr((isset($_REQUEST[PAR_EMP_MODOPAGTO])) ? $_REQUEST[PAR_EMP_MODOPAGTO] : '');
			$vAg = quotedSingleStr((isset($_REQUEST[PAR_EMP_AGE])) ? $_REQUEST[PAR_EMP_AGE] : '');
			$vBc = $_REQUEST[PAR_EMP_BC];
			$vDiav = $_REQUEST[PAR_EMP_DIAV];
			$vCc = quotedSingleStr((isset($_REQUEST[PAR_EMP_CC])) ? $_REQUEST[PAR_EMP_CC] : '');
			$vNcc = quotedSingleStr((isset($_REQUEST[PAR_EMP_NCC])) ? $_REQUEST[PAR_EMP_NCC] : '');						   
			$vCobcp = quotedSingleStr((isset($_REQUEST[PAR_EMP_COBCP])) ? $_REQUEST[PAR_EMP_COBCP] : 'N');
			$vIns = quotedSingleStr((isset($_REQUEST[PAR_EMP_INS])) ? $_REQUEST[PAR_EMP_INS] : '');
			$vDmv = quotedSingleStr((isset($_REQUEST[PAR_EMP_DMV])) ? $_REQUEST[PAR_EMP_DMV] : '');						   
			$vData = quotedSingleStr($vAgora->format('Y-m-d'));
		  
			if ($_REQUEST[PAR_EMP_COD] == 0) {
				$vSQL = 'select CODIGO from SP_NOVOCODIGO(12)';
			  
				if ($vAcessoBD->open($vSQL, false)) {
					$vNovoId= $vAcessoBD->dataSet[0]->CODIGO;
				} else {
					$vRetornoArr['erro'] = $vAcessoBD->erro;
					goto FINALLYY;    
				}

				$vSQL = 'insert into EMPRESA ('.
								'EMPCNPJ, '.
								'EMPCODIGO, '.
								'EMPNOME, '.
								'EMPRAZAOSOCIAL, '.
								//'LOGCODIGO, '.
								'EMPNUMERO, '.
								'EMPCOMPLEMENTO, '.
								//'BAICODIGO, '.
								'EMPCEP, '.
								'EMPCAIXAPOSTAL, '.
								'EMPDATAFUNDACAO, '.
								'EMPOBS, '.
								'MODOPCODIGO,'.
								'EMPAGENCIA, '.
								'BANCOID, '.
								'EMPDIAVENCIMENTO, '.
								'EMPCONTACORRENTE, '.
								'EMPCARTAOCREDITONUM, '.
								'EMPCOBCXPOSTAL, '.
								'EMPINSCEST, '.
								'EMPDATAMUDADIAVENC, '.
								'EMPDATACADASTRO,'.
                'EMPLOGRADOURO,'.
                'EMPBAIRRO,'.
                'EMPCIDADE,'.
                'EMPUF'.
                ') '.
						 'values ('.
								$vCnpj.','.
								$vNovoId.','.
								$vNome.','.
								$vRazao.','.
								//$vLog.','.
								$vNum.','.
								$vCompl.','.
								//$vBai.','.
								$vCep.','.
								$vCxp.','.
								$vDtf.','.
								$vObs.','.
								$vMPg.','.
								$vAg.','.
								$vBc.','.
								$vDiav.','.
								$vCc.','.
								$vNcc.','.
								$vCobcp.','.
								$vIns.','.
								$vDmv.','.
								$vData.','.
                $vLog.','.
                $vBai.','.
                $vCid.','.
                $vUF.
                ')';
			} else {
				$vSQL = 'update EMPRESA set'.
							'EMPCNPJ='.$vCnpj.','.
							'EMPNOME='.$vNome.','.
							'EMPRAZAOSOCIAL='.$vRazao.','.
							//'LOGCODIGO='.$vLog.','.
							'EMPNUMERO='.$vNum.','.
							'EMPCOMPLEMENTO='.$vCompl.','.
							//'BAICODIGO='.$vBai.','.
							'EMPCEP='.$vCep.','.
							'EMPCAIXAPOSTAL='.$vCxp.','.
							'EMPDATAFUNDACAO='.$vDtf.','.
							'EMPOBS='.$vObs.','.
							'EMPAGENCIA='.$vAg.','.
							'BANCOCODIGO='.$vBc.','.
							'EMPDIAVENCIMENTO='.$vDiav.','.
							'EMPCONTACORRENTE='.$vCc.','.
							'EMPCARTAOCREDITONUM='.$vNcc.','.						   
							'EMPCOBCXPOSTAL='.$vCobcp.','.
							'EMPINSCEST='.$vIns.','.
							'EMPDATAMUDADIAVENC='.$vDmv.','.						   
							'EMPDATAALTERACAO='.$vData.','.
              'EMPLOGRADOURO='.$vLog.','.
              'EMPBAIRRO='.$vBai.','.
              'EMPCIDADE='.$vCid.','.
              'EMPUF='.$vUF.' '.
						'where EMPCODIGO='.$_REQUEST[PAR_EMP_COD];
			}
		} else {
			$vCod = $_REQUEST[PAR_EMP_COD];
			$vNome = quotedSingleStr($_REQUEST[PAR_EMP_CON_NOME]);
			$vDdd = quotedSingleStr($_REQUEST[PAR_EMP_CON_DDD]);
			$vTel = quotedSingleStr($_REQUEST[PAR_EMP_CON_TEL]);
			$vCargo = quotedSingleStr($_REQUEST[PAR_EMP_CON_CARGO]);
			$vEmail = quotedSingleStr((isset($_REQUEST[PAR_EMP_CON_EMAIL])) ? $_REQUEST[PAR_EMP_CON_EMAIL] : '');
			$vNovoId = $vCod;
			
			if ($pOper === CTEEMPICON)  {
				$vSQL = 'insert into EMPCONTATO ('.
							'EMPCODIGO, '.
							'EMPCNOME, '.
							'EMPCDDD, '.
							'EMPCNUMERO, '.
							'EMPCCARGO, '.
							'EMPCEMAIL) '.
						'values ('.
							$vCod.','.
							$vNome.','.
							$vDdd.','.
							$vTel.','.
							$vCargo.','.
							$vEmail.')';							
			} elseif ($pOper === CTEEMPACON)  {
				$vSQL = 'update EMPCONTATO set '.
							'EMPCCARGO='.$vCargo.','.
							'EMPCEMAIL='.$vEmail.
						' where '.
							'EMPCODIGO='.$vCod.
							' AND EMPCNOME='.$vNome.
							' AND EMPCDDD='.$vDdd.
							' AND EMPCNUMERO='.$vTel;
			} elseif ($pOper === CTEEMPECON)  {
				$vSQL = 'DELETE FROM EMPCONTATO WHERE '.
							'EMPCODIGO='.$vCod.
							' AND EMPCNOME='.$vNome.
							' AND EMPCDDD='.$vDdd.
							' AND EMPCNUMERO='.$vTel;
			}						   
		} 

		//return $vSQL;
		
		if ($vSQL ==='') {
		  $vRetornoArr['erro'] = 'operacao desconhecida';
		  goto FINALLYY;
		}
	 
		  if ($vAcessoBD->exec($vSQL,false)) {
		  $vRetornoArr['sucesso'] = true;

		} else {  
		  $vRetornoArr['erro'] = $vAcessoBD->erro;
		  goto FINALLYY;
		}
	} catch (Exception $vExcecao) {
		$vRetornoArr['erro'] = 'exception: '.$vExcecao->getMessage();
	}
  
	FINALLYY:
		if ($vRetornoArr['sucesso']) {
			$vAcessoBD->commit();     
			$vRetornoArr['codigo']= $vNovoId;
		} else {
			$vAcessoBD->rollback();    
		}
	FINALLYY_ALL:
		unset($vAcessoBD);
		return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);
}

function trocarSenha($pOper) {    
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
    $vRetornoArr['erro']= 'erro na abertura do BD';
    goto FINALLYY;
  } 

  $vCod = (isset($_REQUEST['cod'])) ? $_REQUEST['cod'] : '0';
  $vSN  = (isset($_REQUEST['sn'])) ? $_REQUEST['sn'] : '';

  if (strlen($vSN)!==32)  {
    $vRetornoArr['erro']= 'nova senha inválida';
    goto FINALLYY;
  }    

  $vAcessoBD->startTrans();
  $vSQL= "select count(EMPRESA.EMPCODIGO) as TOTAL ".
         "from EMPRESA ".  
         "where EMPRESA.EMPCODIGO=".$vCod;
  
  if ($vAcessoBD->open($vSQL)) {
    if ($vAcessoBD->dataSet[0]->TOTAL >0) {
      $vSQL= "update EMPRESA ".
             "set EMPSENHAWEB='".$vSN."' ".
             "where EMPRESA.EMPCODIGO=".$vCod;
      if ($vAcessoBD->exec($vSQL)) {
        $vRetornoArr['sucesso']=true;      
      } else {
      	$vRetornoArr['erro']= $vAcessoBD->erro;
      } 
    } else {  
      $vRetornoArr['erro']= 'empresa nao encontrada';
    }
  } else {
  	$vRetornoArr['erro']= $vAcessoBD->erro;
  } 
  $vAcessoBD->commit();



  FINALLYY:
  unset($vAcessoBD);
  return json_encode($vRetornoArr,JSON_HEX_APOS|JSON_HEX_QUOT);

}
  


?>