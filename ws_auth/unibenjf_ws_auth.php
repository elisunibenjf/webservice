<?php
header('Access-Control-Allow-Origin: *');
$vHelp = '
/*******************************************************************************
  - operacoes (parametro op)
    - ??? - retorna este help   
    - cli  - valida um cliente
    - emp - autentica empresa
    - usu - valida o acesso de um usuario

  - parametros
 
    op  		op2                           op3               retorno
    --  		-----------------------       ----------        ---------  
    cli  		CPF do usuario                senha             codigo do usuario-nome do usuario
                                 		      (MD5)             ZERO se nao validar
    emp     cnpj da empresa               cnpj da empresa   codigo do usuario-nome do usuario
                                          (MD5)             ZERO se nao validar
    usu     login com ate 10 caracteres   senha             codigo do usuario-nome do usuario
                                          (MD5)             ZERO se nao validar
 
  ----  ----------------------------------------------------------------------------------------------
                                                                                                   
                                 
*******************************************************************************/
';

require_once("./uClassFB.php");
require_once("./uDefinicoes.php"); 
require_once("./uUtil.php"); 
 

$vResult= '';
$vOper  = (isset($_REQUEST['op'])) ? $_REQUEST['op'] : '';
$vOper2 = (isset($_REQUEST['op2'])) ? $_REQUEST['op2'] : '';
$vOper3 = (isset($_REQUEST['op3'])) ? $_REQUEST['op3'] : '';

if ($vOper !== '') {
	if ($vOper==='???') {
		$vResult= $vHelp;
	} else {
  	define("CTEUSUVALIDAR",'cli');
    define("CTEEMPAUT",'emp');
    define("CTEUSUVAL",'usu');

	  $vResult = lerDados($vOper,$vOper2,$vOper3);
	}	
}

header("Content-Type: application/json; charset=ISO-8859-1",true);

if ($vResult == ''){
  echo 'Erro desconhecido - '.$vOper;
} else {
  echo $vResult;
}

$fp = fopen("./log/log.txt", "a");
$escreve = fwrite($fp, date("Y-m-d H:i:s").'|'.$vOper.'|'.$_SERVER['QUERY_STRING']."|".$vResult.PHP_EOL);
fclose($fp); 


function lerDados($pOper,$pOper2,$pOper3) {    
  $vRetornoArr = array("sucesso"=>false,"erro"=>"","dados"=>"");

  if (!$vAcessoBD = criaAcessoBD()) {
	  return 'ERRO: erro na abertura do BD';
  }
	
	if (in_array($pOper,array(CTEUSUVALIDAR,CTEEMPAUT,CTEUSUVAL))) {
	  if ($pOper2 === '') {
	    $vRetorno= 'Requisicao invalida2';
	    goto FINALLYY;
	  }
	}
	
	if (in_array($pOper,array(CTEUSUVALIDAR,CTEEMPAUT,CTEUSUVAL))) {
	  if ($pOper3 === '') {
	    $vRetorno= 'Requisicao invalida3';
	    goto FINALLYY;
	  }
	}
  
	$vRetorno='';
	$vSQL= '';
  
	if ($pOper === CTEUSUVALIDAR) {
    $vSQL = "select CLIENTE.CLICODIGO,CLIENTE.CLINOME ".  
            "from CLIENTE ".	
			      "where CLIENTE.CLICPF='".$pOper2."' and ".
                  "CLIENTE.CLISENHAWEB='".$pOper3."'";
  } elseif ($pOper === CTEEMPAUT) {
    $vSQL = "select coalesce(EMPCODIGO,0) as CODIGO, coalesce(EMPNOME,0) as NOME, coalesce(EMPRAZAOSOCIAL,0) AS RAZAO  ".
              "from EMPRESA ".
              "where EMPCNPJ='".$pOper2."'".
              " and EMPSENHAWEB='".$pOper3."'";  
  } elseif ($pOper === CTEUSUVAL) {
    $vSQL = "select USUARIO.UCODIGO as cod, USUARIO.UNOME as nome ".
            "from USUARIO ".
            "where USUARIO.ULOGIN='".$pOper2."' and ".
                  "USUARIO.UINFO='".$pOper3."'";
  } else {
    $vSQL = ''; 
  }  

	if ($vSQL ==='') {
	  goto FINALLYY;
	}
	
	try {
	  if ($vAcessoBD->open($vSQL)) {	
			if ($vAcessoBD->recordCount===0) {
        if ($pOper === CTEUSUVALIDAR || $pOper === CTEEMPAUT || $pOper === CTEUSUVAL) {
          $vRetorno= '0';
        } else {  
	        $vRetorno = 'Sem dados';
        }  
				goto FINALLYY;
			} 
	  } else {
	    return 'ERRO: '.$vAcessoBD->erro.'---'.$vSQL;		
		}

    $vRetorno= $vAcessoBD->dataSetJSON();

    if ($pOper === CTEUSUVALIDAR) {
      $vSQL = 'update CLIENTE set AUTHTOKEN='.
          'where CLICODIGO='.$vAcessoBD->dataSet[0]->CLICODIGO.')';
    } elseif ($pOper === CTEEMPAUT) {
      $vSQL = 'update EMPRESA set AUTHTOKEN='.
          'where EMPCODIGO='.$vAcessoBD->dataSet[0]->CODIGO.')';
    } elseif ($pOper === CTEUSUVAL) {
      $vSQL = 'update USUARIO set AUTHTOKEN='.
          'where UCODIGO='.$vAcessoBD->dataSet[0]->cod.')';
    }
		
	} catch (Exception $pExcecao) {
	  $vRetorno= 'ERRO: Excecao: '.$pExcecao->getMessage();
	}
	FINALLYY:
	unset($vAcessoBD);
  
  if ($vRetornoArr['erro'] != '') {

  } else {
    return $vRetorno;
  } 
}	
?>